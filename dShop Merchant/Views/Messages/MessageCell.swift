//
//  MessageCell.swift
//  dShop Merchant
//
//  Created by Marknel Pogi on 11/8/18.
//  Copyright © 2018 Marknel Pogi. All rights reserved.
//

import UIKit

class MessageCell: UICollectionViewCell {
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupViews()
    }
    
    let profileImageView: CachedImageView = {
        let iv = CachedImageView()
        iv.image = UIImage(named: "placeholder-image")
        iv.layer.cornerRadius = 27
        iv.layer.masksToBounds = true
        iv.contentMode = .scaleAspectFill
        iv.translatesAutoresizingMaskIntoConstraints = false
        return iv
    }()
    
    let senderNameLabel: UILabel = {
        let lbl = UILabel()
        lbl.text = "John Joe"
        lbl.font = UIFont(name: "SFUIDisplay-Bold", size: 16)
        lbl.textColor = .black
        lbl.translatesAutoresizingMaskIntoConstraints = false
        return lbl
    }()
    
    let previewTextLabel: UILabel = {
        let lbl = UILabel()
        lbl.text = "John Joe"
        lbl.font = UIFont(name: "SFUIDisplay-Medium", size: 12)
        lbl.textColor = UIColor(red: 138/255, green: 138/255, blue: 138/255, alpha: 1)
        lbl.translatesAutoresizingMaskIntoConstraints = false
        return lbl
    }()
    
    let messageIndicatorImageView: UIView = {
        let view = UIView()
        view.layer.cornerRadius = 3
        view.layer.masksToBounds = true
        view.backgroundColor = UIColor(red: 51/255, green: 136/255, blue: 250/255, alpha: 1)
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    let dateLabel: UILabel = {
        let lbl = UILabel()
        lbl.text = "Jan 5"
        lbl.textAlignment = .right
        lbl.font = UIFont(name: "SourceSansPro-Regular", size: 12)
        lbl.textColor = UIColor(red: 138/255, green: 138/255, blue: 138/255, alpha: 1)
        lbl.translatesAutoresizingMaskIntoConstraints = false
        return lbl
    }()
    
    private func setupViews() {
        layer.cornerRadius = 10
        layer.masksToBounds = true
        backgroundColor = .white
        
        addSubview(profileImageView)
        profileImageView.centerYAnchor.constraint(equalTo: centerYAnchor).isActive = true
        profileImageView.leftAnchor.constraint(equalTo: leftAnchor, constant: 12).isActive = true
        profileImageView.heightAnchor.constraint(equalToConstant: 54).isActive = true
        profileImageView.widthAnchor.constraint(equalTo: profileImageView.heightAnchor, multiplier: 1.0).isActive = true
        
        addSubview(senderNameLabel)
        senderNameLabel.topAnchor.constraint(equalTo: profileImageView.topAnchor, constant: 6).isActive = true
        senderNameLabel.leftAnchor.constraint(equalTo: profileImageView.rightAnchor, constant: 8).isActive = true
        
        addSubview(previewTextLabel)
        previewTextLabel.topAnchor.constraint(equalTo: senderNameLabel.bottomAnchor, constant: 2).isActive = true
        previewTextLabel.leftAnchor.constraint(equalTo: senderNameLabel.leftAnchor).isActive = true
        
        addSubview(messageIndicatorImageView)
        messageIndicatorImageView.topAnchor.constraint(equalTo: topAnchor, constant: 12).isActive = true
        messageIndicatorImageView.rightAnchor.constraint(equalTo: rightAnchor, constant: -12).isActive = true
        messageIndicatorImageView.heightAnchor.constraint(equalToConstant: 11).isActive = true
        messageIndicatorImageView.widthAnchor.constraint(equalTo: messageIndicatorImageView.heightAnchor, multiplier: 1.0).isActive = true
        
        addSubview(dateLabel)
        dateLabel.topAnchor.constraint(equalTo: messageIndicatorImageView.bottomAnchor, constant: 6).isActive = true
        dateLabel.rightAnchor.constraint(equalTo: messageIndicatorImageView.rightAnchor).isActive = true
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
