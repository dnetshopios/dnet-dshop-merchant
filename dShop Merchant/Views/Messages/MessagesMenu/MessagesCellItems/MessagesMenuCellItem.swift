//
//  MessagesMenuCellItem.swift
//  dShop Merchant
//
//  Created by Marknel Pogi on 11/23/18.
//  Copyright © 2018 Marknel Pogi. All rights reserved.
//

import UIKit

class MessagesMenuCellItem: UICollectionViewCell {
    var messagesMenuItem: MessagesMenuItem? {
        didSet {
            if let title = messagesMenuItem?.menuTitle {
                menuTitleLabel.text = title
            }
            
            if let notifCount = messagesMenuItem?.notificationCount {
                notificationCountLabel.text = notifCount
            }
        }
    }
    
    let menuTitleLabel: UILabel = {
        let lbl = UILabel()
        lbl.font = UIFont(name: "SFUIDisplay-Medium", size: 21)
        lbl.textColor = UIColor(red: 214/255, green: 214/255, blue: 214/255, alpha: 1)
        lbl.translatesAutoresizingMaskIntoConstraints = false
        return lbl
    }()
    
    let notificationCountLabel: UILabel = {
        let lbl = UILabel()
        lbl.font = UIFont(name: "SFUIDisplay-Light", size: 18)
        lbl.textColor = UIColor(red: 167/255, green: 166/255, blue: 166/255, alpha: 1)
        lbl.translatesAutoresizingMaskIntoConstraints = false
        return lbl
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupViews()
    }
    
    private func setupViews() {
        addSubview(menuTitleLabel)
        menuTitleLabel.centerYAnchor.constraint(equalTo: centerYAnchor).isActive = true
        menuTitleLabel.leftAnchor.constraint(equalTo: leftAnchor).isActive = true
        
        addSubview(notificationCountLabel)
        notificationCountLabel.centerYAnchor.constraint(equalTo: centerYAnchor).isActive = true
        notificationCountLabel.rightAnchor.constraint(equalTo: rightAnchor).isActive = true
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
