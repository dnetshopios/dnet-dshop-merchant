//
//  MessagesMenuTagCell.swift
//  dShop Merchant
//
//  Created by Marknel Pogi on 11/27/18.
//  Copyright © 2018 Marknel Pogi. All rights reserved.
//

import UIKit

class MessagesMenuTagCell: UICollectionViewCell {
    var messageTag: MessagesTag? {
        didSet {
            if let name = messageTag?.tagName {
                tagTitleLabel.text = name
            }
            
            if let color = messageTag?.tagColor {
                colorView.backgroundColor = color
            }
        }
    }
    
    let tagTitleLabel: UILabel = {
        let lbl = UILabel()
        lbl.font = UIFont(name: "SFUIDisplay-Medium", size: 21)
        lbl.textColor = UIColor(red: 214/255, green: 214/255, blue: 214/255, alpha: 1)
        lbl.translatesAutoresizingMaskIntoConstraints = false
        return lbl
    }()
    
    let colorView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.layer.cornerRadius = 6
        view.layer.masksToBounds = true
        return view
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupViews()
    }
    
    private func setupViews() {
        addSubview(colorView)
        colorView.centerYAnchor.constraint(equalTo: centerYAnchor).isActive = true
        colorView.leftAnchor.constraint(equalTo: leftAnchor).isActive = true
        colorView.heightAnchor.constraint(equalToConstant: 12).isActive = true
        colorView.widthAnchor.constraint(equalTo: colorView.heightAnchor, multiplier: 1.0).isActive = true
        
        addSubview(tagTitleLabel)
        tagTitleLabel.centerYAnchor.constraint(equalTo: centerYAnchor).isActive = true
        tagTitleLabel.leftAnchor.constraint(equalTo: colorView.rightAnchor, constant: 10).isActive = true
        tagTitleLabel.rightAnchor.constraint(equalTo: rightAnchor).isActive = true
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
