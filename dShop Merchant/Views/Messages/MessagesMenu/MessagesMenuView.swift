//
//  MessagesMenuView.swift
//  dShop Merchant
//
//  Created by Marknel Pogi on 11/10/18.
//  Copyright © 2018 Marknel Pogi. All rights reserved.
//

import UIKit

struct MessagesMenuItem {
    let menuTitle: String
    let notificationCount: String
}

class MessagesMenuView: UIView {
    var tags = [MessagesTag]() {
        didSet {
            collectionView.reloadData()
        }
    }
    
    var messagesMenuItems: [MessagesMenuItem] = [
        MessagesMenuItem(menuTitle: "Inbox", notificationCount: "5"),
        MessagesMenuItem(menuTitle: "Sent", notificationCount: ""),
        MessagesMenuItem(menuTitle: "Drafts", notificationCount: ""),
        MessagesMenuItem(menuTitle: "Trash", notificationCount: ""),
        MessagesMenuItem(menuTitle: "Important", notificationCount: ""),
        MessagesMenuItem(menuTitle: "Spam", notificationCount: "2"),
        MessagesMenuItem(menuTitle: "Starred", notificationCount: ""),
    ]
    
    var messagesMenuDelegate: MessagesMenuDelegate?
    
    lazy var closeButton: UIButton = {
        let btn = UIButton(type: .system)
        btn.setImage(UIImage(named: "close")?.withRenderingMode(.alwaysOriginal), for: .normal)
        btn.addTarget(self, action: #selector(handleCloseMenu), for: .touchUpInside)
        btn.translatesAutoresizingMaskIntoConstraints = false
        return btn
    }()
    
    @objc func handleCloseMenu() {
        messagesMenuDelegate?.doCloseMenu()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupViews()
    }
    
    lazy var collectionView: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        let cv = UICollectionView(frame: .zero, collectionViewLayout: layout)
        cv.translatesAutoresizingMaskIntoConstraints = false
        cv.backgroundColor = .clear
        cv.delegate = self
        cv.dataSource = self
        return cv
    }()
    
    
    let cellId = "cellId"
    let messagesCellId = "messagesCellId"
    let messagesMenuTagsCellId = "messagesMenuTagsCellId"
    
    private func setupViews() {
        backgroundColor = .clear
        
        addSubview(closeButton)
        if #available(iOS 11.0, *) {
            closeButton.topAnchor.constraint(equalTo: safeAreaLayoutGuide.topAnchor, constant: 14).isActive = true
        } else {
            // Fallback on earlier versions
            closeButton.topAnchor.constraint(equalTo: topAnchor, constant: 14).isActive = true
        }
        
        closeButton.leftAnchor.constraint(equalTo: leftAnchor, constant: 24).isActive = true
        closeButton.heightAnchor.constraint(equalToConstant: 24).isActive = true
        closeButton.widthAnchor.constraint(equalTo: closeButton.heightAnchor, constant: 1.0).isActive = true
        
        collectionView.register(UICollectionViewCell.self, forCellWithReuseIdentifier: cellId)
        collectionView.register(MessagesMenuCell.self, forCellWithReuseIdentifier: messagesCellId)
        collectionView.register(MessagesMenuTagsCell.self, forCellWithReuseIdentifier: messagesMenuTagsCellId)
        addSubview(collectionView)
        collectionView.topAnchor.constraint(equalTo: closeButton.bottomAnchor, constant: 10).isActive = true
        collectionView.leftAnchor.constraint(equalTo: leftAnchor).isActive = true
        collectionView.rightAnchor.constraint(equalTo: rightAnchor).isActive = true
        collectionView.bottomAnchor.constraint(equalTo: bottomAnchor).isActive = true
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

// MARK: CollectionView
extension MessagesMenuView: UICollectionViewDelegateFlowLayout, UICollectionViewDelegate, UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 2
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if indexPath.row == 0 {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: messagesCellId, for: indexPath) as! MessagesMenuCell
            cell.messagesMenuItems = messagesMenuItems
            return cell
        } else if indexPath.row == 1 {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: messagesMenuTagsCellId, for: indexPath) as! MessagesMenuTagsCell
            cell.tags = tags
            return cell
        }
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellId, for: indexPath)
        return cell
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 24
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if indexPath.row == 0 {
            return CGSize(width: frame.width - 96, height: 270)
        } else if indexPath.row == 1 {
            if tags.count == 0 {
                return .zero
            }
            
            /*
             cell margin top
             cell title
             cell margin after cell
             cell size multiply by no. of tags
             cell margin bottom
            */
            
            let cellHeaderHeight = 60
            let cellTitleMargin = 10
            let cellHeight = 30
            let cellMarginBetweenCell = 10
            let cellSize: CGFloat = CGFloat(((cellHeight + cellMarginBetweenCell) * tags.count) + cellHeaderHeight + cellTitleMargin)
            return CGSize(width: frame.width - 96, height: cellSize)
        }
        
        return .zero
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 0, left: 48, bottom: 0, right: 48)
    }
}
