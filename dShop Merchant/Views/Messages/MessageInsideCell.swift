//
//  MessageInsideCell.swift
//  dShop Merchant
//
//  Created by Marknel Pogi on 12/3/18.
//  Copyright © 2018 Marknel Pogi. All rights reserved.
//

import UIKit

class MessageInsideCell: UICollectionViewCell {
    var message: Message? {
        didSet {
            guard let hmtlString = message?.messageContent else { return }
            guard let attributedString = try? NSAttributedString(htmlString: hmtlString, font: UIFont(name: "SourceSansPro-Regular", size: 14), useDocumentFontSize: false) else { return }
            
            // change color
            let mutableAttributedString = NSMutableAttributedString(attributedString: attributedString)
            mutableAttributedString.addAttribute(NSAttributedString.Key.foregroundColor, value: UIColor.white, range: NSRange(location: 0, length: attributedString.length))
            
            messageLabel.attributedText = mutableAttributedString
        }
    }
    
    let messageLabel: UILabel = {
        let lbl = UILabel()
        lbl.font = UIFont(name: "SourceSansPro-Regular", size: 14)
        lbl.textColor = UIColor(red: 200/255, green: 200/255, blue: 200/255, alpha: 1)
        lbl.numberOfLines = -1
        lbl.translatesAutoresizingMaskIntoConstraints = false
        return lbl
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupViews()
    }
    
    private func setupViews() {
        addSubview(messageLabel)
        messageLabel.topAnchor.constraint(equalTo: topAnchor).isActive = true
        messageLabel.leftAnchor.constraint(equalTo: leftAnchor, constant: 29).isActive = true
        messageLabel.rightAnchor.constraint(equalTo: rightAnchor, constant: -29).isActive = true
        messageLabel.bottomAnchor.constraint(equalTo: bottomAnchor).isActive = true
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
