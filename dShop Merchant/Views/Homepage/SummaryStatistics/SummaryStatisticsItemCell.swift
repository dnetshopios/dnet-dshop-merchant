//
//  SummaryStatisticsItemCell.swift
//  dShop Merchant
//
//  Created by Marknel Pogi on 11/6/18.
//  Copyright © 2018 Marknel Pogi. All rights reserved.
//

import UIKit

class SummaryStatisticsItemCell: UICollectionViewCell {
    var statistic: SummaryStatistic? {
        didSet {
            if let index = statistic?.index {
                if index == 0 || index == 1 {
                    summaryIconImageView.image = UIImage(named: "sales_gradient")
                    if index == 0 {
                        chartImageView.image = UIImage(named: "bar-chart-yellow")
                    } else {
                        chartImageView.image = UIImage(named: "bar-chart-green")
                    }
                } else if index == 2 {
                    summaryIconImageView.image = UIImage(named: "cart_gradient")
                    chartImageView.image = UIImage(named: "bar-chart-blue")
                } else if index == 3 {
                    summaryIconImageView.image = UIImage(named: "p_gradient")
                    chartImageView.image = UIImage(named: "bar-chart-red")
                }
            }
            
            if let title = statistic?.title {
                titleLabel.text = title
            }
            
            if let percentage = statistic?.percentage {
                if percentage < 0 {
                    arrowImageView.image = UIImage(named: "down_arrow")
                    percentageLabel.textColor = .red
                } else {
                    arrowImageView.image = UIImage(named: "up_arrow")
                    percentageLabel.textColor = .green
                }
                
                percentageLabel.text = "\(percentage)%"
            }
            
            if let currencySymbol = statistic?.currencySymbol {
                currencySymbolLabel.text = currencySymbol
            }
            
            if let value = statistic?.value {
                valueLabel.text = value
            }
            
        }
    }
    
    let summaryIconImageView: UIImageView = {
        let iv = UIImageView()
        iv.contentMode = .scaleAspectFit
        iv.translatesAutoresizingMaskIntoConstraints = false
        return iv
    }()
    
    let titleLabel: UILabel = {
        let lbl = UILabel()
        lbl.font = UIFont(name: "SFUIDisplay-Bold", size: 12)
        lbl.textColor = .white
        lbl.translatesAutoresizingMaskIntoConstraints = false
        return lbl
    }()
    
    let currencySymbolLabel: UILabel = {
        let lbl = UILabel()
        lbl.font = UIFont(name: "SFUIDisplay-Bold", size: 17)
        lbl.textColor = UIColor(red: 255/255, green: 179/255, blue: 0/255, alpha: 1)
        lbl.translatesAutoresizingMaskIntoConstraints = false
        return lbl
    }()
    
    let valueLabel: UILabel = {
        let lbl = UILabel()
        lbl.font = UIFont(name: "SFUIDisplay-Bold", size: 17)
        lbl.textColor = .white
        lbl.translatesAutoresizingMaskIntoConstraints = false
        return lbl
    }()
    
    let arrowImageView: UIImageView = {
        let iv = UIImageView()
        iv.contentMode = .scaleAspectFit
        iv.translatesAutoresizingMaskIntoConstraints = false
        return iv
    }()
    
    let percentageLabel: UILabel = {
        let lbl = UILabel()
        lbl.textColor = .white
        lbl.font = UIFont(name: "SFUIDisplay-Bold", size: 8)
        lbl.translatesAutoresizingMaskIntoConstraints = false
        return lbl
    }()
    
    let chartImageView: UIImageView = {
        let iv = UIImageView()
        iv.translatesAutoresizingMaskIntoConstraints = false
        return iv
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupViews()
    }
    
    private func setupViews() {
        layer.cornerRadius = 6
        layer.masksToBounds = true
        backgroundColor = UIColor(red: 2/255, green: 49/255, blue: 112/255, alpha: 1)
        
        addSubview(summaryIconImageView)
        summaryIconImageView.topAnchor.constraint(equalTo: topAnchor, constant: 10).isActive = true
        summaryIconImageView.leftAnchor.constraint(equalTo: leftAnchor, constant: 10).isActive = true
        summaryIconImageView.heightAnchor.constraint(equalToConstant: 24).isActive = true
        summaryIconImageView.widthAnchor.constraint(equalTo: summaryIconImageView.widthAnchor).isActive = true
        
        addSubview(titleLabel)
        titleLabel.centerYAnchor.constraint(equalTo: summaryIconImageView.centerYAnchor).isActive = true
        titleLabel.leftAnchor.constraint(equalTo: summaryIconImageView.rightAnchor, constant: 2).isActive = true
        
        addSubview(chartImageView)
        chartImageView.rightAnchor.constraint(equalTo: rightAnchor, constant: -10).isActive = true
        chartImageView.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -10).isActive = true
        chartImageView.heightAnchor.constraint(equalToConstant: 27).isActive = true
        chartImageView.widthAnchor.constraint(equalToConstant: 76).isActive = true
        
        addSubview(arrowImageView)
        arrowImageView.leftAnchor.constraint(equalTo: leftAnchor, constant: 12).isActive = true
        arrowImageView.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -12).isActive = true
        arrowImageView.heightAnchor.constraint(equalToConstant: 10).isActive = true
        arrowImageView.widthAnchor.constraint(equalTo: arrowImageView.heightAnchor, multiplier: 1.0).isActive = true
        
        addSubview(percentageLabel)
        percentageLabel.leftAnchor.constraint(equalTo: arrowImageView.rightAnchor, constant: 2).isActive = true
        percentageLabel.bottomAnchor.constraint(equalTo: arrowImageView.bottomAnchor, constant: -2).isActive = true
        
        addSubview(currencySymbolLabel)
        currencySymbolLabel.leftAnchor.constraint(equalTo: summaryIconImageView.leftAnchor, constant: 4).isActive = true
        currencySymbolLabel.bottomAnchor.constraint(equalTo: arrowImageView.topAnchor, constant: -10).isActive = true
        
        addSubview(valueLabel)
        valueLabel.bottomAnchor.constraint(equalTo: arrowImageView.topAnchor, constant: -10).isActive = true
        valueLabel.leftAnchor.constraint(equalTo: currencySymbolLabel.rightAnchor).isActive = true
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
