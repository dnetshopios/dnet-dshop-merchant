//
//  ActivityDetailsCell.swift
//  dShop Merchant
//
//  Created by Marknel Pogi on 11/8/18.
//  Copyright © 2018 Marknel Pogi. All rights reserved.
//

import UIKit

class ActivityDetailsCell: UICollectionViewCell {
    let imageView: UIImageView = {
        let iv = UIImageView()
        iv.image = UIImage(named: "placeholder-image")
        iv.contentMode = .scaleAspectFill
        iv.layer.cornerRadius = 20
        iv.layer.masksToBounds = true
        iv.translatesAutoresizingMaskIntoConstraints = false
        return iv
    }()
    
    let separatorView: UIView = {
        let view = UIView()
        view.backgroundColor = .lightGray
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    let valueLabel: UILabel = {
        let lbl = UILabel()
        lbl.text = "120 Orders"
        lbl.textColor = .white
        lbl.font = UIFont(name: "SFUIDisplay-Medium", size: 12)
        lbl.translatesAutoresizingMaskIntoConstraints = false
        return lbl
    }()
    
    let descriptionLabel: UILabel = {
        let lbl = UILabel()
        lbl.text = "Last 24 Hours"
        lbl.textColor = UIColor(red: 172/255, green: 172/255, blue: 172/255, alpha: 1)
        lbl.font = UIFont(name: "SFUIDisplay-Medium", size: 9)
        lbl.translatesAutoresizingMaskIntoConstraints = false
        return lbl
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupViews()
    }
    
    private func setupViews() {
        addSubview(imageView)
        imageView.centerYAnchor.constraint(equalTo: centerYAnchor).isActive = true
        imageView.leftAnchor.constraint(equalTo: leftAnchor, constant: 2).isActive = true
        imageView.heightAnchor.constraint(equalToConstant: 40).isActive = true
        imageView.widthAnchor.constraint(equalTo: imageView.heightAnchor, multiplier: 1.0).isActive = true
        
        addSubview(separatorView)
        separatorView.bottomAnchor.constraint(equalTo: bottomAnchor).isActive = true
        separatorView.leftAnchor.constraint(equalTo: leftAnchor, constant: 72).isActive = true
        separatorView.rightAnchor.constraint(equalTo: rightAnchor).isActive = true
        separatorView.heightAnchor.constraint(equalToConstant: 0.5).isActive = true
        
        addSubview(valueLabel)
        valueLabel.centerYAnchor.constraint(equalTo: centerYAnchor).isActive = true
        valueLabel.leftAnchor.constraint(equalTo: separatorView.leftAnchor).isActive = true
        
        addSubview(descriptionLabel)
        descriptionLabel.rightAnchor.constraint(equalTo: separatorView.rightAnchor, constant: -2).isActive = true
        descriptionLabel.topAnchor.constraint(equalTo: valueLabel.topAnchor).isActive = true
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
