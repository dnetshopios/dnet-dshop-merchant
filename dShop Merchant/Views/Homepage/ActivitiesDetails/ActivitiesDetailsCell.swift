//
//  ActivitiesDetailsCell.swift
//  dShop Merchant
//
//  Created by Marknel Pogi on 11/8/18.
//  Copyright © 2018 Marknel Pogi. All rights reserved.
//

import UIKit

struct ActivityDetails {
    let index: Int
    let value: String
    let description: String
}

class ActivitiesDetailsCell: UICollectionViewCell {
    var activitiesDetails = [ActivityDetails]()
    
    let titleLabel: UILabel = {
        let lbl = UILabel()
        lbl.text = "Activities Details"
        lbl.font = UIFont(name: "SFUIDisplay-Bold", size: 12)
        lbl.textColor = .white
        lbl.translatesAutoresizingMaskIntoConstraints = false
        return lbl
    }()
    
    let separatorView: UIView = {
        let view = UIView()
        view.backgroundColor = .lightGray
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    lazy var collectionView: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        let cv = UICollectionView(frame: .zero, collectionViewLayout: layout)
        cv.translatesAutoresizingMaskIntoConstraints = false
        cv.isScrollEnabled = false
        cv.backgroundColor = .clear
        cv.delegate = self
        cv.dataSource = self
        return cv
    }()
    
    let cellId = "cellId"

    override init(frame: CGRect) {
        super.init(frame: frame)
        setupViews()
    }
    
    private func setupViews() {
        layer.cornerRadius = 6
        layer.masksToBounds = true
        backgroundColor = UIColor(red: 2/255, green: 49/255, blue: 112/255, alpha: 1)
        
        addSubview(titleLabel)
        titleLabel.topAnchor.constraint(equalTo: topAnchor, constant: 12).isActive = true
        titleLabel.leftAnchor.constraint(equalTo: leftAnchor, constant: 12).isActive = true
        
        addSubview(separatorView)
        separatorView.topAnchor.constraint(equalTo: titleLabel.bottomAnchor, constant: 10).isActive = true
        separatorView.leftAnchor.constraint(equalTo: leftAnchor, constant: 12).isActive = true
        separatorView.rightAnchor.constraint(equalTo: rightAnchor, constant: -12).isActive = true
        separatorView.heightAnchor.constraint(equalToConstant: 0.5).isActive = true
        
        collectionView.register(ActivityDetailsCell.self, forCellWithReuseIdentifier: cellId)
        addSubview(collectionView)
        collectionView.topAnchor.constraint(equalTo: separatorView.bottomAnchor, constant: 12).isActive = true
        collectionView.leftAnchor.constraint(equalTo: leftAnchor, constant: 12).isActive = true
        collectionView.rightAnchor.constraint(equalTo: rightAnchor, constant: -12).isActive = true
        collectionView.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -12).isActive = true
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

// MARK: CollectionView
extension ActivitiesDetailsCell: UICollectionViewDelegateFlowLayout, UICollectionViewDelegate, UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 6
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellId, for: indexPath) as! ActivityDetailsCell
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.frame.width, height: 64)
    }
}
