//
//  MonthlyStatisticsItemCell.swift
//  dShop Merchant
//
//  Created by Marknel Pogi on 11/7/18.
//  Copyright © 2018 Marknel Pogi. All rights reserved.
//

import UIKit

class MonthlyStatisticsItemCell: UICollectionViewCell {
    let separatorView: UIView = {
        let view = UIView()
        view.backgroundColor = .white
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    let stackView: UIStackView = {
        let sv = UIStackView()
        sv.axis = .horizontal
        sv.layoutMargins = UIEdgeInsets(top: 0, left: 10, bottom: 0, right: 0)
        sv.isLayoutMarginsRelativeArrangement = true
        sv.distribution = .fillEqually
        sv.translatesAutoresizingMaskIntoConstraints = false
        return sv
    }()
    
    let monthLabel: UILabel = {
        let lbl = UILabel()
        lbl.text = "January"
        lbl.textColor = .white
        lbl.font = UIFont(name: "SFUIDisplay-Semibold", size: 12)
        lbl.translatesAutoresizingMaskIntoConstraints = false
        return lbl
    }()
    
    let salesLabel: UILabel = {
        let lbl = UILabel()
        lbl.text = "15,000"
        lbl.textColor = .white
        lbl.font = UIFont(name: "SFUIDisplay-Semibold", size: 12)
        lbl.translatesAutoresizingMaskIntoConstraints = false
        return lbl
    }()
    
        let salesCurrencySymbolLabel: UILabel = {
            let lbl = UILabel()
            lbl.text = "₱"
            lbl.font = UIFont(name: "SFUIDisplay-Bold", size: 12)
            lbl.textColor = UIColor(red: 255/255, green: 179/255, blue: 0/255, alpha: 1)
            lbl.translatesAutoresizingMaskIntoConstraints = false
            return lbl
        }()
    
        let salesPecentageLabel: UILabel = {
            let lbl = UILabel()
            lbl.text = "13.8%"
            lbl.textColor = .white
            lbl.font = UIFont(name: "SFUIDisplay-Semibold", size: 8)
            lbl.translatesAutoresizingMaskIntoConstraints = false
            return lbl
        }()
    
        let salesArrowImageView: UIImageView = {
            let iv = UIImageView()
            iv.image = UIImage(named: "up_arrow")
            iv.contentMode = .scaleAspectFit
            iv.translatesAutoresizingMaskIntoConstraints = false
            return iv
        }()
    
        let salesPercentageLabel: UILabel = {
            let lbl = UILabel()
            lbl.text = "13.8%"
            lbl.textColor = .white
            lbl.font = UIFont(name: "SFUIDisplay-Bold", size: 8)
            lbl.translatesAutoresizingMaskIntoConstraints = false
            return lbl
        }()
    
        let salesGraphImageView: UIImageView = {
            let iv = UIImageView()
            iv.image = UIImage(named: "graph-green")
            iv.contentMode = .scaleAspectFit
            iv.translatesAutoresizingMaskIntoConstraints = false
            return iv
        }()
    
    let productSoldLabel: UILabel = {
        let lbl = UILabel()
        lbl.text = "1,002"
        lbl.textColor = .white
        lbl.font = UIFont(name: "SFUIDisplay-Semibold", size: 12)
        lbl.translatesAutoresizingMaskIntoConstraints = false
        return lbl
    }()
    
        let productSoldItemsLabel: UILabel = {
            let lbl = UILabel()
            lbl.text = "items"
            lbl.textColor = UIColor(red: 93/255, green: 93/255, blue: 93/255, alpha: 1)
            lbl.font = UIFont(name: "SFUIDisplay-Semibold", size: 10)
            lbl.translatesAutoresizingMaskIntoConstraints = false
            return lbl
        }()
    
        let productSoldImageView: UIImageView = {
            let iv = UIImageView()
            iv.image = UIImage(named: "down_arrow")
            iv.contentMode = .scaleAspectFit
            iv.translatesAutoresizingMaskIntoConstraints = false
            return iv
        }()
    
        let productSoldPercentageLabel: UILabel = {
            let lbl = UILabel()
            lbl.text = "13.8%"
            lbl.textColor = .white
            lbl.font = UIFont(name: "SFUIDisplay-Bold", size: 8)
            lbl.translatesAutoresizingMaskIntoConstraints = false
            return lbl
        }()
    
        let productSoldGraphImageView: UIImageView = {
            let iv = UIImageView()
            iv.image = UIImage(named: "graph-red")
            iv.contentMode = .scaleAspectFit
            iv.translatesAutoresizingMaskIntoConstraints = false
            return iv
        }()
    
    let salesView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    let productSoldView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupViews()
    }
    
    private func setupViews() {
        addSubview(separatorView)
        separatorView.bottomAnchor.constraint(equalTo: bottomAnchor).isActive = true
        separatorView.leftAnchor.constraint(equalTo: leftAnchor).isActive = true
        separatorView.rightAnchor.constraint(equalTo: rightAnchor).isActive = true
        separatorView.heightAnchor.constraint(equalToConstant: 0.5).isActive = true
        
        addSubview(stackView)
        stackView.addArrangedSubview(monthLabel)
        stackView.addArrangedSubview(salesView)
        stackView.addArrangedSubview(productSoldView)
        
        setupSalesView()
        setupSoldProduct()
        
        stackView.topAnchor.constraint(equalTo: topAnchor).isActive = true
        stackView.leftAnchor.constraint(equalTo: leftAnchor).isActive = true
        stackView.rightAnchor.constraint(equalTo: rightAnchor).isActive = true
        stackView.bottomAnchor.constraint(equalTo: bottomAnchor).isActive = true
    }
    
    private func setupSalesView() {
        salesView.addSubview(salesCurrencySymbolLabel)
        salesCurrencySymbolLabel.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -8).isActive = true
        salesCurrencySymbolLabel.leftAnchor.constraint(equalTo: salesView.leftAnchor, constant: 6).isActive = true
        
        salesView.addSubview(salesLabel)
        salesLabel.bottomAnchor.constraint(equalTo: salesCurrencySymbolLabel.bottomAnchor).isActive = true
        salesLabel.leftAnchor.constraint(equalTo: salesCurrencySymbolLabel.rightAnchor, constant: 4).isActive = true
        
        salesView.addSubview(salesArrowImageView)
        salesArrowImageView.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -24).isActive = true
        salesArrowImageView.leftAnchor.constraint(equalTo: salesView.leftAnchor).isActive = true
        salesArrowImageView.heightAnchor.constraint(equalToConstant: 10).isActive = true
        salesArrowImageView.widthAnchor.constraint(equalTo: salesArrowImageView.heightAnchor, multiplier: 1.0).isActive = true
        
        salesView.addSubview(salesPercentageLabel)
        salesPercentageLabel.topAnchor.constraint(equalTo: salesArrowImageView.topAnchor).isActive = true
        salesPercentageLabel.leftAnchor.constraint(equalTo: salesArrowImageView.rightAnchor, constant: 2).isActive = true
        salesPercentageLabel.textColor = .green
        
        salesView.addSubview(salesGraphImageView)
        salesGraphImageView.topAnchor.constraint(equalTo: salesPercentageLabel.topAnchor, constant: -2).isActive = true
        salesGraphImageView.leftAnchor.constraint(equalTo: salesPercentageLabel.leftAnchor, constant: 25).isActive = true
        salesGraphImageView.heightAnchor.constraint(equalToConstant: 13).isActive = true
        salesGraphImageView.widthAnchor.constraint(equalToConstant: 35).isActive = true
    }
    
    private func setupSoldProduct() {
        productSoldView.addSubview(productSoldLabel)
        productSoldLabel.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -8).isActive = true
        productSoldLabel.leftAnchor.constraint(equalTo: productSoldView.leftAnchor, constant: 4).isActive = true
        
        productSoldView.addSubview(productSoldItemsLabel)
        productSoldItemsLabel.bottomAnchor.constraint(equalTo: productSoldLabel.bottomAnchor).isActive = true
        productSoldItemsLabel.leftAnchor.constraint(equalTo: productSoldLabel.rightAnchor, constant: 4).isActive = true
        
        productSoldView.addSubview(productSoldImageView)
        productSoldImageView.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -24).isActive = true
        productSoldImageView.leftAnchor.constraint(equalTo: productSoldView.leftAnchor).isActive = true
        productSoldImageView.heightAnchor.constraint(equalToConstant: 10).isActive = true
        productSoldImageView.widthAnchor.constraint(equalTo: productSoldImageView.heightAnchor, multiplier: 1.0).isActive = true
        
        salesView.addSubview(productSoldPercentageLabel)
        productSoldPercentageLabel.topAnchor.constraint(equalTo: productSoldImageView.topAnchor).isActive = true
        productSoldPercentageLabel.leftAnchor.constraint(equalTo: productSoldImageView.rightAnchor, constant: 2).isActive = true
        productSoldPercentageLabel.textColor = .red
        
        salesView.addSubview(productSoldGraphImageView)
        productSoldGraphImageView.topAnchor.constraint(equalTo: productSoldPercentageLabel.topAnchor, constant: -2).isActive = true
        productSoldGraphImageView.leftAnchor.constraint(equalTo: productSoldPercentageLabel.leftAnchor, constant: 25).isActive = true
        productSoldGraphImageView.heightAnchor.constraint(equalToConstant: 13).isActive = true
        productSoldGraphImageView.widthAnchor.constraint(equalToConstant: 35).isActive = true
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
