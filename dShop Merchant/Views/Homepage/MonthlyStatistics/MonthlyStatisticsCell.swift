//
//  MonthlyStatisticsCell.swift
//  dShop Merchant
//
//  Created by Marknel Pogi on 11/6/18.
//  Copyright © 2018 Marknel Pogi. All rights reserved.
//

import UIKit

class MonthlyStatisticsCell: UICollectionViewCell {    
    let titleLabel: UILabel = {
        let lbl = UILabel()
        lbl.text = "Monthly Statistics"
        lbl.font = UIFont(name: "SFUIDisplay-Bold", size: 12)
        lbl.textColor = .white
        lbl.translatesAutoresizingMaskIntoConstraints = false
        return lbl
    }()
    
    let annualButton: UIButton = {
        let btn = UIButton(type: .system)
        btn.setImage(UIImage(named: "small-down")?.withRenderingMode(.alwaysOriginal), for: .normal)
        btn.imageView?.contentMode = .scaleAspectFit
        btn.semanticContentAttribute = .forceRightToLeft
        btn.imageEdgeInsets = UIEdgeInsets(top: 10, left: 30, bottom: 10, right: 8)
        btn.setTitle("Last 6 Months", for: .normal)
        btn.titleLabel?.font = UIFont(name: "SFUIDisplay-Medium", size: 8)
        btn.backgroundColor = .white
        btn.tintColor = UIColor(red: 58/255, green: 54/255, blue: 45/255, alpha: 1)
        btn.layer.cornerRadius = 13
        btn.layer.masksToBounds = true
        btn.layer.borderColor = UIColor(red: 215/255, green: 218/255, blue: 226/255, alpha: 1).cgColor
        btn.layer.borderWidth = 1
        btn.translatesAutoresizingMaskIntoConstraints = false
        return btn
    }()
    
    let headerView: MonthlyStatatisticsHeaderView = {
        let view = MonthlyStatatisticsHeaderView()
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    lazy var collectionView: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        let cv = UICollectionView(frame: .zero, collectionViewLayout: layout)
        cv.translatesAutoresizingMaskIntoConstraints = false
        cv.isScrollEnabled = false
        cv.backgroundColor = .clear
        cv.delegate = self
        cv.dataSource = self
        return cv
    }()
    
    let cellId = "cellId"
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupViews()
    }
    
    private func setupViews() {
        layer.cornerRadius = 6
        layer.masksToBounds = true
        backgroundColor = UIColor(red: 2/255, green: 49/255, blue: 112/255, alpha: 1)
        
        addSubview(annualButton)
        annualButton.topAnchor.constraint(equalTo: topAnchor, constant: 10).isActive = true
        annualButton.rightAnchor.constraint(equalTo: rightAnchor, constant: -12).isActive = true
        annualButton.heightAnchor.constraint(equalToConstant: 26).isActive = true
        annualButton.widthAnchor.constraint(equalToConstant: 110).isActive = true
        
        addSubview(titleLabel)
        titleLabel.leftAnchor.constraint(equalTo: leftAnchor, constant: 12).isActive = true
        titleLabel.centerYAnchor.constraint(equalTo: annualButton.centerYAnchor).isActive = true
        
        addSubview(headerView)
        headerView.topAnchor.constraint(equalTo: annualButton.bottomAnchor, constant: 10).isActive = true
        headerView.leftAnchor.constraint(equalTo: leftAnchor, constant: 12).isActive = true
        headerView.rightAnchor.constraint(equalTo: rightAnchor, constant: -12).isActive = true
        headerView.heightAnchor.constraint(equalToConstant: 32).isActive = true
        
        collectionView.register(MonthlyStatisticsItemCell.self, forCellWithReuseIdentifier: cellId)
        addSubview(collectionView)
        collectionView.topAnchor.constraint(equalTo: headerView.bottomAnchor).isActive = true
        collectionView.leftAnchor.constraint(equalTo: leftAnchor, constant: 12).isActive = true
        collectionView.rightAnchor.constraint(equalTo: rightAnchor, constant: -12).isActive = true
        collectionView.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -10).isActive = true
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

extension MonthlyStatisticsCell: UICollectionViewDelegateFlowLayout, UICollectionViewDelegate, UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 12
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellId, for: indexPath) as! MonthlyStatisticsItemCell
        
        if indexPath.row == 11 {
            cell.separatorView.alpha = 0
        }
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.frame.width, height: 46)
    }
}
