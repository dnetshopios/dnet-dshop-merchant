//
//  MonthlyStatatisticsHeaderView.swift
//  dShop Merchant
//
//  Created by Marknel Pogi on 11/7/18.
//  Copyright © 2018 Marknel Pogi. All rights reserved.
//

import UIKit

class MonthlyStatatisticsHeaderView: UIView {
//    lazy var collectionView: UICollectionView = {
//        let layout = UICollectionViewFlowLayout()
//        let cv = UICollectionView(frame: .zero, collectionViewLayout: layout)
//        cv.translatesAutoresizingMaskIntoConstraints = false
//        cv.isScrollEnabled = false
//        cv.backgroundColor = .clear
//        cv.delegate = self
//        cv.dataSource = self
//        return cv
//    }()
    
//    let cellId = "cellId"
    
    let stackView: UIStackView = {
        let sv = UIStackView()
        sv.axis = .horizontal
        sv.layoutMargins = UIEdgeInsets(top: 0, left: 10, bottom: 0, right: 0)
        sv.isLayoutMarginsRelativeArrangement = true
        sv.distribution = .fillEqually
        sv.translatesAutoresizingMaskIntoConstraints = false
        return sv
    }()
    
    let monthLabel: UILabel = {
        let lbl = UILabel()
        lbl.text = "Month"
        lbl.textColor = .white
        lbl.font = UIFont(name: "SFUIDisplay-Semibold", size: 12)
        lbl.translatesAutoresizingMaskIntoConstraints = false
        return lbl
    }()
    
    let salesLabel: UILabel = {
        let lbl = UILabel()
        lbl.text = "Sales"
        lbl.textColor = .white
        lbl.font = UIFont(name: "SFUIDisplay-Semibold", size: 12)
        lbl.translatesAutoresizingMaskIntoConstraints = false
        return lbl
    }()
    
    let productSoldLabel: UILabel = {
        let lbl = UILabel()
        lbl.text = "Product Sold"
        lbl.textColor = .white
        lbl.font = UIFont(name: "SFUIDisplay-Semibold", size: 12)
        lbl.translatesAutoresizingMaskIntoConstraints = false
        return lbl
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupViews()
    }
    
    private func setupViews() {
        backgroundColor = UIColor.white.withAlphaComponent(0.1)
        layer.cornerRadius = 6
        layer.masksToBounds = true
        
        stackView.addArrangedSubview(monthLabel)
        stackView.addArrangedSubview(salesLabel)
        stackView.addArrangedSubview(productSoldLabel)
        addSubview(stackView)
        stackView.topAnchor.constraint(equalTo: topAnchor).isActive = true
        stackView.leftAnchor.constraint(equalTo: leftAnchor).isActive = true
        stackView.rightAnchor.constraint(equalTo: rightAnchor).isActive = true
        stackView.bottomAnchor.constraint(equalTo: bottomAnchor).isActive = true
        
//        collectionView.register(MonthlyStatisticsHeaderCell.self, forCellWithReuseIdentifier: cellId)
//        addSubview(collectionView)
//        collectionView.topAnchor.constraint(equalTo: topAnchor).isActive = true
//        collectionView.leftAnchor.constraint(equalTo: leftAnchor).isActive = true
//        collectionView.rightAnchor.constraint(equalTo: rightAnchor).isActive = true
//        collectionView.bottomAnchor.constraint(equalTo: bottomAnchor).isActive = true
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

//extension MonthlyStatatisticsHeaderView: UICollectionViewDelegateFlowLayout, UICollectionViewDelegate, UICollectionViewDataSource {
//    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
//        return 3
//    }
//
//    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
//        return 0
//    }
//
//    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
//        return 0
//    }
//
//    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
//        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellId, for: indexPath) as! MonthlyStatisticsHeaderCell
//        let titles: [String] = ["Month", "Sold", "Product Sold"]
//        cell.headerTitle = titles[indexPath.row]
//        return cell
//    }
//
//    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
//        return CGSize(width: frame.width / 3, height: frame.height)
//    }
//}
