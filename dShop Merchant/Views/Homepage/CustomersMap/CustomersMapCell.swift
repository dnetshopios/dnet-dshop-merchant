//
//  CustomersMapCell.swift
//  dShop Merchant
//
//  Created by Marknel Pogi on 11/7/18.
//  Copyright © 2018 Marknel Pogi. All rights reserved.
//

import UIKit

class CustomersMapCell: UICollectionViewCell {
    let titleLabel: UILabel = {
        let lbl = UILabel()
        lbl.text = "Customers Map"
        lbl.font = UIFont(name: "SFUIDisplay-Bold", size: 12)
        lbl.textColor = .white
        lbl.translatesAutoresizingMaskIntoConstraints = false
        return lbl
    }()
    
    let showAllListButton: UIButton = {
        let btn = UIButton(type: .system)
        btn.setTitle("Show All List", for: .normal)
        btn.titleLabel?.font = UIFont(name: "SFUIDisplay-Medium", size: 12)
        btn.tintColor = UIColor(red: 184/255, green: 184/255, blue: 184/255, alpha: 1)
        btn.translatesAutoresizingMaskIntoConstraints = false
        return btn
    }()
    
    let mapImageView: CachedImageView = {
        let iv = CachedImageView()
        iv.image = UIImage(named: "world-map")
        iv.contentMode = .scaleAspectFill
        iv.translatesAutoresizingMaskIntoConstraints = false
        return iv
    }()
    
    lazy var collectionView: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        let cv = UICollectionView(frame: .zero, collectionViewLayout: layout)
        cv.translatesAutoresizingMaskIntoConstraints = false
        cv.isScrollEnabled = false
        cv.backgroundColor = .clear
        cv.delegate = self
        cv.dataSource = self
        return cv
    }()
    
    let cellId = "cellId"
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupViews()
    }
    
    private func setupViews() {
        layer.cornerRadius = 6
        layer.masksToBounds = true
        backgroundColor = UIColor(red: 2/255, green: 49/255, blue: 112/255, alpha: 1)
        
        addSubview(titleLabel)
        titleLabel.topAnchor.constraint(equalTo: topAnchor, constant: 12).isActive = true
        titleLabel.leftAnchor.constraint(equalTo: leftAnchor, constant: 12).isActive = true
        
        addSubview(showAllListButton)
        showAllListButton.topAnchor.constraint(equalTo: topAnchor, constant: 5).isActive = true
        showAllListButton.rightAnchor.constraint(equalTo: rightAnchor, constant: -12).isActive = true
        
        let mapViewAspectHeight = getAspectHeight(width: frame.width - 24)
        addSubview(mapImageView)
        mapImageView.topAnchor.constraint(equalTo: topAnchor, constant: 38).isActive = true
        mapImageView.leftAnchor.constraint(equalTo: leftAnchor, constant: 12).isActive = true
        mapImageView.rightAnchor.constraint(equalTo: rightAnchor, constant: -12).isActive = true
        mapImageView.heightAnchor.constraint(equalToConstant: mapViewAspectHeight).isActive = true
        
        collectionView.register(CustomersMapItemCell.self, forCellWithReuseIdentifier: cellId)
        addSubview(collectionView)
        collectionView.topAnchor.constraint(equalTo: mapImageView.bottomAnchor, constant: 10).isActive = true
        collectionView.leftAnchor.constraint(equalTo: leftAnchor, constant: 12).isActive = true
        collectionView.rightAnchor.constraint(equalTo: rightAnchor, constant: -12).isActive = true
        collectionView.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -12).isActive = true
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

extension CustomersMapCell: UICollectionViewDelegateFlowLayout, UICollectionViewDelegate, UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 6
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellId, for: indexPath) as! CustomersMapItemCell
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.frame.width, height: 40)
    }
}
