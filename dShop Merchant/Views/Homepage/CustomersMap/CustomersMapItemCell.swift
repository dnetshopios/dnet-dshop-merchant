//
//  CustomersMapItemCell.swift
//  dShop Merchant
//
//  Created by Marknel Pogi on 11/8/18.
//  Copyright © 2018 Marknel Pogi. All rights reserved.
//

import UIKit

class CustomersMapItemCell: UICollectionViewCell {
    let colorView: UIView = {
        let view = UIView()
        view.backgroundColor = .white
        view.layer.cornerRadius = 7
        view.layer.masksToBounds = true
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    let countryNameLabel: UILabel = {
        let lbl = UILabel()
        lbl.text = "Country Name"
        lbl.textColor = .white
        lbl.font = UIFont(name: "SFUIDisplay-Medium", size: 10)
        lbl.translatesAutoresizingMaskIntoConstraints = false
        return lbl
    }()
    
    let totalSalesLabel: UILabel = {
        let lbl = UILabel()
        lbl.text = "15,000"
        lbl.textColor = .white
        lbl.font = UIFont(name: "SFUIDisplay-Medium", size: 10)
        lbl.translatesAutoresizingMaskIntoConstraints = false
        return lbl
    }()
    
    let currencySymbolLabel: UILabel = {
        let lbl = UILabel()
        lbl.text = "₱"
        lbl.font = UIFont(name: "SFUIDisplay-Medium", size: 10)
        lbl.textColor = UIColor(red: 255/255, green: 179/255, blue: 0/255, alpha: 1)
        lbl.translatesAutoresizingMaskIntoConstraints = false
        return lbl
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupViews()
    }
    
    private func setupViews() {
        addSubview(colorView)
        colorView.centerYAnchor.constraint(equalTo: centerYAnchor).isActive = true
        colorView.leftAnchor.constraint(equalTo: leftAnchor, constant: 12).isActive = true
        colorView.heightAnchor.constraint(equalToConstant: 14).isActive = true
        colorView.widthAnchor.constraint(equalTo: colorView.heightAnchor, multiplier: 1.0).isActive = true
        
        addSubview(countryNameLabel)
        countryNameLabel.leftAnchor.constraint(equalTo: colorView.rightAnchor, constant: 8).isActive = true
        countryNameLabel.centerYAnchor.constraint(equalTo: centerYAnchor).isActive = true
        
        addSubview(totalSalesLabel)
        totalSalesLabel.rightAnchor.constraint(equalTo: rightAnchor, constant: -12).isActive = true
        totalSalesLabel.centerYAnchor.constraint(equalTo: centerYAnchor).isActive = true
        
        addSubview(currencySymbolLabel)
        currencySymbolLabel.rightAnchor.constraint(equalTo: totalSalesLabel.leftAnchor).isActive = true
        currencySymbolLabel.centerYAnchor.constraint(equalTo: centerYAnchor).isActive = true
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
