//
//  HomepageHeaderCell.swift
//  dShop Merchant
//
//  Created by Marknel Pogi on 11/6/18.
//  Copyright © 2018 Marknel Pogi. All rights reserved.
//

import UIKit

class HomepageHeaderCell: UICollectionViewCell {
    let titleLabel: UILabel = {
        let lbl = UILabel()
        lbl.text = "Hello SM Store !"
        lbl.font = UIFont(name: "SFUIDisplay-Light", size: 27)
        lbl.textColor = .white
        lbl.translatesAutoresizingMaskIntoConstraints = false
        return lbl
    }()
    
    let welcomeLabel: UILabel = {
        let lbl = UILabel()
        lbl.text = "Welcome to your Dashboard."
        lbl.font = UIFont(name: "SFUIDisplay-Light", size: 16)
        lbl.textColor = UIColor(red: 68/255, green: 161/255, blue: 247/255, alpha: 1)
        lbl.translatesAutoresizingMaskIntoConstraints = false
        return lbl
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupViews()
    }
    
    private func setupViews() {
        addSubview(welcomeLabel)
        welcomeLabel.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -10).isActive = true
        welcomeLabel.leftAnchor.constraint(equalTo: leftAnchor, constant: 16).isActive = true
        welcomeLabel.rightAnchor.constraint(equalTo: rightAnchor, constant: -16).isActive = true
        
        addSubview(titleLabel)
        titleLabel.bottomAnchor.constraint(equalTo: welcomeLabel.topAnchor).isActive = true
        titleLabel.leftAnchor.constraint(equalTo: welcomeLabel.leftAnchor).isActive = true
        titleLabel.rightAnchor.constraint(equalTo: welcomeLabel.rightAnchor).isActive = true
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
