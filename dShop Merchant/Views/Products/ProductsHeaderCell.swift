//
//  ProductsHeaderCell.swift
//  dShop Merchant
//
//  Created by Marknel Pogi on 11/9/18.
//  Copyright © 2018 Marknel Pogi. All rights reserved.
//

import UIKit

class ProductsHeaderCell: UICollectionViewCell {
    var productsDelegate: ProductsDelegate?
    
    lazy var addNewButton: UIButton = {
        let btn = UIButton(type: .system)
        btn.setTitle("Add New Product", for: .normal)
        btn.titleLabel?.font = UIFont(name: "SFUIDisplay-Bold", size: 16)
        btn.setImage(UIImage(named: "plus"), for: .normal)
        btn.tintColor = .white
        btn.layer.cornerRadius = 8
        btn.layer.masksToBounds = true
        btn.imageEdgeInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 10)
        btn.backgroundColor = UIColor(displayP3Red: 0/255, green: 55/255, blue: 138/255, alpha: 1)
        btn.translatesAutoresizingMaskIntoConstraints = false
        btn.addTarget(self, action: #selector(handleAddProduct), for: .touchUpInside)
        return btn
    }()
    
    @objc func handleAddProduct() {
        productsDelegate?.doAddProduct()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupViews()
    }
    
    private func setupViews() {
        addSubview(addNewButton)
        addNewButton.leftAnchor.constraint(equalTo: leftAnchor, constant: 16).isActive = true
        addNewButton.rightAnchor.constraint(equalTo: rightAnchor, constant: -16).isActive = true
        addNewButton.heightAnchor.constraint(equalToConstant: 40).isActive = true
        addNewButton.centerYAnchor.constraint(equalTo: centerYAnchor).isActive = true
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
