//
//  ProductCell.swift
//  dShop Merchant
//
//  Created by Marknel Pogi on 11/9/18.
//  Copyright © 2018 Marknel Pogi. All rights reserved.
//

import UIKit

class ProductCell: UICollectionViewCell {
    
    var productsDelegate: ProductsDelegate?
    
    let productRatingsView: ProductRatingsView = {
        let view = ProductRatingsView()
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    let productImageView: UIImageView = {
        let iv = UIImageView()
        iv.image = UIImage(named: "placeholder-image")
        iv.contentMode = .scaleAspectFill
        iv.layer.masksToBounds = true
        iv.translatesAutoresizingMaskIntoConstraints = false
        return iv
    }()
    
    let previewTextLabel: UILabel = {
        let lbl = UILabel()
        lbl.text = "KD Trey 5 Vl Shoes"
        lbl.font = UIFont(name: "SFUIDisplay-Semibold", size: 14)
        lbl.textColor = UIColor(red: 77/255, green: 79/255, blue: 92/255, alpha: 1)
        lbl.translatesAutoresizingMaskIntoConstraints = false
        return lbl
    }()
    
    let inStockView: UIView = {
        let view = UIView()
        view.layer.cornerRadius = 11
        view.layer.masksToBounds = true
        view.backgroundColor = UIColor(red: 105/255, green: 228/255, blue: 166/255, alpha: 0.2)
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    let inStockLabel: UILabel = {
        let lbl = UILabel()
        lbl.text = "320 in Stock"
        lbl.textColor = .black
        lbl.font = UIFont(name: "SourceSansPro-Bold", size: 9)
        lbl.translatesAutoresizingMaskIntoConstraints = false
        return lbl
    }()
    
    let bookmarkButton: UIButton = {
        let btn = UIButton(type: .system)
        btn.contentMode = .scaleAspectFill
        btn.setImage(UIImage(named: "bookmark")?.withRenderingMode(.alwaysOriginal), for: .normal)
        btn.translatesAutoresizingMaskIntoConstraints = false
        return btn
    }()
    
    lazy var editButton: UIButton = {
        let btn = UIButton(type: .system)
        btn.contentMode = .scaleAspectFill
        btn.setImage(UIImage(named: "edit-blue")?.withRenderingMode(.alwaysOriginal), for: .normal)
        btn.translatesAutoresizingMaskIntoConstraints = false
        btn.addTarget(self, action: #selector(handleEditProduct), for: .touchUpInside)
        return btn
    }()
    
    @objc func handleEditProduct() {
        productsDelegate?.doAddProduct()
    }
    
    let deleteButton: UIButton = {
        let btn = UIButton(type: .system)
        btn.contentMode = .scaleAspectFill
        btn.setImage(UIImage(named: "delete-red")?.withRenderingMode(.alwaysOriginal), for: .normal)
        btn.translatesAutoresizingMaskIntoConstraints = false
        return btn
    }()
    
    let currencySymbolLabel: UILabel = {
        let lbl = UILabel()
        lbl.text = "₱"
        lbl.font = UIFont(name: "SFUIDisplay-Medium", size: 16)
        lbl.textColor = UIColor(red: 255/255, green: 179/255, blue: 0/255, alpha: 1)
        lbl.translatesAutoresizingMaskIntoConstraints = false
        return lbl
    }()
    
    let valueLabel: UILabel = {
        let lbl = UILabel()
        lbl.text = "29,192"
        lbl.font = UIFont(name: "SFUIDisplay-Medium", size: 16)
        lbl.textColor = UIColor(red: 77/255, green: 79/255, blue: 92/255, alpha: 1)
        lbl.translatesAutoresizingMaskIntoConstraints = false
        return lbl
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupViews()
    }
    
    private func setupViews() {
        layer.cornerRadius = 10
        layer.masksToBounds = true
        backgroundColor = .white
        
        addSubview(productImageView)
        productImageView.topAnchor.constraint(equalTo: topAnchor, constant: 12).isActive = true
        productImageView.leftAnchor.constraint(equalTo: leftAnchor, constant: 12).isActive = true
        productImageView.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -12).isActive = true
        productImageView.widthAnchor.constraint(equalToConstant: 90).isActive = true
        
        addSubview(previewTextLabel)
        previewTextLabel.topAnchor.constraint(equalTo: topAnchor, constant: 16).isActive = true
        previewTextLabel.leftAnchor.constraint(equalTo: productImageView.rightAnchor, constant: 12).isActive = true
        
        addSubview(productRatingsView)
        productRatingsView.topAnchor.constraint(equalTo: previewTextLabel.bottomAnchor, constant: 4).isActive = true
        productRatingsView.leftAnchor.constraint(equalTo: previewTextLabel.leftAnchor).isActive = true
        productRatingsView.heightAnchor.constraint(equalToConstant: 16).isActive = true
        productRatingsView.widthAnchor.constraint(equalToConstant: 110).isActive = true
        
        addSubview(inStockView)
        inStockView.topAnchor.constraint(equalTo: productRatingsView.bottomAnchor, constant: 6).isActive = true
        inStockView.leftAnchor.constraint(equalTo: productRatingsView.leftAnchor).isActive = true
        inStockView.heightAnchor.constraint(equalToConstant: 22).isActive = true
        inStockView.widthAnchor.constraint(equalToConstant: 100).isActive = true
        
        addSubview(inStockLabel)
        inStockLabel.centerXAnchor.constraint(equalTo: inStockView.centerXAnchor).isActive = true
        inStockLabel.centerYAnchor.constraint(equalTo: inStockView.centerYAnchor).isActive = true
        
        addSubview(deleteButton)
        deleteButton.topAnchor.constraint(equalTo: topAnchor, constant: 14).isActive = true
        deleteButton.rightAnchor.constraint(equalTo: rightAnchor, constant: -10).isActive = true
        deleteButton.heightAnchor.constraint(equalToConstant: 20).isActive = true
        deleteButton.widthAnchor.constraint(equalToConstant: 20).isActive = true
        
        addSubview(editButton)
        editButton.topAnchor.constraint(equalTo: deleteButton.topAnchor).isActive = true
        editButton.rightAnchor.constraint(equalTo: deleteButton.leftAnchor, constant: -8).isActive = true
        editButton.heightAnchor.constraint(equalToConstant: 20).isActive = true
        editButton.widthAnchor.constraint(equalToConstant: 20).isActive = true

        addSubview(bookmarkButton)
        bookmarkButton.topAnchor.constraint(equalTo: editButton.topAnchor).isActive = true
        bookmarkButton.rightAnchor.constraint(equalTo: editButton.leftAnchor, constant: -8).isActive = true
        bookmarkButton.heightAnchor.constraint(equalToConstant: 20).isActive = true
        bookmarkButton.widthAnchor.constraint(equalToConstant: 20).isActive = true
        
        addSubview(valueLabel)
        valueLabel.rightAnchor.constraint(equalTo: rightAnchor, constant: -10).isActive = true
        valueLabel.bottomAnchor.constraint(equalTo: inStockView.bottomAnchor, constant: -2).isActive = true
        
        addSubview(currencySymbolLabel)
        currencySymbolLabel.topAnchor.constraint(equalTo: valueLabel.topAnchor).isActive = true
        currencySymbolLabel.rightAnchor.constraint(equalTo: valueLabel.leftAnchor).isActive = true
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
