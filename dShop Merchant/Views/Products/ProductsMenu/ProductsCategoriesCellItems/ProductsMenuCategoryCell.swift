//
//  ProductsMenuCategoryCell.swift
//  dShop Merchant
//
//  Created by Marknel Pogi on 11/27/18.
//  Copyright © 2018 Marknel Pogi. All rights reserved.
//

import UIKit

class ProductsMenuCategoryCell: UICollectionViewCell {
    var category: Category? {
        didSet {
            if let name = category?.categoryName {
                categoryNameLabel.text = name
            }
        }
    }
    
    let categoryNameLabel: UILabel = {
        let lbl = UILabel()
        lbl.font = UIFont(name: "SFUIDisplay-Medium", size: 14)
        lbl.textColor = UIColor(red: 161/255, green: 161/255, blue: 161/255, alpha: 1)
        lbl.translatesAutoresizingMaskIntoConstraints = false
        return lbl
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupViews()
    }
    
    private func setupViews() {
        addSubview(categoryNameLabel)
        categoryNameLabel.centerYAnchor.constraint(equalTo: centerYAnchor).isActive = true
        categoryNameLabel.leftAnchor.constraint(equalTo: leftAnchor).isActive = true
        categoryNameLabel.rightAnchor.constraint(equalTo: rightAnchor).isActive = true
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
