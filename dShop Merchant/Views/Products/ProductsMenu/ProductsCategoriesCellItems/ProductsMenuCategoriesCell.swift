//
//  ProductsMenuCategoriesCell.swift
//  dShop Merchant
//
//  Created by Marknel Pogi on 11/27/18.
//  Copyright © 2018 Marknel Pogi. All rights reserved.
//

import UIKit

class ProductsMenuCategoriesCell: UICollectionViewCell {
    var productsMenuDelegate: ProductsMenuDelegate?
    
    var categories = [Category]() {
        didSet {
            collectionView.reloadData()
        }
    }
    
    let cellId = "cellId"
    
    lazy var collectionView: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        let cv = UICollectionView(frame: .zero, collectionViewLayout: layout)
        cv.backgroundColor = .clear
        cv.delegate = self
        cv.dataSource = self
        cv.translatesAutoresizingMaskIntoConstraints  = false
        return cv
    }()
    
    let topView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    let tagTitleLabel: UILabel = {
        let lbl = UILabel()
        lbl.text = "Categories"
        lbl.font = UIFont(name: "SFUIDisplay-Bold", size: 21)
        lbl.textColor = .white
        lbl.translatesAutoresizingMaskIntoConstraints = false
        return lbl
    }()
    
    let separatorView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints  = false
        view.backgroundColor = .lightGray
        return view
    }()
    
    lazy var addButton: UIButton = {
        let btn = UIButton(type: .system)
        btn.setImage(UIImage(named: "plus")?.withRenderingMode(.alwaysTemplate), for: .normal)
        btn.tintColor = UIColor(red: 161/255, green: 161/255, blue: 161/255, alpha: 1)
        btn.translatesAutoresizingMaskIntoConstraints  = false
        btn.addTarget(self, action: #selector(handleAddCategory), for: .touchUpInside)
        return btn
    }()
    
    @objc func handleAddCategory() {
        productsMenuDelegate?.doShowAddCategory()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupViews()
    }
    
    private func setupViews() {
        addSubview(topView)
        topView.topAnchor.constraint(equalTo: topAnchor).isActive = true
        topView.leftAnchor.constraint(equalTo: leftAnchor).isActive = true
        topView.rightAnchor.constraint(equalTo: rightAnchor).isActive = true
        topView.heightAnchor.constraint(equalToConstant: 60).isActive = true
        
        addSubview(separatorView)
        separatorView.topAnchor.constraint(equalTo: topAnchor).isActive = true
        separatorView.leftAnchor.constraint(equalTo: leftAnchor).isActive = true
        separatorView.rightAnchor.constraint(equalTo: rightAnchor).isActive = true
        separatorView.heightAnchor.constraint(equalToConstant: 1).isActive = true
        
        addSubview(addButton)
        addButton.topAnchor.constraint(equalTo: topAnchor, constant: 20).isActive = true
        addButton.rightAnchor.constraint(equalTo: rightAnchor).isActive = true
        addButton.heightAnchor.constraint(equalToConstant: 22).isActive = true
        addButton.widthAnchor.constraint(equalTo: addButton.widthAnchor, multiplier: 1.0).isActive = true
        
        addSubview(tagTitleLabel)
        tagTitleLabel.topAnchor.constraint(equalTo: topAnchor, constant: 20).isActive = true
        tagTitleLabel.leftAnchor.constraint(equalTo: leftAnchor).isActive = true
        tagTitleLabel.rightAnchor.constraint(equalTo: addButton.leftAnchor).isActive = true
        
        collectionView.register(ProductsMenuCategoryCell.self, forCellWithReuseIdentifier: cellId)
        addSubview(collectionView)
        collectionView.topAnchor.constraint(equalTo: topView.bottomAnchor).isActive = true
        collectionView.leftAnchor.constraint(equalTo: leftAnchor).isActive = true
        collectionView.rightAnchor.constraint(equalTo: rightAnchor).isActive = true
        collectionView.bottomAnchor.constraint(equalTo: bottomAnchor).isActive = true
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

extension ProductsMenuCategoriesCell: UICollectionViewDelegateFlowLayout, UICollectionViewDelegate, UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return categories.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellId, for: indexPath) as! ProductsMenuCategoryCell
        cell.category = categories[indexPath.row]
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 10
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: frame.width, height: 30)
    }
}
