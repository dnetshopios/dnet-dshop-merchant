//
//  ColorBorderCell.swift
//  dShop Merchant
//
//  Created by Marknel Pogi on 12/6/18.
//  Copyright © 2018 Marknel Pogi. All rights reserved.
//

import UIKit

class ColorBorderCell: UICollectionViewCell {
    let circleView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.layer.masksToBounds = true
        return view
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupViews()
    }
    
    private func setupViews() {
        let circlePadding = frame.width * 0.1 // 10 % padding
        addSubview(circleView)
        circleView.topAnchor.constraint(equalTo: topAnchor, constant: circlePadding).isActive = true
        circleView.leftAnchor.constraint(equalTo: leftAnchor, constant: circlePadding).isActive = true
        circleView.rightAnchor.constraint(equalTo: rightAnchor, constant: -circlePadding).isActive = true
        circleView.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -circlePadding).isActive = true
        circleView.layer.cornerRadius = (frame.width - (circlePadding * 2)) / 2
        circleView.layer.borderColor = UIColor.white.withAlphaComponent(0).cgColor
        circleView.layer.borderWidth = 2
    }
    
    override var isSelected: Bool {
        didSet {
            if isSelected {
                circleView.layer.borderColor =  UIColor(red: 220/255, green: 226/255, blue: 229/255, alpha: 1).cgColor
            } else {
                circleView.layer.borderColor =  UIColor.white.withAlphaComponent(0).cgColor
            }
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
