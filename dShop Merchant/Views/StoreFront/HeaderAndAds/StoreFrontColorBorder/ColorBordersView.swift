//
//  ColorBordersView.swift
//  dShop Merchant
//
//  Created by Marknel Pogi on 12/6/18.
//  Copyright © 2018 Marknel Pogi. All rights reserved.
//

import UIKit

class ColorBordersView: UIView {
    var selectedIndexRow = 0 {
        didSet {
            collectionView.reloadData()
        }
    }
    
    lazy var collectionView: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        let cv = UICollectionView(frame: .zero, collectionViewLayout: layout)
        cv.isScrollEnabled = false
        cv.backgroundColor = .clear
        cv.delegate = self
        cv.dataSource = self
        cv.translatesAutoresizingMaskIntoConstraints = false
        return cv
    }()
    
    let cellId = "cellId"
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupViews()
    }
    
    private func setupViews() {
        collectionView.register(ColorBorderCell.self, forCellWithReuseIdentifier: cellId)
        addSubview(collectionView)
        collectionView.topAnchor.constraint(equalTo: topAnchor).isActive = true
        collectionView.leftAnchor.constraint(equalTo: leftAnchor).isActive = true
        collectionView.rightAnchor.constraint(equalTo: rightAnchor).isActive = true
        collectionView.bottomAnchor.constraint(equalTo: bottomAnchor).isActive = true
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

// MARK: CollectionView
extension ColorBordersView: UICollectionViewDelegateFlowLayout, UICollectionViewDelegate, UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        selectedIndexRow = indexPath.row
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 8
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: frame.width / 4, height: frame.width / 4)
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellId, for: indexPath) as! ColorBorderCell
        let colors: [UIColor] = [
            UIColor(red: 30/255, green: 30/255, blue: 30/255, alpha: 1),
            UIColor(red: 96/255, green: 200/255, blue: 201/255, alpha: 1),
            UIColor(red: 44/255, green: 170/255, blue: 30/255, alpha: 1),
            UIColor(red: 251/255, green: 74/255, blue: 74/255, alpha: 1),
            UIColor(red: 207/255, green: 130/255, blue: 25/255, alpha: 1),
            UIColor(red: 255/255, green: 168/255, blue: 241/255, alpha: 1),
            UIColor(red: 251/255, green: 226/255, blue: 63/255, alpha: 1),
            UIColor(red: 75/255, green: 75/255, blue: 255/255, alpha: 1),
        ]
        
        cell.circleView.backgroundColor = colors[indexPath.row]
        
        if selectedIndexRow == indexPath.row {
            cell.circleView.layer.borderColor = UIColor(red: 220/255, green: 226/255, blue: 229/255, alpha: 1).cgColor
        }
        
        return cell
    }
}

