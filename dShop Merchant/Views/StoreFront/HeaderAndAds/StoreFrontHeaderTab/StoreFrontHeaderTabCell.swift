//
//  StoreFrontHeaderTabCell.swift
//  dShop Merchant
//
//  Created by Marknel Pogi on 12/6/18.
//  Copyright © 2018 Marknel Pogi. All rights reserved.
//

import UIKit

class StoreFrontHeaderTabCell: UICollectionViewCell {
    let titleLabel: UILabel = {
        let lbl = UILabel()
        lbl.textAlignment = .center
        lbl.numberOfLines = 2
        lbl.font = UIFont(name: "SFUIDisplay-Medium", size: 14)
        lbl.textColor = UIColor(red: 130/255, green: 133/255, blue: 143/255, alpha: 1)
        lbl.translatesAutoresizingMaskIntoConstraints = false
        lbl.contentMode = .scaleToFill
        return lbl
    }()
    
    let separatorView: UIView = {
        let view = UIView()
        view.backgroundColor = .lightGray
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    override var isSelected: Bool {
        didSet {
            if isSelected {
                titleLabel.font = UIFont(name: "SFUIDisplay-SemiBold", size: 16)
                titleLabel.textColor = UIColor(red: 0/255, green: 165/255, blue: 227/255, alpha: 1)
            } else {
                titleLabel.font = UIFont(name: "SFUIDisplay-Medium", size: 14)
                titleLabel.textColor = UIColor(red: 130/255, green: 133/255, blue: 143/255, alpha: 1)
            }
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupViews()
        
    }
    
    private func setupViews() {
        addSubview(titleLabel)
        titleLabel.leftAnchor.constraint(equalTo: leftAnchor, constant: 6).isActive = true
        titleLabel.rightAnchor.constraint(equalTo: rightAnchor, constant: -6).isActive = true
        titleLabel.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -6).isActive = true
        
        addSubview(separatorView)
        separatorView.leftAnchor.constraint(equalTo: leftAnchor).isActive = true
        separatorView.rightAnchor.constraint(equalTo: rightAnchor).isActive = true
        separatorView.bottomAnchor.constraint(equalTo: bottomAnchor).isActive = true
        separatorView.heightAnchor.constraint(equalToConstant: 1).isActive = true
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
