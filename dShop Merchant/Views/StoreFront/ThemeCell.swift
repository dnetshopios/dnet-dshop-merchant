//
//  ThemeCell.swift
//  dShop Merchant
//
//  Created by Marknel Pogi on 12/6/18.
//  Copyright © 2018 Marknel Pogi. All rights reserved.
//

import UIKit

class ThemeCell: UICollectionViewCell {
    let scrollView: UIScrollView = {
        let sv = UIScrollView()
        sv.translatesAutoresizingMaskIntoConstraints = false
        return sv
    }()
    
    let safeView: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor(red: 31/255, green: 83/255, blue: 223/255, alpha: 1)
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    let fontsLabel: UILabel = {
        let lbl = UILabel()
        lbl.text = "Fonts"
        lbl.font = UIFont(name: "SFUIDisplay-SemiBold", size: 24)
        lbl.textColor = UIColor(red: 238/255, green: 238/255, blue: 238/255, alpha: 1)
        lbl.translatesAutoresizingMaskIntoConstraints = false
        return lbl
    }()
    
    let fonts = [
        "SF UI Display",
        "Arial",
    ]
    
    lazy var fontsTextField: UITextField = {
        let tf = UITextField()
        tf.loadDropdownData(data: fonts)
        tf.addDoneButtonOnKeyboard()
        tf.font = UIFont(name: "SFUIDisplay-SemiBold", size: 18)
        tf.placeholder = "Category Name"
        tf.layer.cornerRadius = 6
        tf.layer.masksToBounds = true
        tf.translatesAutoresizingMaskIntoConstraints = false
        tf.backgroundColor = .white
        tf.textColor =  UIColor(red: 15/255, green: 36/255, blue: 67/255, alpha: 1)
        tf.setLeftPaddingPoints(16)
        tf.setRightPaddingPoints(16)
        return tf
    }()
    
    let dropdownArrowImageView: UIImageView = {
        let iv = UIImageView()
        iv.image = UIImage(named: "small-down")?.withRenderingMode(.alwaysTemplate)
        iv.tintColor = UIColor(red: 96/255, green: 96/255, blue: 96/255, alpha: 1)
        iv.translatesAutoresizingMaskIntoConstraints = false
        return iv
    }()
    
    let fontsPreviewStackView: UIStackView = {
        let sv = UIStackView()
        sv.distribution = .equalCentering
        sv.translatesAutoresizingMaskIntoConstraints = false
        return sv
    }()
    
    let fontTitlePreviewLabel: UILabel = {
        let lbl = UILabel()
        lbl.text = "Title"
        lbl.font = UIFont(name: "SFUIDisplay-Bold", size: 30)
        lbl.textColor = UIColor(red: 127/255, green: 127/255, blue: 127/255, alpha: 1)
        lbl.translatesAutoresizingMaskIntoConstraints = false
        return lbl
    }()
    
    let fontTitleDescriptionLabel: UILabel = {
        let lbl = UILabel()
        lbl.text = "Bold 30px"
        lbl.font = UIFont(name: "Arial", size: 10)
        lbl.textColor = UIColor(red: 127/255, green: 127/255, blue: 127/255, alpha: 1)
        lbl.translatesAutoresizingMaskIntoConstraints = false
        return lbl
    }()
    
    let fontSubheaderPreviewLabel: UILabel = {
        let lbl = UILabel()
        lbl.text = "Subheader"
        lbl.font = UIFont(name: "SFUIDisplay-Regular", size: 20)
        lbl.textColor = UIColor(red: 127/255, green: 127/255, blue: 127/255, alpha: 1)
        lbl.translatesAutoresizingMaskIntoConstraints = false
        return lbl
    }()
    
    let fontSubheaderDescriptionLabel: UILabel = {
        let lbl = UILabel()
        lbl.text = "Regular 20px"
        lbl.font = UIFont(name: "Arial", size: 10)
        lbl.textColor = UIColor(red: 127/255, green: 127/255, blue: 127/255, alpha: 1)
        lbl.translatesAutoresizingMaskIntoConstraints = false
        return lbl
    }()
    
    let fontBodyPreviewLabel: UILabel = {
        let lbl = UILabel()
        lbl.text = "Body"
        lbl.font = UIFont(name: "SFUIDisplay-Regular", size: 14)
        lbl.textColor = UIColor(red: 127/255, green: 127/255, blue: 127/255, alpha: 1)
        lbl.translatesAutoresizingMaskIntoConstraints = false
        return lbl
    }()
    
    let fontBodyDescriptionLabel: UILabel = {
        let lbl = UILabel()
        lbl.text = "Regular 14px"
        lbl.font = UIFont(name: "Arial", size: 10)
        lbl.textColor = UIColor(red: 127/255, green: 127/255, blue: 127/255, alpha: 1)
        lbl.translatesAutoresizingMaskIntoConstraints = false
        return lbl
    }()
    
    let backgrounStyleLabel: UILabel = {
        let lbl = UILabel()
        lbl.text = "Background Style"
        lbl.font = UIFont(name: "SFUIDisplay-SemiBold", size: 24)
        lbl.textColor = UIColor(red: 238/255, green: 238/255, blue: 238/255, alpha: 1)
        lbl.translatesAutoresizingMaskIntoConstraints = false
        return lbl
    }()
    
    let backgroundStyleView: BackgroundStyleView = {
        let view = BackgroundStyleView()
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    let bannerAdsLabel: UILabel = {
        let lbl = UILabel()
        lbl.text = "Banner / Ads"
        lbl.font = UIFont(name: "SFUIDisplay-SemiBold", size: 24)
        lbl.textColor = UIColor(red: 238/255, green: 238/255, blue: 238/255, alpha: 1)
        lbl.translatesAutoresizingMaskIntoConstraints = false
        return lbl
    }()
    
    let bannerAdsView: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor(red: 28/255, green: 28/255, blue: 28/255, alpha: 1)
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    let bannerAdsPlaceholderImageView: UIImageView = {
        let iv = UIImageView()
        iv.image = UIImage(named: "placeholder_to_images")
        iv.contentMode = .scaleAspectFit
        iv.translatesAutoresizingMaskIntoConstraints = false
        return iv
    }()
    
    let imagesImagesImageView: CachedImageView = {
        let iv = CachedImageView()
        iv.contentMode = .scaleAspectFit
        iv.translatesAutoresizingMaskIntoConstraints = false
        return iv
    }()
    
    lazy var uploadImagesButton: UIButton = {
        let btn = UIButton(type: .system)
        btn.setTitle("Upload Images", for: .normal)
        btn.titleLabel?.font = UIFont(name: "SFUIDisplay-Bold", size: 12)
        btn.tintColor = UIColor(red: 207/255, green: 207/255, blue: 207/255, alpha: 1)
        btn.backgroundColor = UIColor(red: 126/255, green: 126/255, blue: 126/255, alpha: 1)
        btn.layer.cornerRadius = 4
        btn.layer.masksToBounds = true
        btn.addTarget(self, action: #selector(handleUploadImages), for: .touchUpInside)
        btn.translatesAutoresizingMaskIntoConstraints = false
        return btn
    }()
    
    let bannerAdsPositionLabel: UILabel = {
        let lbl = UILabel()
        lbl.text = "Banner / Ads Position"
        lbl.font = UIFont(name: "SFUIDisplay-SemiBold", size: 24)
        lbl.textColor = UIColor(red: 238/255, green: 238/255, blue: 238/255, alpha: 1)
        lbl.translatesAutoresizingMaskIntoConstraints = false
        return lbl
    }()
    
    let bannerAdsPositionView: BannerAdsPositionView = {
        let view = BannerAdsPositionView()
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    let categoriesPositionLabel: UILabel = {
        let lbl = UILabel()
        lbl.text = "Categories Position"
        lbl.font = UIFont(name: "SFUIDisplay-SemiBold", size: 24)
        lbl.textColor = UIColor(red: 238/255, green: 238/255, blue: 238/255, alpha: 1)
        lbl.translatesAutoresizingMaskIntoConstraints = false
        return lbl
    }()
    
    let categoriesPositionView: CategoriesPositionView = {
        let view = CategoriesPositionView()
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    lazy var saveButton: UIButton = {
        let btn = UIButton(type: .system)
        btn.setTitle("SAVE", for: .normal)
        btn.titleLabel?.font = UIFont(name: "SFUIDisplay-Bold", size: 18)
        btn.tintColor = .white
        btn.backgroundColor = UIColor(red: 31/255, green: 83/255, blue: 223/255, alpha: 1)
        btn.addTarget(self, action: #selector(handleSave), for: .touchUpInside)
        btn.translatesAutoresizingMaskIntoConstraints = false
        return btn
    }()
    
    @objc func handleUploadImages() {
        print("Upload Images")
    }
    
    @objc func handleSave() {
        print("Save")
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupViews()
    }
    
    private func setupViews() {
        backgroundColor = .mainBG
        
        addSubview(safeView)
        if #available(iOS 11.0, *) {
            safeView.topAnchor.constraint(equalTo: safeAreaLayoutGuide.bottomAnchor).isActive = true
        } else {
            // Fallback on earlier versions
            safeView.topAnchor.constraint(equalTo: bottomAnchor).isActive = true
        }
        safeView.leftAnchor.constraint(equalTo: leftAnchor).isActive = true
        safeView.rightAnchor.constraint(equalTo: rightAnchor).isActive = true
        safeView.bottomAnchor.constraint(equalTo: bottomAnchor).isActive = true
        
        addSubview(saveButton)
        saveButton.heightAnchor.constraint(equalToConstant: 60).isActive = true
        saveButton.leftAnchor.constraint(equalTo: leftAnchor).isActive = true
        saveButton.rightAnchor.constraint(equalTo: rightAnchor).isActive = true
        saveButton.bottomAnchor.constraint(equalTo: safeView.topAnchor).isActive = true
        
        addSubview(scrollView)
        scrollView.topAnchor.constraint(equalTo: topAnchor).isActive = true
        scrollView.leftAnchor.constraint(equalTo: leftAnchor).isActive = true
        scrollView.rightAnchor.constraint(equalTo: rightAnchor).isActive = true
        scrollView.bottomAnchor.constraint(equalTo: saveButton.topAnchor).isActive = true
        
        /*
            backgroundStyleViewWidth = frame.width - 32
            backgroundStyleCellWidth = (frame.width - (32 + 40)) / 2
         */
        
        let backgroundStyleViewHeight = (frame.width - (32 + 40)) / 2
        let bannerAdsPositionViewHeight = ((frame.width - (32 + 40)) / 3) + 30
        let categoriesPositionViewHeight = ((frame.width - (32 + 40)) / 3) + 30
        let bannerAdsViewHeight = frame.width - 52
        
        // MARK: Default Height + x2 cellRowHeight
        scrollView.contentSize = CGSize(width: frame.width, height: 550 + bannerAdsViewHeight + backgroundStyleViewHeight + bannerAdsPositionViewHeight + categoriesPositionViewHeight)
        
        scrollView.addSubview(fontsLabel)
        fontsLabel.topAnchor.constraint(equalTo: scrollView.topAnchor, constant: 30).isActive = true
        fontsLabel.leftAnchor.constraint(equalTo: leftAnchor, constant: 16).isActive = true
        
        scrollView.addSubview(fontsTextField)
        fontsTextField.topAnchor.constraint(equalTo: fontsLabel.bottomAnchor, constant: 8).isActive = true
        fontsTextField.leftAnchor.constraint(equalTo: leftAnchor, constant: 16).isActive = true
        fontsTextField.rightAnchor.constraint(equalTo: rightAnchor, constant: -16).isActive = true
        fontsTextField.heightAnchor.constraint(equalToConstant: 38).isActive = true
        
        scrollView.addSubview(dropdownArrowImageView)
        dropdownArrowImageView.centerYAnchor.constraint(equalTo: fontsTextField.centerYAnchor).isActive = true
        dropdownArrowImageView.rightAnchor.constraint(equalTo: fontsTextField.rightAnchor, constant: -14).isActive = true
        dropdownArrowImageView.heightAnchor.constraint(equalToConstant: 9).isActive = true
        dropdownArrowImageView.widthAnchor.constraint(equalToConstant: 14).isActive = true
        
        fontsPreviewStackView.addArrangedSubview(fontTitlePreviewLabel)
        fontTitlePreviewLabel.addSubview(fontTitleDescriptionLabel)
        fontTitleDescriptionLabel.topAnchor.constraint(equalTo: fontTitlePreviewLabel.bottomAnchor).isActive = true
        fontTitleDescriptionLabel.leftAnchor.constraint(equalTo: fontTitlePreviewLabel.leftAnchor).isActive = true
        
        fontsPreviewStackView.addArrangedSubview(fontSubheaderPreviewLabel)
        fontSubheaderPreviewLabel.addSubview(fontSubheaderDescriptionLabel)
        fontSubheaderDescriptionLabel.topAnchor.constraint(equalTo: fontSubheaderPreviewLabel.bottomAnchor).isActive = true
        fontSubheaderDescriptionLabel.leftAnchor.constraint(equalTo: fontSubheaderPreviewLabel.leftAnchor).isActive = true
        
        fontsPreviewStackView.addArrangedSubview(fontBodyPreviewLabel)
        fontBodyPreviewLabel.addSubview(fontBodyDescriptionLabel)
        fontBodyDescriptionLabel.topAnchor.constraint(equalTo: fontBodyPreviewLabel.bottomAnchor).isActive = true
        fontBodyDescriptionLabel.leftAnchor.constraint(equalTo: fontBodyPreviewLabel.leftAnchor).isActive = true
        
        scrollView.addSubview(fontsPreviewStackView)
        fontsPreviewStackView.topAnchor.constraint(equalTo: fontsTextField.bottomAnchor, constant: 18).isActive = true
        fontsPreviewStackView.centerXAnchor.constraint(equalTo: centerXAnchor).isActive = true
        fontsPreviewStackView.widthAnchor.constraint(equalToConstant: 240).isActive = true
        
        scrollView.addSubview(backgrounStyleLabel)
        backgrounStyleLabel.topAnchor.constraint(equalTo: fontsPreviewStackView.bottomAnchor, constant: 60).isActive = true
        backgrounStyleLabel.leftAnchor.constraint(equalTo: fontsTextField.leftAnchor).isActive = true
        
        scrollView.addSubview(backgroundStyleView)
        backgroundStyleView.topAnchor.constraint(equalTo: backgrounStyleLabel.bottomAnchor, constant: 10).isActive = true
        backgroundStyleView.leftAnchor.constraint(equalTo: leftAnchor, constant: 16).isActive = true
        backgroundStyleView.rightAnchor.constraint(equalTo: rightAnchor, constant: -16).isActive = true
        backgroundStyleView.heightAnchor.constraint(equalToConstant: backgroundStyleViewHeight).isActive = true
        
        scrollView.addSubview(bannerAdsLabel)
        bannerAdsLabel.topAnchor.constraint(equalTo: backgroundStyleView.bottomAnchor, constant: 30).isActive = true
        bannerAdsLabel.leftAnchor.constraint(equalTo: leftAnchor, constant: 26).isActive = true
        
        scrollView.addSubview(bannerAdsView)
        bannerAdsView.topAnchor.constraint(equalTo: bannerAdsLabel.bottomAnchor, constant: 8).isActive = true
        bannerAdsView.leftAnchor.constraint(equalTo: bannerAdsLabel.leftAnchor).isActive = true
        bannerAdsView.rightAnchor.constraint(equalTo: rightAnchor, constant: -26).isActive = true
        bannerAdsView.heightAnchor.constraint(equalTo: bannerAdsView.widthAnchor, multiplier: 1.0).isActive = true
        
        scrollView.addSubview(bannerAdsPlaceholderImageView)
        bannerAdsPlaceholderImageView.centerXAnchor.constraint(equalTo: bannerAdsView.centerXAnchor).isActive = true
        bannerAdsPlaceholderImageView.centerYAnchor.constraint(equalTo: bannerAdsView.centerYAnchor).isActive = true
        bannerAdsPlaceholderImageView.heightAnchor.constraint(equalToConstant: 72).isActive = true
        bannerAdsPlaceholderImageView.widthAnchor.constraint(equalToConstant: 81).isActive = true
        
        scrollView.addSubview(imagesImagesImageView)
        imagesImagesImageView.topAnchor.constraint(equalTo: bannerAdsView.topAnchor).isActive = true
        imagesImagesImageView.leftAnchor.constraint(equalTo: bannerAdsView.leftAnchor).isActive = true
        imagesImagesImageView.rightAnchor.constraint(equalTo: bannerAdsView.rightAnchor).isActive = true
        imagesImagesImageView.bottomAnchor.constraint(equalTo: bannerAdsView.bottomAnchor).isActive = true
        
        scrollView.addSubview(uploadImagesButton)
        uploadImagesButton.centerXAnchor.constraint(equalTo: bannerAdsView.centerXAnchor).isActive = true
        uploadImagesButton.bottomAnchor.constraint(equalTo: bannerAdsView.bottomAnchor, constant: -44).isActive = true
        uploadImagesButton.heightAnchor.constraint(equalToConstant: 33).isActive = true
        uploadImagesButton.widthAnchor.constraint(equalToConstant: 177).isActive = true
        
        scrollView.addSubview(bannerAdsPositionLabel)
        bannerAdsPositionLabel.topAnchor.constraint(equalTo: bannerAdsView.bottomAnchor, constant: 30).isActive = true
        bannerAdsPositionLabel.leftAnchor.constraint(equalTo: backgroundStyleView.leftAnchor).isActive = true
        
        scrollView.addSubview(bannerAdsPositionView)
        bannerAdsPositionView.topAnchor.constraint(equalTo: bannerAdsPositionLabel.bottomAnchor, constant: 10).isActive = true
        bannerAdsPositionView.leftAnchor.constraint(equalTo: leftAnchor, constant: 16).isActive = true
        bannerAdsPositionView.rightAnchor.constraint(equalTo: rightAnchor, constant: -16).isActive = true
        bannerAdsPositionView.heightAnchor.constraint(equalToConstant: bannerAdsPositionViewHeight).isActive = true
        
        scrollView.addSubview(categoriesPositionLabel)
        categoriesPositionLabel.topAnchor.constraint(equalTo: bannerAdsPositionView.bottomAnchor, constant: 30).isActive = true
        categoriesPositionLabel.leftAnchor.constraint(equalTo: bannerAdsPositionView.leftAnchor).isActive = true
        
        scrollView.addSubview(categoriesPositionView)
        categoriesPositionView.topAnchor.constraint(equalTo: categoriesPositionLabel.bottomAnchor, constant: 30).isActive = true
        categoriesPositionView.leftAnchor.constraint(equalTo: bannerAdsPositionView.leftAnchor).isActive = true
        categoriesPositionView.rightAnchor.constraint(equalTo: bannerAdsPositionView.rightAnchor).isActive = true
        categoriesPositionView.heightAnchor.constraint(equalToConstant: categoriesPositionViewHeight).isActive = true
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
