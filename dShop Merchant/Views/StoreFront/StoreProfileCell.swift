//
//  StoreProfileCell.swift
//  dShop Merchant
//
//  Created by Marknel Pogi on 12/7/18.
//  Copyright © 2018 Marknel Pogi. All rights reserved.
//

import UIKit

class StoreProfileCell: UICollectionViewCell {
    var storeProfileDelegate: StoreProfileDelegate?
    
    var selectedPhoto: UIImage? {
        didSet {
            if let image = selectedPhoto {
                DispatchQueue.main.async {
                    self.contentTextView.addImage(image: image)
                    self.contentTextView.font = UIFont(name: "SFUIDisplay-Light", size: 20)
                }
            }
        }
    }
    
    let safeView: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor(red: 31/255, green: 83/255, blue: 223/255, alpha: 1)
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    lazy var saveButton: UIButton = {
        let btn = UIButton(type: .system)
        btn.setTitle("SAVE", for: .normal)
        btn.titleLabel?.font = UIFont(name: "SFUIDisplay-Bold", size: 18)
        btn.tintColor = .white
        btn.backgroundColor = UIColor(red: 31/255, green: 83/255, blue: 223/255, alpha: 1)
        btn.addTarget(self, action: #selector(handleSave), for: .touchUpInside)
        btn.translatesAutoresizingMaskIntoConstraints = false
        return btn
    }()
    
    @objc func handleSave() {
        print("SAVE")
    }
    
    let contentTextView: UITextView = {
        let tv = UITextView()
        tv.addDoneButtonOnKeyboard()
        tv.font = UIFont(name: "SFUIDisplay-Light", size: 20)
        tv.layer.cornerRadius = 10
        tv.layer.masksToBounds = true
        tv.backgroundColor = UIColor(red: 243/255, green: 243/255, blue: 243/255, alpha: 1)
        tv.textColor =  UIColor(red: 77/255, green: 77/255, blue: 77/255, alpha: 1)
        tv.translatesAutoresizingMaskIntoConstraints = false
        tv.isScrollEnabled = true
        tv.textContainerInset = UIEdgeInsets(top: 10, left: 10, bottom: 70, right: 10)
        return tv
    }()
    
    let contentDividerView: UIView = {
        let view = UIView()
        view.layer.cornerRadius = 10
        view.layer.masksToBounds = true
        view.backgroundColor = UIColor(red: 243/255, green: 243/255, blue: 243/255, alpha: 1)
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    let optionsView: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor(red: 243/255, green: 243/255, blue: 243/255, alpha: 1)
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    lazy var addImageImageView: UIImageView = {
        let iv = UIImageView()
        iv.image = UIImage(named: "image-placeholder")?.withRenderingMode(.alwaysTemplate)
        iv.tintColor = UIColor(red: 162/255, green: 162/255, blue: 162/255, alpha: 1)
        iv.translatesAutoresizingMaskIntoConstraints = false
        iv.isUserInteractionEnabled = true
        iv.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(handleAddImage)))
        return iv
    }()
    
    lazy var addImageButton: UIButton = {
        let btn = UIButton(type: .system)
        btn.setTitle("Add Image", for: .normal)
        btn.titleLabel?.font = UIFont(name: "SFUIDisplay-Medium", size: 13)
        btn.tintColor = UIColor(red: 94/255, green: 94/255, blue: 94/255, alpha: 1)
        btn.translatesAutoresizingMaskIntoConstraints = false
        btn.addTarget(self, action: #selector(handleAddImage), for: .touchUpInside)
        return btn
    }()
    
    @objc func handleAddImage() {
        storeProfileDelegate?.doAddImageStoreProfile()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        //keyboard observers
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: UIResponder.keyboardDidShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
        
        setupViews()
    }

    @objc func keyboardWillShow(notification: NSNotification) {
        if let keyboardHeight = (notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue.height {
            contentTextView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: keyboardHeight + 20, right: 0)
        }
    }
    
    @objc func keyboardWillHide(notification: NSNotification) {
        UIView.animate(withDuration: 0.2, animations: {
            self.contentTextView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        })
    }
    
    deinit {
        print("denit")
        NotificationCenter.default.removeObserver(self)
    }

    private func setupViews() {
        backgroundColor = .mainBG
        
        addSubview(safeView)
        if #available(iOS 11.0, *) {
            safeView.topAnchor.constraint(equalTo: safeAreaLayoutGuide.bottomAnchor).isActive = true
        } else {
            // Fallback on earlier versions
            safeView.topAnchor.constraint(equalTo: bottomAnchor).isActive = true
        }
        safeView.leftAnchor.constraint(equalTo: leftAnchor).isActive = true
        safeView.rightAnchor.constraint(equalTo: rightAnchor).isActive = true
        safeView.bottomAnchor.constraint(equalTo: bottomAnchor).isActive = true
        
        addSubview(saveButton)
        saveButton.heightAnchor.constraint(equalToConstant: 60).isActive = true
        saveButton.leftAnchor.constraint(equalTo: leftAnchor).isActive = true
        saveButton.rightAnchor.constraint(equalTo: rightAnchor).isActive = true
        saveButton.bottomAnchor.constraint(equalTo: safeView.topAnchor).isActive = true
        
        addSubview(contentTextView)
        contentTextView.topAnchor.constraint(equalTo: topAnchor, constant: 20).isActive = true
        contentTextView.leftAnchor.constraint(equalTo: leftAnchor, constant: 20).isActive = true
        contentTextView.rightAnchor.constraint(equalTo: rightAnchor, constant: -20).isActive = true
        contentTextView.bottomAnchor.constraint(equalTo: saveButton.topAnchor, constant: -20).isActive = true
        
        addSubview(contentDividerView)
        contentDividerView.bottomAnchor.constraint(equalTo: contentTextView.bottomAnchor).isActive = true
        contentDividerView.leftAnchor.constraint(equalTo: contentTextView.leftAnchor).isActive = true
        contentDividerView.rightAnchor.constraint(equalTo: contentTextView.rightAnchor).isActive = true
        contentDividerView.heightAnchor.constraint(equalToConstant: 30).isActive = true
        
        addSubview(optionsView)
        optionsView.leftAnchor.constraint(equalTo: contentTextView.leftAnchor).isActive = true
        optionsView.rightAnchor.constraint(equalTo: contentTextView.rightAnchor).isActive = true
        optionsView.bottomAnchor.constraint(equalTo: contentTextView.bottomAnchor, constant: -10).isActive = true
        optionsView.heightAnchor.constraint(equalToConstant: 60).isActive = true
        
        addSubview(addImageImageView)
        addImageImageView.leftAnchor.constraint(equalTo: optionsView.leftAnchor, constant: 20).isActive = true
        addImageImageView.centerYAnchor.constraint(equalTo: optionsView.centerYAnchor).isActive = true
        addImageImageView.widthAnchor.constraint(equalToConstant: 32).isActive = true
        addImageImageView.heightAnchor.constraint(equalToConstant: 26).isActive = true
        
        addSubview(addImageButton)
        addImageButton.centerYAnchor.constraint(equalTo: optionsView.centerYAnchor).isActive = true
        addImageButton.leftAnchor.constraint(equalTo: addImageImageView.rightAnchor, constant: 10).isActive = true
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
