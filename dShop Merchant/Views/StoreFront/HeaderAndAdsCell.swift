//
//  HeaderAndAdsCell.swift
//  dShop Merchant
//
//  Created by Marknel Pogi on 12/6/18.
//  Copyright © 2018 Marknel Pogi. All rights reserved.
//

import UIKit

class HeaderAndAdsCell: UICollectionViewCell {
    let scrollView: UIScrollView = {
        let sv = UIScrollView()
        sv.translatesAutoresizingMaskIntoConstraints = false
        return sv
    }()
    
    let safeView: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor(red: 31/255, green: 83/255, blue: 223/255, alpha: 1)
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    let storeLogoLabel: UILabel = {
        let lbl = UILabel()
        lbl.text = "Store Logo"
        lbl.font = UIFont(name: "SFUIDisplay-SemiBold", size: 24)
        lbl.textColor = UIColor(red: 238/255, green: 238/255, blue: 238/255, alpha: 1)
        lbl.translatesAutoresizingMaskIntoConstraints = false
        return lbl
    }()
    
    let storeLogoView: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor(red: 28/255, green: 28/255, blue: 28/255, alpha: 1)
        view.layer.cornerRadius = 120
        view.layer.masksToBounds = true
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    let storeLogoPlaceHolderImageView: UIImageView = {
        let iv = UIImageView()
        iv.image = UIImage(named: "image-placeholder")
        iv.translatesAutoresizingMaskIntoConstraints = false
        return iv
    }()
    
    let uploadLogoLabel: UILabel = {
        let lbl = UILabel()
        lbl.text = "Upload Logo"
        lbl.font = UIFont(name: "SFUIDisplay-SemiBold", size: 14)
        lbl.textColor = UIColor(red: 175/255, green: 175/255, blue: 175/255, alpha: 1)
        lbl.translatesAutoresizingMaskIntoConstraints = false
        return lbl
    }()
    
    let storeLogoImageView: CachedImageView = {
        let iv = CachedImageView()
        iv.translatesAutoresizingMaskIntoConstraints = false
        return iv
    }()
    
    let logoBorderLabel: UILabel = {
        let lbl = UILabel()
        lbl.text = "Logo Border Color"
        lbl.font = UIFont(name: "SFUIDisplay-SemiBold", size: 24)
        lbl.textColor = UIColor(red: 238/255, green: 238/255, blue: 238/255, alpha: 1)
        lbl.translatesAutoresizingMaskIntoConstraints = false
        return lbl
    }()
    
    let selectColorLabel: UILabel = {
        let lbl = UILabel()
        lbl.text = "Select Color"
        lbl.font = UIFont(name: "SFUIDisplay-SemiBold", size: 14)
        lbl.textColor = UIColor(red: 175/255, green: 175/255, blue: 175/255, alpha: 1)
        lbl.translatesAutoresizingMaskIntoConstraints = false
        return lbl
    }()
    
    let socialMediaLabel: UILabel = {
        let lbl = UILabel()
        lbl.text = "Social Media"
        lbl.font = UIFont(name: "SFUIDisplay-SemiBold", size: 24)
        lbl.textColor = UIColor(red: 238/255, green: 238/255, blue: 238/255, alpha: 1)
        lbl.translatesAutoresizingMaskIntoConstraints = false
        return lbl
    }()
    
    let facebookLogoImageView: UIImageView = {
        let iv = UIImageView()
        iv.image = UIImage(named: "facebook_media_logo")
        iv.contentMode = .scaleAspectFill
        iv.translatesAutoresizingMaskIntoConstraints = false
        return iv
    }()
    
    let facebookUrlTextField: UITextField = {
        let tf = UITextField()
        tf.addDoneButtonOnKeyboard()
        tf.font = UIFont(name: "SFUIDisplay-Medium", size: 14)
        tf.placeholder = "https://"
        tf.layer.cornerRadius = 6
        tf.layer.masksToBounds = true
        tf.backgroundColor = .white
        tf.textColor =  UIColor(red: 15/255, green: 36/255, blue: 67/255, alpha: 1)
        tf.setLeftPaddingPoints(16)
        tf.setRightPaddingPoints(16)
        tf.translatesAutoresizingMaskIntoConstraints = false
        return tf
    }()
    
    let twitterLogoImageView: UIImageView = {
        let iv = UIImageView()
        iv.image = UIImage(named: "twitter_media_logo")
        iv.contentMode = .scaleAspectFill
        iv.translatesAutoresizingMaskIntoConstraints = false
        return iv
    }()
    
    let twitterUrlTextField: UITextField = {
        let tf = UITextField()
        tf.addDoneButtonOnKeyboard()
        tf.font = UIFont(name: "SFUIDisplay-Medium", size: 14)
        tf.placeholder = "https://"
        tf.layer.cornerRadius = 6
        tf.layer.masksToBounds = true
        tf.backgroundColor = .white
        tf.textColor =  UIColor(red: 15/255, green: 36/255, blue: 67/255, alpha: 1)
        tf.setLeftPaddingPoints(16)
        tf.setRightPaddingPoints(16)
        tf.translatesAutoresizingMaskIntoConstraints = false
        return tf
    }()
    
    let instagramLogoImageView: UIImageView = {
        let iv = UIImageView()
        iv.image = UIImage(named: "instagram_media_logo")
        iv.contentMode = .scaleAspectFill
        iv.translatesAutoresizingMaskIntoConstraints = false
        return iv
    }()
    
    let instagramUrlTextField: UITextField = {
        let tf = UITextField()
        tf.addDoneButtonOnKeyboard()
        tf.font = UIFont(name: "SFUIDisplay-Medium", size: 14)
        tf.placeholder = "https://"
        tf.layer.cornerRadius = 6
        tf.layer.masksToBounds = true
        tf.backgroundColor = .white
        tf.textColor =  UIColor(red: 15/255, green: 36/255, blue: 67/255, alpha: 1)
        tf.setLeftPaddingPoints(16)
        tf.setRightPaddingPoints(16)
        tf.translatesAutoresizingMaskIntoConstraints = false
        return tf
    }()
    
    let bannerAdsLabel: UILabel = {
        let lbl = UILabel()
        lbl.text = "Banner / Ads"
        lbl.font = UIFont(name: "SFUIDisplay-SemiBold", size: 24)
        lbl.textColor = UIColor(red: 238/255, green: 238/255, blue: 238/255, alpha: 1)
        lbl.translatesAutoresizingMaskIntoConstraints = false
        return lbl
    }()
    
    let bannerAdsView: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor(red: 28/255, green: 28/255, blue: 28/255, alpha: 1)
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    let bannerAdsPlaceholderImageView: UIImageView = {
        let iv = UIImageView()
        iv.image = UIImage(named: "placeholder_to_images")
        iv.contentMode = .scaleAspectFit
        iv.translatesAutoresizingMaskIntoConstraints = false
        return iv
    }()
    
    let imagesImagesImageView: CachedImageView = {
        let iv = CachedImageView()
        iv.contentMode = .scaleAspectFit
        iv.translatesAutoresizingMaskIntoConstraints = false
        return iv
    }()
    
    lazy var uploadImagesButton: UIButton = {
        let btn = UIButton(type: .system)
        btn.setTitle("Upload Images", for: .normal)
        btn.titleLabel?.font = UIFont(name: "SFUIDisplay-Bold", size: 12)
        btn.tintColor = UIColor(red: 207/255, green: 207/255, blue: 207/255, alpha: 1)
        btn.backgroundColor = UIColor(red: 126/255, green: 126/255, blue: 126/255, alpha: 1)
        btn.layer.cornerRadius = 4
        btn.layer.masksToBounds = true
        btn.addTarget(self, action: #selector(handleUploadImages), for: .touchUpInside)
        btn.translatesAutoresizingMaskIntoConstraints = false
        return btn
    }()
    
    lazy var saveButton: UIButton = {
        let btn = UIButton(type: .system)
        btn.setTitle("SAVE", for: .normal)
        btn.titleLabel?.font = UIFont(name: "SFUIDisplay-Bold", size: 18)
        btn.tintColor = .white
        btn.backgroundColor = UIColor(red: 31/255, green: 83/255, blue: 223/255, alpha: 1)
        btn.addTarget(self, action: #selector(handleSave), for: .touchUpInside)
        btn.translatesAutoresizingMaskIntoConstraints = false
        return btn
    }()
    
    lazy var colorBordersView: ColorBordersView = {
        let view = ColorBordersView()
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    @objc func handleUploadImages() {
        print("Upload Banner Ads Images")
    }
    
    @objc func handleSave() {
        print("Save")
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupViews()
    }
    
    private func setupViews() {
        backgroundColor = .mainBG
        
        addSubview(safeView)
        if #available(iOS 11.0, *) {
            safeView.topAnchor.constraint(equalTo: safeAreaLayoutGuide.bottomAnchor).isActive = true
        } else {
            // Fallback on earlier versions
            safeView.topAnchor.constraint(equalTo: bottomAnchor).isActive = true
        }
        safeView.leftAnchor.constraint(equalTo: leftAnchor).isActive = true
        safeView.rightAnchor.constraint(equalTo: rightAnchor).isActive = true
        safeView.bottomAnchor.constraint(equalTo: bottomAnchor).isActive = true
        
        addSubview(saveButton)
        saveButton.heightAnchor.constraint(equalToConstant: 60).isActive = true
        saveButton.leftAnchor.constraint(equalTo: leftAnchor).isActive = true
        saveButton.rightAnchor.constraint(equalTo: rightAnchor).isActive = true
        saveButton.bottomAnchor.constraint(equalTo: safeView.topAnchor).isActive = true
        
        addSubview(scrollView)
        scrollView.topAnchor.constraint(equalTo: topAnchor).isActive = true
        scrollView.leftAnchor.constraint(equalTo: leftAnchor).isActive = true
        scrollView.rightAnchor.constraint(equalTo: rightAnchor).isActive = true
        scrollView.bottomAnchor.constraint(equalTo: saveButton.topAnchor).isActive = true
        
        /*
         Store Border Color per cell size
         cellHeight = (frame.width - 32) / 4
         cell2RowHeight = cellHeight * 2
        */
        
        let colorBorderViewHeight = (((frame.width - 32) / 4) * 2)
        let bannerAdsViewHeight = frame.width - 52
        
        // MARK: Default Height + x2 cellRowHeight
        scrollView.contentSize = CGSize(width: frame.width, height: 800 + colorBorderViewHeight + bannerAdsViewHeight)
        
        scrollView.addSubview(storeLogoLabel)
        storeLogoLabel.topAnchor.constraint(equalTo: scrollView.topAnchor, constant: 30).isActive = true
        storeLogoLabel.leftAnchor.constraint(equalTo: leftAnchor, constant: 16).isActive = true
        
        scrollView.addSubview(storeLogoView)
        storeLogoView.topAnchor.constraint(equalTo: storeLogoLabel.bottomAnchor, constant: 8).isActive = true
        storeLogoView.centerXAnchor.constraint(equalTo: centerXAnchor).isActive = true
        storeLogoView.heightAnchor.constraint(equalToConstant: 240).isActive = true
        storeLogoView.widthAnchor.constraint(equalToConstant: 240).isActive = true
        
        storeLogoView.addSubview(storeLogoPlaceHolderImageView)
        storeLogoPlaceHolderImageView.centerXAnchor.constraint(equalTo: storeLogoView.centerXAnchor).isActive = true
        storeLogoPlaceHolderImageView.centerYAnchor.constraint(equalTo: storeLogoView.centerYAnchor).isActive = true
        storeLogoPlaceHolderImageView.widthAnchor.constraint(equalToConstant: 50).isActive = true
        storeLogoPlaceHolderImageView.heightAnchor.constraint(equalToConstant: 40).isActive = true
        
        storeLogoView.addSubview(uploadLogoLabel)
        uploadLogoLabel.topAnchor.constraint(equalTo: storeLogoPlaceHolderImageView.bottomAnchor, constant: 6).isActive = true
        uploadLogoLabel.centerXAnchor.constraint(equalTo: storeLogoView.centerXAnchor).isActive = true
        
        storeLogoView.addSubview(storeLogoImageView)
        storeLogoImageView.topAnchor.constraint(equalTo: storeLogoImageView.topAnchor).isActive = true
        storeLogoImageView.leftAnchor.constraint(equalTo: storeLogoImageView.leftAnchor).isActive = true
        storeLogoImageView.rightAnchor.constraint(equalTo: storeLogoImageView.rightAnchor).isActive = true
        storeLogoImageView.bottomAnchor.constraint(equalTo: storeLogoImageView.bottomAnchor).isActive = true
        
        scrollView.addSubview(logoBorderLabel)
        logoBorderLabel.topAnchor.constraint(equalTo: storeLogoView.bottomAnchor, constant: 30).isActive = true
        logoBorderLabel.leftAnchor.constraint(equalTo: storeLogoLabel.leftAnchor).isActive = true
        
        scrollView.addSubview(selectColorLabel)
        selectColorLabel.topAnchor.constraint(equalTo: logoBorderLabel.bottomAnchor, constant: 8).isActive = true
        selectColorLabel.leftAnchor.constraint(equalTo: logoBorderLabel.leftAnchor).isActive = true
    
        scrollView.addSubview(colorBordersView)
        colorBordersView.topAnchor.constraint(equalTo: selectColorLabel.bottomAnchor, constant: 8).isActive = true
        colorBordersView.leftAnchor.constraint(equalTo: leftAnchor, constant: 16).isActive = true
        colorBordersView.rightAnchor.constraint(equalTo: rightAnchor, constant: -16).isActive = true
        colorBordersView.heightAnchor.constraint(equalToConstant: colorBorderViewHeight).isActive = true
        
        scrollView.addSubview(socialMediaLabel)
        socialMediaLabel.topAnchor.constraint(equalTo: colorBordersView.bottomAnchor, constant: 30).isActive = true
        socialMediaLabel.leftAnchor.constraint(equalTo: logoBorderLabel.leftAnchor).isActive = true
        
        scrollView.addSubview(facebookUrlTextField)
        facebookUrlTextField.topAnchor.constraint(equalTo: socialMediaLabel.bottomAnchor, constant: 30).isActive = true
        facebookUrlTextField.leftAnchor.constraint(equalTo: socialMediaLabel.leftAnchor, constant: 51).isActive = true
        facebookUrlTextField.rightAnchor.constraint(equalTo: rightAnchor, constant: -16).isActive = true
        facebookUrlTextField.heightAnchor.constraint(equalToConstant: 34).isActive = true
        
        scrollView.addSubview(facebookLogoImageView)
        facebookLogoImageView.topAnchor.constraint(equalTo: socialMediaLabel.bottomAnchor, constant: 18).isActive = true
        facebookLogoImageView.leftAnchor.constraint(equalTo: socialMediaLabel.leftAnchor).isActive = true
        facebookLogoImageView.heightAnchor.constraint(equalToConstant: 58).isActive = true
        facebookLogoImageView.widthAnchor.constraint(equalToConstant: 58).isActive = true
        
        scrollView.addSubview(twitterUrlTextField)
        twitterUrlTextField.topAnchor.constraint(equalTo: facebookLogoImageView.bottomAnchor, constant: 28).isActive = true
        twitterUrlTextField.leftAnchor.constraint(equalTo: facebookUrlTextField.leftAnchor).isActive = true
        twitterUrlTextField.rightAnchor.constraint(equalTo: facebookUrlTextField.rightAnchor).isActive = true
        twitterUrlTextField.heightAnchor.constraint(equalToConstant: 34).isActive = true
        
        scrollView.addSubview(twitterLogoImageView)
        twitterLogoImageView.topAnchor.constraint(equalTo: facebookLogoImageView.bottomAnchor, constant: 16).isActive = true
        twitterLogoImageView.leftAnchor.constraint(equalTo: facebookLogoImageView.leftAnchor).isActive = true
        twitterLogoImageView.heightAnchor.constraint(equalToConstant: 58).isActive = true
        twitterLogoImageView.widthAnchor.constraint(equalToConstant: 58).isActive = true
        
        scrollView.addSubview(instagramUrlTextField)
        instagramUrlTextField.topAnchor.constraint(equalTo: twitterLogoImageView.bottomAnchor, constant: 28).isActive = true
        instagramUrlTextField.leftAnchor.constraint(equalTo: twitterUrlTextField.leftAnchor).isActive = true
        instagramUrlTextField.rightAnchor.constraint(equalTo: twitterUrlTextField.rightAnchor).isActive = true
        instagramUrlTextField.heightAnchor.constraint(equalToConstant: 34).isActive = true
        
        scrollView.addSubview(instagramLogoImageView)
        instagramLogoImageView.topAnchor.constraint(equalTo: twitterLogoImageView.bottomAnchor, constant: 16).isActive = true
        instagramLogoImageView.leftAnchor.constraint(equalTo: twitterLogoImageView.leftAnchor).isActive = true
        instagramLogoImageView.heightAnchor.constraint(equalToConstant: 58).isActive = true
        instagramLogoImageView.widthAnchor.constraint(equalToConstant: 58).isActive = true
        
        scrollView.addSubview(bannerAdsLabel)
        bannerAdsLabel.topAnchor.constraint(equalTo: instagramLogoImageView.bottomAnchor, constant: 30).isActive = true
        bannerAdsLabel.leftAnchor.constraint(equalTo: leftAnchor, constant: 26).isActive = true
        
        scrollView.addSubview(bannerAdsView)
        bannerAdsView.topAnchor.constraint(equalTo: bannerAdsLabel.bottomAnchor, constant: 8).isActive = true
        bannerAdsView.leftAnchor.constraint(equalTo: bannerAdsLabel.leftAnchor).isActive = true
        bannerAdsView.rightAnchor.constraint(equalTo: rightAnchor, constant: -26).isActive = true
        bannerAdsView.heightAnchor.constraint(equalTo: bannerAdsView.widthAnchor, multiplier: 1.0).isActive = true
        
        scrollView.addSubview(bannerAdsPlaceholderImageView)
        bannerAdsPlaceholderImageView.centerXAnchor.constraint(equalTo: bannerAdsView.centerXAnchor).isActive = true
        bannerAdsPlaceholderImageView.centerYAnchor.constraint(equalTo: bannerAdsView.centerYAnchor).isActive = true
        bannerAdsPlaceholderImageView.heightAnchor.constraint(equalToConstant: 72).isActive = true
        bannerAdsPlaceholderImageView.widthAnchor.constraint(equalToConstant: 81).isActive = true
        
        scrollView.addSubview(imagesImagesImageView)
        imagesImagesImageView.topAnchor.constraint(equalTo: bannerAdsView.topAnchor).isActive = true
        imagesImagesImageView.leftAnchor.constraint(equalTo: bannerAdsView.leftAnchor).isActive = true
        imagesImagesImageView.rightAnchor.constraint(equalTo: bannerAdsView.rightAnchor).isActive = true
        imagesImagesImageView.bottomAnchor.constraint(equalTo: bannerAdsView.bottomAnchor).isActive = true
        
        scrollView.addSubview(uploadImagesButton)
        uploadImagesButton.centerXAnchor.constraint(equalTo: bannerAdsView.centerXAnchor).isActive = true
        uploadImagesButton.bottomAnchor.constraint(equalTo: bannerAdsView.bottomAnchor, constant: -44).isActive = true
        uploadImagesButton.heightAnchor.constraint(equalToConstant: 33).isActive = true
        uploadImagesButton.widthAnchor.constraint(equalToConstant: 177).isActive = true
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
