//
//  BannerAdsPositionView.swift
//  dShop Merchant
//
//  Created by Marknel Pogi on 12/7/18.
//  Copyright © 2018 Marknel Pogi. All rights reserved.
//

import UIKit

struct BannerAdsPositionStyleItem {
    let positionId: Int
    let positionTitle: String
    let positionImage: UIImage
}

class BannerAdsPositionView: UIView {
    let items: [BannerAdsPositionStyleItem] = [
        BannerAdsPositionStyleItem(positionId: 1, positionTitle: "Top", positionImage: UIImage(named: "banner_ads_top")!),
        BannerAdsPositionStyleItem(positionId: 2, positionTitle: "Center", positionImage: UIImage(named: "banner_ads_center")!),
        BannerAdsPositionStyleItem(positionId: 3, positionTitle: "Bottom", positionImage: UIImage(named: "banner_ads_bottom")!)
    ]
    
    var selectedIndexRow = 0 {
        didSet {
            collectionView.reloadData()
        }
    }
    
    lazy var collectionView: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        let cv = UICollectionView(frame: .zero, collectionViewLayout: layout)
        cv.backgroundColor = .clear
        cv.delegate = self
        cv.dataSource = self
        cv.translatesAutoresizingMaskIntoConstraints = false
        return cv
    }()
    
    let cellId = "cellId"
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupViews()
    }
    
    private func setupViews() {
        collectionView.register(BannerAdsPositionCell.self, forCellWithReuseIdentifier: cellId)
        addSubview(collectionView)
        collectionView.topAnchor.constraint(equalTo: topAnchor).isActive = true
        collectionView.leftAnchor.constraint(equalTo: leftAnchor).isActive = true
        collectionView.rightAnchor.constraint(equalTo: rightAnchor).isActive = true
        collectionView.bottomAnchor.constraint(equalTo: bottomAnchor).isActive = true
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

// MARK: CollectionView
extension BannerAdsPositionView: UICollectionViewDelegateFlowLayout, UICollectionViewDelegate, UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        selectedIndexRow = indexPath.row
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return items.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellId, for: indexPath) as! BannerAdsPositionCell
        cell.item = items[indexPath.row]
        
        if selectedIndexRow == indexPath.row {
            cell.radioButton.backgroundColor = UIColor(red: 59/255, green: 53/255, blue: 235/255, alpha: 1)
        }
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: frame.width / 3, height: frame.height)
    }
}
