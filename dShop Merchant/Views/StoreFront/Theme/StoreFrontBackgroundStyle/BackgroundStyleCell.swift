//
//  BackgroundStyleCell.swift
//  dShop Merchant
//
//  Created by Marknel Pogi on 12/6/18.
//  Copyright © 2018 Marknel Pogi. All rights reserved.
//

import UIKit

class BackgroundStyleCell: UICollectionViewCell {
    let backgroundStyleImageView: CachedImageView = {
        let iv = CachedImageView()
        iv.image = UIImage(named: "placeholder-image")
        iv.contentMode = .scaleAspectFill
        iv.backgroundColor = .darkGray
        iv.translatesAutoresizingMaskIntoConstraints = false
        return iv
    }()
    
    let titleLabel: UILabel = {
        let lbl = UILabel()
        lbl.text = "Style"
        lbl.font = UIFont(name: "SFUIDisplay-Bold", size: 14)
        lbl.textColor = UIColor(red: 94/255, green: 94/255, blue: 94/255, alpha: 1)
        lbl.translatesAutoresizingMaskIntoConstraints = false
        return lbl
    }()
    
    let radioButton: UIButton = {
        let btn = UIButton(type: .system)
        btn.backgroundColor = UIColor(white: 1, alpha: 0)
        btn.translatesAutoresizingMaskIntoConstraints = false
        btn.layer.cornerRadius = 8
        btn.layer.masksToBounds = true
        btn.layer.borderWidth = 3
        btn.layer.borderColor = UIColor(red: 127/255, green: 196/255, blue: 253/255, alpha: 1).cgColor
        return btn
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupViews()
    }
    
    private func setupViews() {
        addSubview(backgroundStyleImageView)
        backgroundStyleImageView.topAnchor.constraint(equalTo: topAnchor).isActive = true
        backgroundStyleImageView.leftAnchor.constraint(equalTo: leftAnchor, constant: 36).isActive = true
        backgroundStyleImageView.rightAnchor.constraint(equalTo: rightAnchor, constant: -4).isActive = true
        backgroundStyleImageView.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -40).isActive = true
        
        addSubview(titleLabel)
        titleLabel.topAnchor.constraint(equalTo: backgroundStyleImageView.bottomAnchor, constant: 11).isActive = true
        titleLabel.centerXAnchor.constraint(equalTo: backgroundStyleImageView.centerXAnchor).isActive = true
        
        addSubview(radioButton)
        radioButton.centerYAnchor.constraint(equalTo: backgroundStyleImageView.centerYAnchor).isActive = true
        radioButton.rightAnchor.constraint(equalTo: backgroundStyleImageView.leftAnchor, constant: -11).isActive = true
        radioButton.widthAnchor.constraint(equalToConstant: 16).isActive = true
        radioButton.heightAnchor.constraint(equalToConstant: 16).isActive = true
    }
    
    override var isSelected: Bool {
        didSet {
            if isSelected {
                radioButton.backgroundColor = UIColor(red: 59/255, green: 53/255, blue: 235/255, alpha: 1)
            } else {
                radioButton.backgroundColor = UIColor(white: 1, alpha: 0)
            }
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
