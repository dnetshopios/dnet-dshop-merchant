//
//  MainMenuView.swift
//  dShop Merchant
//
//  Created by Marknel Pogi on 11/9/18.
//  Copyright © 2018 Marknel Pogi. All rights reserved.
//

import UIKit

struct MainMenuItem {
    let menuImage: UIImage?
    let menuLabel: String
}

class MainMenuView: UIView {
    var loginDelegate: LoginDelegate?
    var mainMenuDelegate: MainMenuDelegate?
    
    let menuItems: [MainMenuItem] = [
        MainMenuItem(menuImage: UIImage(named: "dashboard"), menuLabel: "Dashboard"),
        MainMenuItem(menuImage: UIImage(named: "cart"), menuLabel: "Orders"),
        MainMenuItem(menuImage: UIImage(named: "mail-1"), menuLabel: "Mails"),
        MainMenuItem(menuImage: UIImage(named: "products"), menuLabel: "Products"),
        MainMenuItem(menuImage: UIImage(named: "invoices"), menuLabel: "Sales Report Analytics"),
        MainMenuItem(menuImage: UIImage(named: "store-front"), menuLabel: "StoreFront Settings"),
        MainMenuItem(menuImage: UIImage(named: "setting"), menuLabel: "Account Settings"),
        MainMenuItem(menuImage: UIImage(named: "help-center"), menuLabel: "Help Center"),
        MainMenuItem(menuImage: UIImage(named: "log-out")?.withRenderingMode(.alwaysTemplate), menuLabel: "Sign Out"),
    ]
    
    lazy var collectionView: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        let cv = UICollectionView(frame: .zero, collectionViewLayout: layout)
        cv.translatesAutoresizingMaskIntoConstraints = false
        cv.backgroundColor = .clear
        cv.delegate = self
        cv.dataSource = self
        return cv
    }()
    
    let cellId = "cellId"
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupViews()
    }
    
    private func setupViews() {
        backgroundColor = UIColor(red: 15/255, green: 39/255, blue: 75/255, alpha: 1)
        
        collectionView.register(MainMenuItemCell.self, forCellWithReuseIdentifier: cellId)
        addSubview(collectionView)
        if #available(iOS 11.0, *) {
            collectionView.topAnchor.constraint(equalTo: safeAreaLayoutGuide.topAnchor, constant: 20).isActive = true
        } else {
            // Fallback on earlier versions
            collectionView.topAnchor.constraint(equalTo: topAnchor, constant: 20).isActive = true
        }
        collectionView.leftAnchor.constraint(equalTo: leftAnchor, constant: 12).isActive = true
        collectionView.rightAnchor.constraint(equalTo: rightAnchor, constant: -20).isActive = true
        if #available(iOS 11.0, *) {
            collectionView.bottomAnchor.constraint(equalTo: safeAreaLayoutGuide.bottomAnchor, constant: -20).isActive = true
        } else {
            // Fallback on earlier versions
            collectionView.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -20).isActive = true
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

// MARK: CollectionView
extension MainMenuView: UICollectionViewDelegateFlowLayout, UICollectionViewDelegate, UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if indexPath.row == 8 {
            loginDelegate?.doShowLogin()
        }
        
        mainMenuDelegate?.doHideMainMenu()
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return menuItems.count
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 10
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellId, for: indexPath) as! MainMenuItemCell
        cell.menuItem = menuItems[indexPath.row]
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: frame.width - 40, height: 40)
    }
}
