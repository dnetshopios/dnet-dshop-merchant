//
//  LoginView.swift
//  dShop Merchant
//
//  Created by Marknel Pogi on 11/28/18.
//  Copyright © 2018 Marknel Pogi. All rights reserved.
//

import UIKit

class LoginView: UIView {
    var loginDelegate: LoginDelegate?
    
    let topImageView: UIImageView = {
        let iv = UIImageView()
        iv.image = UIImage(named: "circuit-right")
        iv.contentMode = .scaleAspectFill
        iv.isOpaque = true
        iv.translatesAutoresizingMaskIntoConstraints = false
        return iv
    }()
    
    let bottomImageView: UIImageView = {
        let iv = UIImageView()
        iv.image = UIImage(named: "login-bottom-bg")
        iv.contentMode = .scaleAspectFill
        iv.isOpaque = true
        iv.translatesAutoresizingMaskIntoConstraints = false
        return iv
    }()
    
    let containerView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    let logoImageView: UIImageView = {
        let iv = UIImageView()
        iv.image = UIImage(named: "dshop_logo_2")
        iv.contentMode = .scaleAspectFit
        iv.translatesAutoresizingMaskIntoConstraints = false
        return iv
    }()
    
    let titleLabel: UILabel = {
        let lbl = UILabel()
        lbl.text = "Welcome Merchant!"
        lbl.font = UIFont(name: "SFUIDisplay-SemiBold", size: 28)
        lbl.textAlignment = .center
        lbl.textColor = .white
        lbl.translatesAutoresizingMaskIntoConstraints = false
        return lbl
    }()
    
    let emailLabel: UILabel = {
        let lbl = UILabel()
        lbl.text = "E-mail"
        lbl.font = UIFont(name: "SFUIDisplay-Bold", size: 14)
        lbl.textColor = UIColor(red: 181/255, green: 181/255, blue: 181/255, alpha: 1)
        lbl.translatesAutoresizingMaskIntoConstraints = false
        return lbl
    }()
    
    lazy var usernameTextField: UITextField = {
        let tf = UITextField()
        tf.font = UIFont(name: "SFUIDisplay-Medium", size: 15)
        tf.placeholder = "Email Address"
        tf.layer.cornerRadius = 10
        tf.layer.masksToBounds = true
        tf.translatesAutoresizingMaskIntoConstraints = false
        tf.backgroundColor = .white
        tf.setLeftPaddingPoints(45)
        tf.setRightPaddingPoints(16)
        tf.addDoneButtonOnKeyboard()
        return tf
    }()
    
    let usernameImageView: UIImageView = {
        let iv = UIImageView()
        iv.image = UIImage(named: "user-icon")
        iv.translatesAutoresizingMaskIntoConstraints = false
        return iv
    }()
    
    let passwordLabel: UILabel = {
        let lbl = UILabel()
        lbl.text = "Password"
        lbl.font = UIFont(name: "SFUIDisplay-Bold", size: 14)
        lbl.textColor = UIColor(red: 181/255, green: 181/255, blue: 181/255, alpha: 1)
        lbl.translatesAutoresizingMaskIntoConstraints = false
        return lbl
    }()
    
    lazy var passwordTextField: UITextField = {
        let tf = UITextField()
        tf.font = UIFont(name: "SFUIDisplay-Medium", size: 15)
        tf.placeholder = "●●●●●●●●●●"
        tf.layer.cornerRadius = 10
        tf.layer.masksToBounds = true
        tf.translatesAutoresizingMaskIntoConstraints = false
        tf.backgroundColor = .white
        tf.setLeftPaddingPoints(45)
        tf.setRightPaddingPoints(16)
        tf.addDoneButtonOnKeyboard()
        return tf
    }()
    
    let passwordImageView: UIImageView = {
        let iv = UIImageView()
        iv.image = UIImage(named: "key-icon")
        iv.translatesAutoresizingMaskIntoConstraints = false
        return iv
    }()
    
    let forgotPasswordButton: UIButton = {
        let btn = UIButton(type: .system)
        btn.setTitle("Forgot Password", for: .normal)
        btn.titleLabel?.font = UIFont(name: "SFUIDisplay-Bold", size: 14)
        btn.tintColor = UIColor(red: 181/255, green: 181/255, blue: 181/255, alpha: 1)
        btn.translatesAutoresizingMaskIntoConstraints = false
        return btn
    }()
    
    lazy var loginButton: UIButton = {
        let btn = UIButton(type: .system)
        btn.setTitle("Login", for: .normal)
        btn.titleLabel?.font = UIFont(name: "SFUIDisplay-Bold", size: 14)
        btn.tintColor = .white
        btn.translatesAutoresizingMaskIntoConstraints = false
        btn.layer.cornerRadius = 10
        btn.layer.masksToBounds = true
        btn.backgroundColor = UIColor(red: 243/255, green: 150/255, blue: 0/255, alpha: 1)
        btn.addTarget(self, action: #selector(handleCloseLogin), for: .touchUpInside)
        return btn
    }()
    
    @objc func handleCloseLogin() {
        loginDelegate?.doHideLogin()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupViews()
    }
    
    private func setupViews() {
        backgroundColor = UIColor(red: 3/255, green: 16/255, blue: 57/255, alpha: 1)
        
        let aspectTopHeight = getAspectNewHeight(width: UIScreen.main.bounds.size.width, originalWidth: 496, originalHeight: 336)
        let aspectBottomHeight = getAspectNewHeight(width: UIScreen.main.bounds.size.width, originalWidth: 472, originalHeight: 272)
        
        addSubview(containerView)
        containerView.centerYAnchor.constraint(equalTo: centerYAnchor).isActive = true
        containerView.leftAnchor.constraint(equalTo: leftAnchor).isActive = true
        containerView.rightAnchor.constraint(equalTo: rightAnchor).isActive = true
        containerView.heightAnchor.constraint(equalToConstant: 472).isActive = true
        
        addSubview(topImageView)
        topImageView.topAnchor.constraint(equalTo: topAnchor).isActive = true
        topImageView.leftAnchor.constraint(equalTo: leftAnchor).isActive = true
        topImageView.rightAnchor.constraint(equalTo: rightAnchor).isActive = true
        topImageView.heightAnchor.constraint(equalToConstant: aspectTopHeight)
        
        addSubview(bottomImageView)
        bottomImageView.heightAnchor.constraint(equalToConstant: aspectBottomHeight).isActive = true
        bottomImageView.bottomAnchor.constraint(equalTo: bottomAnchor).isActive = true
        bottomImageView.leftAnchor.constraint(equalTo: leftAnchor).isActive = true
        bottomImageView.rightAnchor.constraint(equalTo: rightAnchor).isActive = true
        
        addSubview(logoImageView)
        logoImageView.topAnchor.constraint(equalTo: containerView.topAnchor).isActive = true
        logoImageView.centerXAnchor.constraint(equalTo: centerXAnchor).isActive = true
        logoImageView.widthAnchor.constraint(equalToConstant: 193).isActive = true
        logoImageView.heightAnchor.constraint(equalToConstant: 57).isActive = true
        
        addSubview(titleLabel)
        titleLabel.topAnchor.constraint(equalTo: logoImageView.bottomAnchor, constant: 90).isActive = true
        titleLabel.leftAnchor.constraint(equalTo: leftAnchor).isActive = true
        titleLabel.rightAnchor.constraint(equalTo: rightAnchor).isActive = true
        
        addSubview(emailLabel)
        emailLabel.topAnchor.constraint(equalTo: titleLabel.bottomAnchor, constant: 44).isActive = true
        emailLabel.leftAnchor.constraint(equalTo: leftAnchor, constant: 26).isActive = true
        emailLabel.rightAnchor.constraint(equalTo: rightAnchor, constant: -26).isActive = true
        
        addSubview(usernameTextField)
        usernameTextField.topAnchor.constraint(equalTo: emailLabel.bottomAnchor, constant: 4).isActive = true
        usernameTextField.leftAnchor.constraint(equalTo: emailLabel.leftAnchor).isActive = true
        usernameTextField.rightAnchor.constraint(equalTo: emailLabel.rightAnchor).isActive = true
        usernameTextField.heightAnchor.constraint(equalToConstant: 50).isActive = true
        
        addSubview(usernameImageView)
        usernameImageView.centerYAnchor.constraint(equalTo: usernameTextField.centerYAnchor).isActive = true
        usernameImageView.leftAnchor.constraint(equalTo: usernameTextField.leftAnchor, constant: 14).isActive = true
        usernameImageView.widthAnchor.constraint(equalToConstant: 16).isActive = true
        usernameImageView.heightAnchor.constraint(equalToConstant: 16).isActive = true
        
        addSubview(passwordLabel)
        passwordLabel.topAnchor.constraint(equalTo: usernameTextField.bottomAnchor, constant: 6).isActive = true
        passwordLabel.leftAnchor.constraint(equalTo: usernameTextField.leftAnchor).isActive = true
        passwordLabel.rightAnchor.constraint(equalTo: usernameTextField.rightAnchor).isActive = true
        
        addSubview(passwordTextField)
        passwordTextField.topAnchor.constraint(equalTo: passwordLabel.bottomAnchor, constant: 4).isActive = true
        passwordTextField.leftAnchor.constraint(equalTo: passwordLabel.leftAnchor).isActive = true
        passwordTextField.rightAnchor.constraint(equalTo: passwordLabel.rightAnchor).isActive = true
        passwordTextField.heightAnchor.constraint(equalToConstant: 50).isActive = true
        
        addSubview(passwordImageView)
        passwordImageView.centerYAnchor.constraint(equalTo: passwordTextField.centerYAnchor).isActive = true
        passwordImageView.leftAnchor.constraint(equalTo: passwordTextField.leftAnchor, constant: 11).isActive = true
        passwordImageView.widthAnchor.constraint(equalToConstant: 22).isActive = true
        passwordImageView.heightAnchor.constraint(equalToConstant: 12).isActive = true
        
        addSubview(forgotPasswordButton)
        forgotPasswordButton.topAnchor.constraint(equalTo: passwordTextField.bottomAnchor, constant: 2).isActive = true
        forgotPasswordButton.rightAnchor.constraint(equalTo: passwordTextField.rightAnchor).isActive = true
        
        addSubview(loginButton)
        loginButton.topAnchor.constraint(equalTo: forgotPasswordButton.bottomAnchor, constant: 14).isActive = true
        loginButton.leftAnchor.constraint(equalTo: passwordTextField.leftAnchor).isActive = true
        loginButton.rightAnchor.constraint(equalTo: passwordTextField.rightAnchor).isActive = true
        loginButton.heightAnchor.constraint(equalToConstant: 50).isActive = true
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
