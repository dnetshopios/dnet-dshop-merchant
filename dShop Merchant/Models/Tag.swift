//
//  Tag.swift
//  dShop Merchant
//
//  Created by Marknel Pogi on 11/19/18.
//  Copyright © 2018 Marknel Pogi. All rights reserved.
//

import UIKit

class MessagesTag: NSObject {
    var id: Int?
    var tagName: String?
    var tagColor: UIColor?
}
