//
//  SummaryStatistic.swift
//  dShop Merchant
//
//  Created by Marknel Pogi on 11/6/18.
//  Copyright © 2018 Marknel Pogi. All rights reserved.
//

import Foundation

class SummaryStatistic: NSObject {
    var index: Int?
    var title: String?
    var currencySymbol: String?
    var value: String?
    var percentage: Double?
}
