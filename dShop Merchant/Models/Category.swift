//
//  Category.swift
//  dShop Merchant
//
//  Created by Marknel Pogi on 11/27/18.
//  Copyright © 2018 Marknel Pogi. All rights reserved.
//

import UIKit

class Category: NSObject {
    var id: Int?
    var categoryName: String?
}
