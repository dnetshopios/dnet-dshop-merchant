//
//  Message.swift
//  dShop Merchant
//
//  Created by Marknel Pogi on 11/29/18.
//  Copyright © 2018 Marknel Pogi. All rights reserved.
//

import UIKit

class Message: NSObject {
    var id: Int?
    var messageTitle: String?
    var messageContent: String?
}
