//
//  ProductsController.swift
//  dShop Merchant
//
//  Created by Marknel Pogi on 11/9/18.
//  Copyright © 2018 Marknel Pogi. All rights reserved.
//

import UIKit

protocol ProductsMenuDelegate {
    func doCloseMenu()
    func doShowAddCategory()
}

protocol ProductsDelegate {
    func doAddProduct()
}

class ProductsController: UIViewController, ProductsMenuDelegate, ProductsDelegate {
    lazy var collectionView: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        let cv = UICollectionView(frame: .zero, collectionViewLayout: layout)
        cv.translatesAutoresizingMaskIntoConstraints = false
        cv.backgroundColor = .clear
        cv.delegate = self
        cv.dataSource = self
        return cv
    }()
    
    let cellId = "cellId"
    let headerCellId = "headerCellId"
    
    lazy var searchBar: UISearchBar = {
        let sb = UISearchBar()
        sb.placeholder = "All Products"
        sb.barTintColor = .white
        sb.addDoneButtonOnKeyboard()
        // sb.becomeFirstResponder()
        sb.translatesAutoresizingMaskIntoConstraints = false
        return sb
    }()
    
    lazy var searchButton: UIButton = {
        let btn = UIButton(type: .system)
        btn.setImage(UIImage(named: "search")?.withRenderingMode(.alwaysOriginal), for: .normal)
        btn.addTarget(self, action: #selector(handleShowMenu), for: .touchUpInside)
        return btn
    }()
    
    lazy var productsMenuView: ProductsMenuView = {
        let view = ProductsMenuView()
        view.productsMenuDelegate = self
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    lazy var blackView: UIVisualEffectView = {
        let blurEffect = UIBlurEffect(style: UIBlurEffect.Style.dark)
        let view = UIVisualEffectView(effect: blurEffect)
        view.alpha = 0
        view.backgroundColor = .black
        return view
    }()
    
    func loadCategories() {
        var categories = [Category]()
        for i in 0...5 {
            let category = Category()
            category.id = i
            if i == 0 {
                category.categoryName = "Shirt"
            } else if i == 1 {
                category.categoryName = "Dress"
            } else if i == 2 {
                category.categoryName = "Sleeves"
            } else if i == 3 {
                category.categoryName = "Cloacks"
            } else if i == 4 {
                category.categoryName = "Denims"
            } else if i == 5 {
                category.categoryName = "Jeans"
            }
            
            categories.append(category)
        }
        productsMenuView.categories = categories
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupViews()
        setupMenu()
        loadCategories()
    }
    
    private func setupViews() {
        view.backgroundColor = .mainBG
        
        navigationItem.titleView = searchBar
        navigationItem.rightBarButtonItem = UIBarButtonItem(customView: searchButton)
        
        collectionView.register(ProductsHeaderCell.self, forSupplementaryViewOfKind: UICollectionView.elementKindSectionHeader, withReuseIdentifier: headerCellId)
        
        collectionView.register(ProductCell.self, forCellWithReuseIdentifier: cellId)
        view.addSubview(collectionView)
        collectionView.topAnchor.constraint(equalTo: view.topAnchor).isActive = true
        collectionView.leftAnchor.constraint(equalTo: view.leftAnchor).isActive = true
        collectionView.rightAnchor.constraint(equalTo: view.rightAnchor).isActive = true
        collectionView.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true
    }
    
    var window: UIWindow!
    var windowHeight: CGFloat = 0
    var menuBottomConstraint: NSLayoutConstraint!
    var blackViewBottomConstraint: NSLayoutConstraint!
    
    private func setupMenu() {
        window = UIApplication.shared.keyWindow
        windowHeight = window.frame.height
        
        window.addSubview(blackView)
        blackView.frame = window.frame
        
        window.addSubview(productsMenuView)
        productsMenuView.leftAnchor.constraint(equalTo: window.leftAnchor).isActive = true
        productsMenuView.rightAnchor.constraint(equalTo: window.rightAnchor).isActive = true
        productsMenuView.heightAnchor.constraint(equalToConstant: windowHeight).isActive = true
        
        menuBottomConstraint = productsMenuView.bottomAnchor.constraint(equalTo: window.bottomAnchor, constant: windowHeight)
        menuBottomConstraint.isActive = true
    }
    
    public func doCloseMenu() {
        handleHideMenu()
    }
    
    public func doShowAddCategory() {
        handleHideMenu()
        let controller = AddCategoryController()
        navigationController?.present(controller, animated: true, completion: nil)
    }
    
    public func doAddProduct() {
        navigateToViewProductDetails()
    }
    
    public func navigateToViewProductDetails() {
        let controller = ProductDetailsViewController()
        self.present(controller, animated: true, completion: nil)
    }
    
    @objc func handleShowMenu() {
        searchBar.endEditing(true)
        menuBottomConstraint.constant = 0
        UIView.animate(withDuration: 0.2) {
            self.blackView.alpha = 0.8
            self.window.layoutIfNeeded()
        }
    }
    
    @objc func handleHideMenu() {
        menuBottomConstraint.constant = windowHeight
        UIView.animate(withDuration: 0.2) {
            self.blackView.alpha = 0
            self.window.layoutIfNeeded()
        }
    }
}

// MARK: CollectionView
extension ProductsController: UICollectionViewDelegateFlowLayout, UICollectionViewDelegate, UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        let view = collectionView.dequeueReusableSupplementaryView(ofKind: UICollectionView.elementKindSectionHeader, withReuseIdentifier: headerCellId, for: indexPath) as! ProductsHeaderCell
        view.productsDelegate = self
        return view
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
        return CGSize(width: view.frame.width, height: 74)
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 12
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellId, for: indexPath) as! ProductCell
        cell.productsDelegate = self
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 8
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: view.frame.width - 32, height: 100)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 0, left: 16, bottom: 12, right: 16)
    }
}
