//
//  MainTabBarController.swift
//  dShop
//
//  Created by Marknel Pineda on  08/10/18.
//  Copyright © 2018 Marknel Pineda. All rights reserved.
//

import UIKit

class MainTabBarController: UITabBarController {
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupTabStyle()
        setupViewControllers()
    }
    
    // MARK:- Setup Functions
    
    private func setupTabStyle() {
        tabBar.tintColor = .activeTab
        tabBar.barTintColor = .navBG
        tabBar.isTranslucent = false
    }
    
    private func setupViewControllers() {
        let homepageController = HomepageController()
        
        let messagesController = MessagesController()
        
        let productsController = ProductsController()
        
        let storeFrontController = StoreFrontController()
        
        homepageController.userProfileDelegate = storeFrontController
        storeFrontController.mainMenuDelegate = homepageController
        
        viewControllers = [
            generateNavigationController(for: homepageController, title: "Home", image: UIImage(named: "home")!),
            generateNavigationController(for: messagesController, title: "Messages", image: UIImage(named: "mail")!),
            generateNavigationController(for: productsController, title: "Products", image: UIImage(named: "products")!),
            generateNavigationController(for: storeFrontController, title: "Store Front", image: UIImage(named: "store-front")!),
        ]
    }
}
