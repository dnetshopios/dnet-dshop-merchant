//
//  MessageController.swift
//  dShop Merchant
//
//  Created by Marknel Pogi on 12/3/18.
//  Copyright © 2018 Marknel Pogi. All rights reserved.
//

import UIKit

class MessageController: UIViewController {
    var message: Message? {
        didSet {
            if let title = message?.messageTitle {
                navigationItem.title = title
            }
            collectionView.reloadData()
        }
    }
    
    let bottomImageView: UIImageView = {
        let iv = UIImageView()
        iv.image = UIImage(named: "login-bottom-bg")
        iv.contentMode = .scaleAspectFill
        iv.isOpaque = true
        iv.translatesAutoresizingMaskIntoConstraints = false
        return iv
    }()
    
    let safeView: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor(red: 2/255, green: 17/255, blue: 58/255, alpha: 1)
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    let actionsView: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor(red: 2/255, green: 17/255, blue: 58/255, alpha: 1)
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    let leftActionsView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    let rightActionsView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    let replyButton: UIButton = {
        let btn = UIButton(type: .system)
        btn.setTitle("REPLY", for: .normal)
        btn.titleLabel?.font = UIFont(name: "SFUIDisplay-SemiBold", size: 14)
        btn.tintColor = .white
        btn.translatesAutoresizingMaskIntoConstraints = false
        return btn
    }()
    
    let replyImageView: UIImageView = {
        let iv = UIImageView()
        iv.image = UIImage(named: "reply")
        iv.contentMode = .scaleAspectFill
        iv.translatesAutoresizingMaskIntoConstraints = false
        return iv
    }()
    
    let forwardButton: UIButton = {
        let btn = UIButton(type: .system)
        btn.setTitle("FORWARD", for: .normal)
        btn.titleLabel?.font = UIFont(name: "SFUIDisplay-SemiBold", size: 14)
        btn.tintColor = .white
        btn.translatesAutoresizingMaskIntoConstraints = false
        return btn
    }()
    
    let forwardImageView: UIImageView = {
        let iv = UIImageView()
        iv.image = UIImage(named: "forward")
        iv.contentMode = .scaleAspectFill
        iv.translatesAutoresizingMaskIntoConstraints = false
        return iv
    }()
    
    let profileImageView: UIImageView = {
        let iv = UIImageView()
        iv.image = UIImage(named: "placeholder-image")
        iv.layer.cornerRadius = 27
        iv.layer.masksToBounds = true
        iv.contentMode = .scaleAspectFill
        iv.translatesAutoresizingMaskIntoConstraints = false
        return iv
    }()
    
    let profileNameLabel: UILabel = {
        let lbl = UILabel()
        lbl.text = "John Doe"
        lbl.font = UIFont(name: "SFUIDisplay-Bold", size: 12)
        lbl.textColor = .white
        lbl.translatesAutoresizingMaskIntoConstraints = false
        return lbl
    }()
    
    let messageDateTimeLabel: UILabel = {
        let lbl = UILabel()
        lbl.text = "Jan 5, 2018- 5:23pm"
        lbl.font = UIFont(name: "SFUIDisplay-Light", size: 11)
        lbl.textColor = UIColor(red: 96/255, green: 96/255, blue: 96/255, alpha: 1)
        lbl.translatesAutoresizingMaskIntoConstraints = false
        return lbl
    }()
    
    lazy var collectionView: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        let cv = UICollectionView(frame: .zero, collectionViewLayout: layout)
        cv.translatesAutoresizingMaskIntoConstraints = false
        cv.backgroundColor = .clear
        cv.delegate = self
        cv.dataSource = self
        return cv
    }()
    
    let cellId = "cellId"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupViews()
        setupNavigationBar()
    }
    
    private func setupViews() {
        view.backgroundColor = UIColor(red: 0/255, green: 34/255, blue: 87/255, alpha: 1)
        
        let aspectBottomHeight = getAspectNewHeight(width: UIScreen.main.bounds.size.width, originalWidth: 472, originalHeight: 272)
        
        view.addSubview(bottomImageView)
        bottomImageView.heightAnchor.constraint(equalToConstant: aspectBottomHeight).isActive = true
        if #available(iOS 11.0, *) {
            bottomImageView.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor, constant: -55).isActive = true
        } else {
            // Fallback on earlier versions
            bottomImageView.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: -55).isActive = true
        }
        bottomImageView.leftAnchor.constraint(equalTo: view.leftAnchor).isActive = true
        bottomImageView.rightAnchor.constraint(equalTo: view.rightAnchor).isActive = true
        
        view.addSubview(profileImageView)
        profileImageView.topAnchor.constraint(equalTo: view.topAnchor, constant: 20).isActive = true
        profileImageView.leftAnchor.constraint(equalTo: view.leftAnchor, constant: 19).isActive = true
        profileImageView.heightAnchor.constraint(equalToConstant: 54).isActive = true
        profileImageView.widthAnchor.constraint(equalToConstant: 54).isActive = true
        
        view.addSubview(profileNameLabel)
        profileNameLabel.centerYAnchor.constraint(equalTo: profileImageView.centerYAnchor, constant: -8).isActive = true
        profileNameLabel.leftAnchor.constraint(equalTo: profileImageView.rightAnchor, constant: 10).isActive = true
        
        view.addSubview(messageDateTimeLabel)
        messageDateTimeLabel.topAnchor.constraint(equalTo: profileNameLabel.bottomAnchor, constant: 2).isActive = true
        messageDateTimeLabel.leftAnchor.constraint(equalTo: profileNameLabel.leftAnchor).isActive = true
        
        view.addSubview(safeView)
        if #available(iOS 11.0, *) {
            safeView.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor).isActive = true
        } else {
            // Fallback on earlier versions
            safeView.topAnchor.constraint(equalTo: view.bottomAnchor).isActive = true
        }
        safeView.leftAnchor.constraint(equalTo: view.leftAnchor).isActive = true
        safeView.rightAnchor.constraint(equalTo: view.rightAnchor).isActive = true
        safeView.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true
        
        setupActionViews()
        setupCollectionView()
    }
    
    private func setupCollectionView() {
        collectionView.register(MessageInsideCell.self, forCellWithReuseIdentifier: cellId)
        view.addSubview(collectionView)
        collectionView.topAnchor.constraint(equalTo: profileImageView.bottomAnchor, constant: 22).isActive = true
        collectionView.leftAnchor.constraint(equalTo: view.leftAnchor).isActive = true
        collectionView.rightAnchor.constraint(equalTo: view.rightAnchor).isActive = true
        collectionView.bottomAnchor.constraint(equalTo: actionsView.topAnchor).isActive = true
    }
    
    private func setupActionViews() {
        view.addSubview(actionsView)
        actionsView.leftAnchor.constraint(equalTo: view.leftAnchor).isActive = true
        actionsView.rightAnchor.constraint(equalTo: view.rightAnchor).isActive = true
        actionsView.bottomAnchor.constraint(equalTo: safeView.topAnchor).isActive = true
        actionsView.heightAnchor.constraint(equalToConstant: 64).isActive = true
        
        view.addSubview(leftActionsView)
        leftActionsView.topAnchor.constraint(equalTo: actionsView.topAnchor).isActive = true
        leftActionsView.leftAnchor.constraint(equalTo: view.leftAnchor).isActive = true
        leftActionsView.widthAnchor.constraint(equalToConstant: view.frame.width / 2).isActive = true
        leftActionsView.bottomAnchor.constraint(equalTo: actionsView.bottomAnchor).isActive = true
        
        view.addSubview(rightActionsView)
        rightActionsView.topAnchor.constraint(equalTo: actionsView.topAnchor).isActive = true
        rightActionsView.leftAnchor.constraint(equalTo: leftActionsView.rightAnchor).isActive = true
        rightActionsView.rightAnchor.constraint(equalTo: view.rightAnchor).isActive = true
        rightActionsView.bottomAnchor.constraint(equalTo: actionsView.bottomAnchor).isActive = true
        
        view.addSubview(replyButton)
        replyButton.centerXAnchor.constraint(equalTo: leftActionsView.centerXAnchor, constant: -27).isActive = true
        replyButton.centerYAnchor.constraint(equalTo: actionsView.centerYAnchor).isActive = true
        
        view.addSubview(replyImageView)
        replyImageView.leftAnchor.constraint(equalTo: replyButton.rightAnchor, constant: 4).isActive = true
        replyImageView.centerYAnchor.constraint(equalTo: leftActionsView.centerYAnchor).isActive = true
        replyImageView.heightAnchor.constraint(equalToConstant: 27).isActive = true
        replyImageView.widthAnchor.constraint(equalToConstant: 23).isActive = true
        
        view.addSubview(forwardButton)
        forwardButton.centerXAnchor.constraint(equalTo: rightActionsView.centerXAnchor, constant: -27).isActive = true
        forwardButton.centerYAnchor.constraint(equalTo: actionsView.centerYAnchor).isActive = true
        
        view.addSubview(forwardImageView)
        forwardImageView.leftAnchor.constraint(equalTo: forwardButton.rightAnchor, constant: 4).isActive = true
        forwardImageView.centerYAnchor.constraint(equalTo: rightActionsView.centerYAnchor).isActive = true
        forwardImageView.heightAnchor.constraint(equalToConstant: 27).isActive = true
        forwardImageView.widthAnchor.constraint(equalToConstant: 23).isActive = true
    }
    
    private func setupNavigationBar() {
        navigationController?.navigationBar.isTranslucent = false
        navigationController?.navigationBar.barTintColor = UIColor(red: 0/255, green: 34/255, blue: 87/255, alpha: 1)
        navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor(red: 126/255, green: 138/255, blue: 152/255, alpha: 1)]
        navigationController?.navigationBar.tintColor = UIColor(red: 126/255, green: 138/255, blue: 152/255, alpha: 1)
        navigationItem.rightBarButtonItem = UIBarButtonItem(image: UIImage(named: "menu-icon"), style: .plain, target: self, action: #selector(handleShowMenu))
        navigationItem.leftBarButtonItem = UIBarButtonItem(image: UIImage(named: "back_arrow"), style: .plain, target: self, action: #selector(handleDismiss))
    }
    
    @objc func handleShowMenu() {
        //
    }
    
    @objc func handleDismiss() {
        self.dismissDetail()
    }
}

extension MessageController: UICollectionViewDelegateFlowLayout, UICollectionViewDelegate, UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellId, for: indexPath) as! MessageInsideCell
        cell.message = message
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        guard let hmtlString = message?.messageContent else { return .zero }
        let attributedString = try? NSAttributedString(htmlString: hmtlString, font: UIFont(name: "SourceSansPro-Regular", size: 14), useDocumentFontSize: false)
        guard let textHeight = attributedString?.height(withConstrainedWidth: view.frame.width - 58) else { return .zero }
        return CGSize(width: view.frame.width, height: textHeight)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 0, left: 0, bottom: 10, right: 0)
    }
}
