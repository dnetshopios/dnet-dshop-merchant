//
//  AddCategoryController.swift
//  dShop Merchant
//
//  Created by Marknel Pogi on 12/4/18.
//  Copyright © 2018 Marknel Pogi. All rights reserved.
//

import UIKit

class AddCategoryController: UIViewController {
    let subCategories = [
        "Mouse",
        "Keyboard",
        "Monitor",
        "CPU"
    ]
    
    let categoryNameTextField: UITextField = {
        let tf = UITextField()
        tf.addDoneButtonOnKeyboard()
        tf.font = UIFont(name: "SFUIDisplay-Medium", size: 18)
        tf.placeholder = "Category Name"
        tf.layer.cornerRadius = 10
        tf.layer.masksToBounds = true
        tf.translatesAutoresizingMaskIntoConstraints = false
        tf.backgroundColor = .white
        tf.textColor =  UIColor(red: 96/255, green: 96/255, blue: 96/255, alpha: 1)
        tf.setLeftPaddingPoints(16)
        tf.setRightPaddingPoints(16)
        return tf
    }()
    
    lazy var subCategoryTextField: UITextField = {
        let tf = UITextField()
        tf.loadDropdownData(data: subCategories)
        tf.addDoneButtonOnKeyboard()
        tf.font = UIFont(name: "SFUIDisplay-Medium", size: 18)
        tf.placeholder = "Category Name"
        tf.layer.cornerRadius = 10
        tf.layer.masksToBounds = true
        tf.translatesAutoresizingMaskIntoConstraints = false
        tf.backgroundColor = .white
        tf.textColor =  UIColor(red: 96/255, green: 96/255, blue: 96/255, alpha: 1)
        tf.setLeftPaddingPoints(16)
        tf.setRightPaddingPoints(16)
        return tf
    }()
    
    let dropdownArrowImageView: UIImageView = {
        let iv = UIImageView()
        iv.image = UIImage(named: "small-down")?.withRenderingMode(.alwaysTemplate)
        iv.tintColor = UIColor(red: 96/255, green: 96/255, blue: 96/255, alpha: 1)
        iv.translatesAutoresizingMaskIntoConstraints = false
        return iv
    }()
    
    lazy var btnCheckbox: UIButton = {
        let btn = UIButton(type: .system)
        btn.setImage(UIImage(named: "checkbox"), for: .normal)
        btn.tag = 0
        btn.addTarget(self, action: #selector(handleCheck), for: .touchUpInside)
        btn.layer.borderWidth = 2
        btn.layer.cornerRadius = 2
        btn.layer.masksToBounds = true
        btn.imageView?.contentMode = .scaleAspectFit
        btn.layer.borderColor = UIColor(red: 188/255, green: 224/255, blue: 253/255, alpha: 1).cgColor
        btn.contentEdgeInsets = UIEdgeInsets(top: 2, left: 0, bottom: 0, right: 0)
        btn.backgroundColor = .white
        btn.translatesAutoresizingMaskIntoConstraints = false
        return btn
    }()
    
    lazy var closeButton: UIButton = {
        let btn = UIButton(type: .system)
        btn.setImage(UIImage(named: "close"), for: .normal)
        btn.tintColor = UIColor(red: 126/255, green: 138/255, blue: 152/255, alpha: 1)
        btn.addTarget(self, action: #selector(handleClose), for: .touchUpInside)
        btn.translatesAutoresizingMaskIntoConstraints = false
        return btn
    }()
    
    let safeView: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor(red: 31/255, green: 83/255, blue: 223/255, alpha: 1)
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    lazy var saveButton: UIButton = {
        let btn = UIButton(type: .system)
        btn.setTitle("SAVE", for: .normal)
        btn.titleLabel?.font = UIFont(name: "SFUIDisplay-Bold", size: 18)
        btn.tintColor = .white
        btn.backgroundColor = UIColor(red: 31/255, green: 83/255, blue: 223/255, alpha: 1)
        btn.addTarget(self, action: #selector(handleSave), for: .touchUpInside)
        btn.translatesAutoresizingMaskIntoConstraints = false
        return btn
    }()
    
    @objc func handleClose() {
        self.dismiss(animated: true, completion: nil)
    }
    
    @objc func handleSave() {
        print("save")
    }
    
    @objc func handleCheck() {
        if btnCheckbox.tag == 0 {
            btnCheckbox.tintColor = .white
            btnCheckbox.tag = 1
        } else {
            btnCheckbox.tintColor = UIColor(red: 38/255, green: 153/255, blue: 251/255, alpha: 1)
            btnCheckbox.tag = 0
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupViews()
    }
    
    private func setupViews() {
        view.backgroundColor = UIColor(red: 0/255, green: 34/255, blue: 87/255, alpha: 1)

        view.addSubview(closeButton)
        if #available(iOS 11.0, *) {
            closeButton.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor, constant: 14).isActive = true
        } else {
            // Fallback on earlier versions
            closeButton.topAnchor.constraint(equalTo: view.topAnchor, constant: 14).isActive = true
        }
        
        closeButton.leftAnchor.constraint(equalTo: view.leftAnchor, constant: 24).isActive = true
        closeButton.heightAnchor.constraint(equalToConstant: 24).isActive = true
        closeButton.widthAnchor.constraint(equalTo: closeButton.heightAnchor, constant: 1.0).isActive = true
        
        view.addSubview(safeView)
        if #available(iOS 11.0, *) {
            safeView.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor).isActive = true
        } else {
            // Fallback on earlier versions
            safeView.topAnchor.constraint(equalTo: view.bottomAnchor).isActive = true
        }
        safeView.leftAnchor.constraint(equalTo: view.leftAnchor).isActive = true
        safeView.rightAnchor.constraint(equalTo: view.rightAnchor).isActive = true
        safeView.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true
        
        view.addSubview(saveButton)
        saveButton.heightAnchor.constraint(equalToConstant: 64).isActive = true
        saveButton.leftAnchor.constraint(equalTo: view.leftAnchor).isActive = true
        saveButton.rightAnchor.constraint(equalTo: view.rightAnchor).isActive = true
        saveButton.bottomAnchor.constraint(equalTo: safeView.topAnchor).isActive = true
        
        view.addSubview(categoryNameTextField)
        categoryNameTextField.topAnchor.constraint(equalTo: closeButton.bottomAnchor, constant: 56).isActive = true
        categoryNameTextField.leftAnchor.constraint(equalTo: view.leftAnchor, constant: 24).isActive = true
        categoryNameTextField.rightAnchor.constraint(equalTo: view.rightAnchor, constant: -24).isActive = true
        categoryNameTextField.heightAnchor.constraint(equalToConstant: 50).isActive = true
        
        view.addSubview(btnCheckbox)
        btnCheckbox.topAnchor.constraint(equalTo: categoryNameTextField.bottomAnchor, constant: 18).isActive = true
        btnCheckbox.leftAnchor.constraint(equalTo: categoryNameTextField.leftAnchor, constant: 2).isActive = true
        btnCheckbox.heightAnchor.constraint(equalToConstant: 30).isActive = true
        btnCheckbox.widthAnchor.constraint(equalToConstant: 30).isActive = true
        
        view.addSubview(subCategoryTextField)
        subCategoryTextField.topAnchor.constraint(equalTo: btnCheckbox.bottomAnchor, constant: 18).isActive = true
        subCategoryTextField.leftAnchor.constraint(equalTo: categoryNameTextField.leftAnchor).isActive = true
        subCategoryTextField.rightAnchor.constraint(equalTo: categoryNameTextField.rightAnchor).isActive = true
        subCategoryTextField.heightAnchor.constraint(equalToConstant: 50).isActive = true
        
        view.addSubview(dropdownArrowImageView)
        dropdownArrowImageView.centerYAnchor.constraint(equalTo: subCategoryTextField.centerYAnchor).isActive = true
        dropdownArrowImageView.rightAnchor.constraint(equalTo: subCategoryTextField.rightAnchor, constant: -14).isActive = true
        dropdownArrowImageView.heightAnchor.constraint(equalToConstant: 9).isActive = true
        dropdownArrowImageView.widthAnchor.constraint(equalToConstant: 14).isActive = true
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
}
