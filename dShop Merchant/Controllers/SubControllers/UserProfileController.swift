//
//  UserProfileController.swift
//  dShop Merchant
//
//  Created by Marknel Pogi on 12/10/18.
//  Copyright © 2018 Marknel Pogi. All rights reserved.
//

import UIKit

class UserProfileController: UIViewController {
    let scrollView: UIScrollView = {
        let sv = UIScrollView()
        sv.translatesAutoresizingMaskIntoConstraints = false
        return sv
    }()
    
    let accountInformationLabel: UILabel = {
        let lbl = UILabel()
        lbl.text = "Account Information"
        lbl.font = UIFont(name: "SFUIDisplay-SemiBold", size: 20)
        lbl.textColor = UIColor(red: 180/255, green: 180/255, blue: 180/255, alpha: 1)
        lbl.translatesAutoresizingMaskIntoConstraints = false
        return lbl
    }()
    
    let pictureProfileLabel: UILabel = {
        let lbl = UILabel()
        lbl.text = "Picture Profile"
        lbl.font = UIFont(name: "SFUIDisplay-Medium", size: 12)
        lbl.textColor = UIColor(red: 105/255, green: 180/255, blue: 106/255, alpha: 1)
        lbl.translatesAutoresizingMaskIntoConstraints = false
        return lbl
    }()
    
    let profilePictureView: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor(red: 28/255, green: 28/255, blue: 28/255, alpha: 1)
        view.layer.cornerRadius = 120
        view.layer.masksToBounds = true
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    let profilePicturePlaceHolderImageView: UIImageView = {
        let iv = UIImageView()
        iv.image = UIImage(named: "image-placeholder")
        iv.translatesAutoresizingMaskIntoConstraints = false
        return iv
    }()
    
    let uploadLogoLabel: UILabel = {
        let lbl = UILabel()
        lbl.text = "Upload Logo"
        lbl.font = UIFont(name: "SFUIDisplay-SemiBold", size: 14)
        lbl.textColor = UIColor(red: 175/255, green: 175/255, blue: 175/255, alpha: 1)
        lbl.translatesAutoresizingMaskIntoConstraints = false
        return lbl
    }()
    
    let profilePictureImageView: CachedImageView = {
        let iv = CachedImageView()
        iv.translatesAutoresizingMaskIntoConstraints = false
        return iv
    }()
    
    let accountNameLabel: UILabel = {
        let lbl = UILabel()
        lbl.text = "Account Name"
        lbl.font = UIFont(name: "SFUIDisplay-Medium", size: 12)
        lbl.textColor = UIColor(red: 105/255, green: 106/255, blue: 106/255, alpha: 1)
        lbl.translatesAutoresizingMaskIntoConstraints = false
        return lbl
    }()
    
    let accountNameTextLabel: UILabel = {
        let lbl = UILabel()
        lbl.text = "Account Name"
        lbl.font = UIFont(name: "SFUIDisplay-SemiBold", size: 20)
        lbl.textColor = UIColor(red: 207/255, green: 215/255, blue: 223/255, alpha: 1)
        lbl.translatesAutoresizingMaskIntoConstraints = false
        return lbl
    }()
    
    let storeNameLabel: UILabel = {
        let lbl = UILabel()
        lbl.text = "Store Name"
        lbl.font = UIFont(name: "SFUIDisplay-Medium", size: 12)
        lbl.textColor = UIColor(red: 105/255, green: 106/255, blue: 106/255, alpha: 1)
        lbl.translatesAutoresizingMaskIntoConstraints = false
        return lbl
    }()
    
    let storeNameTextLabel: UILabel = {
        let lbl = UILabel()
        lbl.text = "Nike Store"
        lbl.font = UIFont(name: "SFUIDisplay-SemiBold", size: 20)
        lbl.textColor = UIColor(red: 207/255, green: 215/255, blue: 223/255, alpha: 1)
        lbl.translatesAutoresizingMaskIntoConstraints = false
        return lbl
    }()
    
    let storeEmailLabel: UILabel = {
        let lbl = UILabel()
        lbl.text = "Store Email"
        lbl.font = UIFont(name: "SFUIDisplay-Medium", size: 12)
        lbl.textColor = UIColor(red: 105/255, green: 106/255, blue: 106/255, alpha: 1)
        lbl.translatesAutoresizingMaskIntoConstraints = false
        return lbl
    }()
    
    let storeEmailTextLabel: UILabel = {
        let lbl = UILabel()
        lbl.text = "email@store.com"
        lbl.font = UIFont(name: "SFUIDisplay-SemiBold", size: 20)
        lbl.textColor = UIColor(red: 207/255, green: 215/255, blue: 223/255, alpha: 1)
        lbl.translatesAutoresizingMaskIntoConstraints = false
        return lbl
    }()
    
    let usernameLabel: UILabel = {
        let lbl = UILabel()
        lbl.text = "Username"
        lbl.font = UIFont(name: "SFUIDisplay-Medium", size: 12)
        lbl.textColor = UIColor(red: 105/255, green: 106/255, blue: 106/255, alpha: 1)
        lbl.translatesAutoresizingMaskIntoConstraints = false
        return lbl
    }()
    
    let usernameTextLabel: UILabel = {
        let lbl = UILabel()
        lbl.text = "nikeph2018"
        lbl.font = UIFont(name: "SFUIDisplay-SemiBold", size: 20)
        lbl.textColor = UIColor(red: 207/255, green: 215/255, blue: 223/255, alpha: 1)
        lbl.translatesAutoresizingMaskIntoConstraints = false
        return lbl
    }()
    
    let storeTelephoneLabel: UILabel = {
        let lbl = UILabel()
        lbl.text = "Store Telephone"
        lbl.font = UIFont(name: "SFUIDisplay-Medium", size: 12)
        lbl.textColor = UIColor(red: 105/255, green: 106/255, blue: 106/255, alpha: 1)
        lbl.translatesAutoresizingMaskIntoConstraints = false
        return lbl
    }()
    
    let storeTelephoneTextLabel: UILabel = {
        let lbl = UILabel()
        lbl.text = "+42 1664 644"
        lbl.font = UIFont(name: "SFUIDisplay-SemiBold", size: 20)
        lbl.textColor = UIColor(red: 207/255, green: 215/255, blue: 223/255, alpha: 1)
        lbl.translatesAutoresizingMaskIntoConstraints = false
        return lbl
    }()
    
    let storeAddressLabel: UILabel = {
        let lbl = UILabel()
        lbl.text = "Store Address"
        lbl.font = UIFont(name: "SFUIDisplay-Medium", size: 12)
        lbl.textColor = UIColor(red: 105/255, green: 106/255, blue: 106/255, alpha: 1)
        lbl.translatesAutoresizingMaskIntoConstraints = false
        return lbl
    }()
    
    let storeAddressTextLabel: UILabel = {
        let lbl = UILabel()
        lbl.text = "Mandaluyong"
        lbl.font = UIFont(name: "SFUIDisplay-SemiBold", size: 20)
        lbl.textColor = UIColor(red: 207/255, green: 215/255, blue: 223/255, alpha: 1)
        lbl.translatesAutoresizingMaskIntoConstraints = false
        return lbl
    }()
        
    let passwordLabel: UILabel = {
        let lbl = UILabel()
        lbl.text = "Password"
        lbl.font = UIFont(name: "SFUIDisplay-Medium", size: 12)
        lbl.textColor = UIColor(red: 105/255, green: 106/255, blue: 106/255, alpha: 1)
        lbl.translatesAutoresizingMaskIntoConstraints = false
        return lbl
    }()
        
    let passwordTextLabel: UILabel = {
        let lbl = UILabel()
        lbl.text = "***************"
        lbl.font = UIFont(name: "SFUIDisplay-SemiBold", size: 20)
        lbl.textColor = UIColor(red: 207/255, green: 215/255, blue: 223/255, alpha: 1)
        lbl.translatesAutoresizingMaskIntoConstraints = false
        return lbl
    }()
    
    lazy var viewPlanButton: UIButton = {
        let btn = UIButton(type: .system)
        btn.setTitle("View Plan", for: .normal)
        btn.titleLabel?.font = UIFont(name: "SFUIDisplay-Bold", size: 16)
        btn.tintColor = .white
        btn.backgroundColor = UIColor(red: 0/255, green: 55/255, blue: 138/255, alpha: 1)
        btn.layer.cornerRadius = 6
        btn.layer.masksToBounds = true
        btn.addTarget(self, action: #selector(handleViewPlan), for: .touchUpInside)
        btn.translatesAutoresizingMaskIntoConstraints = false
        return btn
    }()
    
    lazy var accountNameEditButton: UIButton = {
        let btn = UIButton(type: .system)
        btn.setImage(UIImage(named: "edit-blue"), for: .normal)
        btn.tintColor = UIColor(red: 115/255, green: 165/255, blue: 252/255, alpha: 1)
        btn.addTarget(self, action: #selector(handleViewPlan), for: .touchUpInside)
        btn.translatesAutoresizingMaskIntoConstraints = false
        return btn
    }()
    
    lazy var storeNameEditButton: UIButton = {
        let btn = UIButton(type: .system)
        btn.setImage(UIImage(named: "edit-blue"), for: .normal)
        btn.tintColor = UIColor(red: 115/255, green: 165/255, blue: 252/255, alpha: 1)
        btn.addTarget(self, action: #selector(handleViewPlan), for: .touchUpInside)
        btn.translatesAutoresizingMaskIntoConstraints = false
        return btn
    }()
    
    lazy var storeEmailEditButton: UIButton = {
        let btn = UIButton(type: .system)
        btn.setImage(UIImage(named: "edit-blue"), for: .normal)
        btn.tintColor = UIColor(red: 115/255, green: 165/255, blue: 252/255, alpha: 1)
        btn.addTarget(self, action: #selector(handleViewPlan), for: .touchUpInside)
        btn.translatesAutoresizingMaskIntoConstraints = false
        return btn
    }()
    
    lazy var userNameEditButton: UIButton = {
        let btn = UIButton(type: .system)
        btn.setImage(UIImage(named: "edit-blue"), for: .normal)
        btn.tintColor = UIColor(red: 115/255, green: 165/255, blue: 252/255, alpha: 1)
        btn.addTarget(self, action: #selector(handleViewPlan), for: .touchUpInside)
        btn.translatesAutoresizingMaskIntoConstraints = false
        return btn
    }()
    
    lazy var storeTelephoneEditButton: UIButton = {
        let btn = UIButton(type: .system)
        btn.setImage(UIImage(named: "edit-blue"), for: .normal)
        btn.tintColor = UIColor(red: 115/255, green: 165/255, blue: 252/255, alpha: 1)
        btn.addTarget(self, action: #selector(handleViewPlan), for: .touchUpInside)
        btn.translatesAutoresizingMaskIntoConstraints = false
        return btn
    }()
    
    lazy var storeAddressEditButton: UIButton = {
        let btn = UIButton(type: .system)
        btn.setImage(UIImage(named: "edit-blue"), for: .normal)
        btn.tintColor = UIColor(red: 115/255, green: 165/255, blue: 252/255, alpha: 1)
        btn.addTarget(self, action: #selector(handleViewPlan), for: .touchUpInside)
        btn.translatesAutoresizingMaskIntoConstraints = false
        return btn
    }()
    
    lazy var passwordEditButton: UIButton = {
        let btn = UIButton(type: .system)
        btn.setImage(UIImage(named: "edit-blue"), for: .normal)
        btn.tintColor = UIColor(red: 115/255, green: 165/255, blue: 252/255, alpha: 1)
        btn.addTarget(self, action: #selector(handleViewPlan), for: .touchUpInside)
        btn.translatesAutoresizingMaskIntoConstraints = false
        return btn
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupViews()
        setupNavigationBar()
    }
    
    @objc func handleViewPlan() {
        print("view plan")
    }
    
    private func setupViews() {
        view.backgroundColor = .mainBG
        view.addSubview(scrollView)
        scrollView.topAnchor.constraint(equalTo: view.topAnchor).isActive = true
        scrollView.leftAnchor.constraint(equalTo: view.leftAnchor).isActive = true
        scrollView.rightAnchor.constraint(equalTo: view.rightAnchor).isActive = true
        scrollView.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true
        scrollView.contentSize = CGSize(width: view.frame.width, height: 930)
        
        scrollView.addSubview(accountInformationLabel)
        accountInformationLabel.topAnchor.constraint(equalTo: scrollView.topAnchor, constant: 20).isActive = true
        accountInformationLabel.leftAnchor.constraint(equalTo: view.leftAnchor, constant: 24).isActive = true
        
        scrollView.addSubview(pictureProfileLabel)
        pictureProfileLabel.topAnchor.constraint(equalTo: accountInformationLabel.bottomAnchor, constant: 8).isActive = true
        pictureProfileLabel.leftAnchor.constraint(equalTo: accountInformationLabel.leftAnchor).isActive = true
        
        scrollView.addSubview(profilePictureView)
        profilePictureView.topAnchor.constraint(equalTo: pictureProfileLabel.bottomAnchor, constant: 8).isActive = true
        profilePictureView.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        profilePictureView.heightAnchor.constraint(equalToConstant: 240).isActive = true
        profilePictureView.widthAnchor.constraint(equalToConstant: 240).isActive = true
        
        profilePictureView.addSubview(profilePicturePlaceHolderImageView)
        profilePicturePlaceHolderImageView.centerXAnchor.constraint(equalTo: profilePictureView.centerXAnchor).isActive = true
        profilePicturePlaceHolderImageView.centerYAnchor.constraint(equalTo: profilePictureView.centerYAnchor).isActive = true
        profilePicturePlaceHolderImageView.widthAnchor.constraint(equalToConstant: 50).isActive = true
        profilePicturePlaceHolderImageView.heightAnchor.constraint(equalToConstant: 40).isActive = true
        
        profilePictureView.addSubview(uploadLogoLabel)
        uploadLogoLabel.topAnchor.constraint(equalTo: profilePicturePlaceHolderImageView.bottomAnchor, constant: 6).isActive = true
        uploadLogoLabel.centerXAnchor.constraint(equalTo: profilePictureView.centerXAnchor).isActive = true

        profilePictureView.addSubview(profilePictureImageView)
        profilePictureImageView.topAnchor.constraint(equalTo: profilePictureView.topAnchor).isActive = true
        profilePictureImageView.leftAnchor.constraint(equalTo: profilePictureView.leftAnchor).isActive = true
        profilePictureImageView.rightAnchor.constraint(equalTo: profilePictureView.rightAnchor).isActive = true
        profilePictureImageView.bottomAnchor.constraint(equalTo: profilePictureImageView.bottomAnchor).isActive = true
        
        scrollView.addSubview(accountNameLabel)
        accountNameLabel.topAnchor.constraint(equalTo: profilePictureView.bottomAnchor, constant: 30).isActive = true
        accountNameLabel.leftAnchor.constraint(equalTo: pictureProfileLabel.leftAnchor).isActive = true
        
        scrollView.addSubview(accountNameTextLabel)
        accountNameTextLabel.topAnchor.constraint(equalTo: accountNameLabel.bottomAnchor, constant: 3).isActive = true
        accountNameTextLabel.leftAnchor.constraint(equalTo: accountNameLabel.leftAnchor, constant: 12).isActive = true
        
        scrollView.addSubview(storeNameLabel)
        storeNameLabel.topAnchor.constraint(equalTo: accountNameTextLabel.bottomAnchor, constant: 24).isActive = true
        storeNameLabel.leftAnchor.constraint(equalTo: accountNameLabel.leftAnchor).isActive = true
        
        scrollView.addSubview(storeNameTextLabel)
        storeNameTextLabel.topAnchor.constraint(equalTo: storeNameLabel.bottomAnchor, constant: 3).isActive = true
        storeNameTextLabel.leftAnchor.constraint(equalTo: accountNameTextLabel.leftAnchor).isActive = true
        
        scrollView.addSubview(storeEmailLabel)
        storeEmailLabel.topAnchor.constraint(equalTo: storeNameTextLabel.bottomAnchor, constant: 24).isActive = true
        storeEmailLabel.leftAnchor.constraint(equalTo: accountNameLabel.leftAnchor).isActive = true
        
        scrollView.addSubview(storeEmailTextLabel)
        storeEmailTextLabel.topAnchor.constraint(equalTo: storeEmailLabel.bottomAnchor, constant: 3).isActive = true
        storeEmailTextLabel.leftAnchor.constraint(equalTo: storeNameTextLabel.leftAnchor).isActive = true
        
        scrollView.addSubview(usernameLabel)
        usernameLabel.topAnchor.constraint(equalTo: storeEmailTextLabel.bottomAnchor, constant: 24).isActive = true
        usernameLabel.leftAnchor.constraint(equalTo: storeEmailLabel.leftAnchor).isActive = true
        
        scrollView.addSubview(usernameTextLabel)
        usernameTextLabel.topAnchor.constraint(equalTo: usernameLabel.bottomAnchor, constant: 3).isActive = true
        usernameTextLabel.leftAnchor.constraint(equalTo: storeEmailTextLabel.leftAnchor).isActive = true
        
        scrollView.addSubview(storeTelephoneLabel)
        storeTelephoneLabel.topAnchor.constraint(equalTo: usernameTextLabel.bottomAnchor, constant: 24).isActive = true
        storeTelephoneLabel.leftAnchor.constraint(equalTo: usernameLabel.leftAnchor).isActive = true
        
        scrollView.addSubview(storeTelephoneTextLabel)
        storeTelephoneTextLabel.topAnchor.constraint(equalTo: storeTelephoneLabel.bottomAnchor, constant: 3).isActive = true
        storeTelephoneTextLabel.leftAnchor.constraint(equalTo: usernameTextLabel.leftAnchor).isActive = true
        
        scrollView.addSubview(storeAddressLabel)
        storeAddressLabel.topAnchor.constraint(equalTo: storeTelephoneTextLabel.bottomAnchor, constant: 24).isActive = true
        storeAddressLabel.leftAnchor.constraint(equalTo: storeTelephoneLabel.leftAnchor).isActive = true
        
        scrollView.addSubview(storeAddressTextLabel)
        storeAddressTextLabel.topAnchor.constraint(equalTo: storeAddressLabel.bottomAnchor, constant: 3).isActive = true
        storeAddressTextLabel.leftAnchor.constraint(equalTo: storeTelephoneTextLabel.leftAnchor).isActive = true
        
        scrollView.addSubview(passwordLabel)
        passwordLabel.topAnchor.constraint(equalTo: storeAddressTextLabel.bottomAnchor, constant: 24).isActive = true
        passwordLabel.leftAnchor.constraint(equalTo: storeAddressLabel.leftAnchor).isActive = true
        
        scrollView.addSubview(passwordTextLabel)
        passwordTextLabel.topAnchor.constraint(equalTo: passwordLabel.bottomAnchor, constant: 3).isActive = true
        passwordTextLabel.leftAnchor.constraint(equalTo: storeAddressTextLabel.leftAnchor).isActive = true
        
        scrollView.addSubview(viewPlanButton)
        viewPlanButton.topAnchor.constraint(equalTo: passwordTextLabel.bottomAnchor, constant: 24).isActive = true
        viewPlanButton.leftAnchor.constraint(equalTo: view.leftAnchor, constant: 24).isActive = true
        viewPlanButton.rightAnchor.constraint(equalTo: view.rightAnchor, constant: -24).isActive = true
        viewPlanButton.heightAnchor.constraint(equalToConstant: 45).isActive = true
        
        scrollView.addSubview(accountNameEditButton)
        accountNameEditButton.rightAnchor.constraint(equalTo: view.rightAnchor, constant: -24).isActive = true
        accountNameEditButton.widthAnchor.constraint(equalToConstant: 15).isActive = true
        accountNameEditButton.heightAnchor.constraint(equalToConstant: 15).isActive = true
        accountNameEditButton.topAnchor.constraint(equalTo: accountNameTextLabel.topAnchor, constant: 6).isActive = true
        
        scrollView.addSubview(storeNameEditButton)
        storeNameEditButton.rightAnchor.constraint(equalTo: accountNameEditButton.rightAnchor).isActive = true
        storeNameEditButton.widthAnchor.constraint(equalToConstant: 15).isActive = true
        storeNameEditButton.heightAnchor.constraint(equalToConstant: 15).isActive = true
        storeNameEditButton.topAnchor.constraint(equalTo: storeNameTextLabel.topAnchor, constant: 6).isActive = true
        
        scrollView.addSubview(storeEmailEditButton)
        storeEmailEditButton.rightAnchor.constraint(equalTo: storeNameEditButton.rightAnchor).isActive = true
        storeEmailEditButton.widthAnchor.constraint(equalToConstant: 15).isActive = true
        storeEmailEditButton.heightAnchor.constraint(equalToConstant: 15).isActive = true
        storeEmailEditButton.topAnchor.constraint(equalTo: storeEmailTextLabel.topAnchor, constant: 6).isActive = true
        
        scrollView.addSubview(userNameEditButton)
        userNameEditButton.rightAnchor.constraint(equalTo: storeEmailEditButton.rightAnchor).isActive = true
        userNameEditButton.widthAnchor.constraint(equalToConstant: 15).isActive = true
        userNameEditButton.heightAnchor.constraint(equalToConstant: 15).isActive = true
        userNameEditButton.topAnchor.constraint(equalTo: usernameTextLabel.topAnchor, constant: 6).isActive = true
        
        scrollView.addSubview(storeTelephoneEditButton)
        storeTelephoneEditButton.rightAnchor.constraint(equalTo: userNameEditButton.rightAnchor).isActive = true
        storeTelephoneEditButton.widthAnchor.constraint(equalToConstant: 15).isActive = true
        storeTelephoneEditButton.heightAnchor.constraint(equalToConstant: 15).isActive = true
        storeTelephoneEditButton.topAnchor.constraint(equalTo: storeTelephoneTextLabel.topAnchor, constant: 6).isActive = true
        
        scrollView.addSubview(storeAddressEditButton)
        storeAddressEditButton.rightAnchor.constraint(equalTo: storeTelephoneEditButton.rightAnchor).isActive = true
        storeAddressEditButton.widthAnchor.constraint(equalToConstant: 15).isActive = true
        storeAddressEditButton.heightAnchor.constraint(equalToConstant: 15).isActive = true
        storeAddressEditButton.topAnchor.constraint(equalTo: storeAddressTextLabel.topAnchor, constant: 6).isActive = true
        
        scrollView.addSubview(passwordEditButton)
        passwordEditButton.rightAnchor.constraint(equalTo: storeAddressEditButton.rightAnchor).isActive = true
        passwordEditButton.widthAnchor.constraint(equalToConstant: 15).isActive = true
        passwordEditButton.heightAnchor.constraint(equalToConstant: 15).isActive = true
        passwordEditButton.topAnchor.constraint(equalTo: passwordTextLabel.topAnchor, constant: 6).isActive = true
    }
    
    private func setupNavigationBar() {
        let dshopImageView = UIImageView()
        navigationItem.titleView = dshopImageView.dshopLogo()
        navigationController?.navigationBar.tintColor = .white
    }
}
