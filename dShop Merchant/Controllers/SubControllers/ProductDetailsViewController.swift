//
//  ProductDetailsViewController.swift
//  dShop Merchant
//
//  Created by Marknel Pogi on 12/5/18.
//  Copyright © 2018 Marknel Pogi. All rights reserved.
//

import UIKit

class ProductDetailsViewController: UIViewController {
    var categories = [
        "Mouse",
        "Keyboard",
        "Monitor",
        "CPU"
    ]
    
    let scrollView: UIScrollView = {
        let sv = UIScrollView()
        sv.translatesAutoresizingMaskIntoConstraints = false
        return sv
    }()
    
    let safeView: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor(red: 31/255, green: 83/255, blue: 223/255, alpha: 1)
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    let productImagesLabel: UILabel = {
        let lbl = UILabel()
        lbl.text = "Product Images"
        lbl.font = UIFont(name: "SFUIDisplay-SemiBold", size: 16)
        lbl.textColor = UIColor(red: 180/255, green: 180/255, blue: 180/255, alpha: 1)
        lbl.translatesAutoresizingMaskIntoConstraints = false
        return lbl
    }()
    
    let placeholderView: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor(red: 28/255, green: 28/255, blue: 28/255, alpha: 1)
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    let imagesPlaceholderImageView: UIImageView = {
        let iv = UIImageView()
        iv.image = UIImage(named: "placeholder_to_images")
        iv.contentMode = .scaleAspectFit
        iv.translatesAutoresizingMaskIntoConstraints = false
        return iv
    }()
    
    let productImagesImageView: CachedImageView = {
        let iv = CachedImageView()
        iv.contentMode = .scaleAspectFit
        iv.translatesAutoresizingMaskIntoConstraints = false
        return iv
    }()
    
    lazy var uploadImagesButton: UIButton = {
        let btn = UIButton(type: .system)
        btn.setTitle("Upload Images", for: .normal)
        btn.titleLabel?.font = UIFont(name: "SFUIDisplay-Bold", size: 12)
        btn.tintColor = UIColor(red: 207/255, green: 207/255, blue: 207/255, alpha: 1)
        btn.backgroundColor = UIColor(red: 126/255, green: 126/255, blue: 126/255, alpha: 1)
        btn.layer.cornerRadius = 4
        btn.layer.masksToBounds = true
        btn.addTarget(self, action: #selector(handleUploadImages), for: .touchUpInside)
        btn.translatesAutoresizingMaskIntoConstraints = false
        return btn
    }()
    
    let nameLabel: UILabel = {
        let lbl = UILabel()
        lbl.text = "Name"
        lbl.font = UIFont(name: "SFUIDisplay-SemiBold", size: 12)
        lbl.textColor = UIColor(red: 180/255, green: 180/255, blue: 180/255, alpha: 1)
        lbl.translatesAutoresizingMaskIntoConstraints = false
        return lbl
    }()
    
    let nameTextField: UITextField = {
        let tf = UITextField()
        tf.addDoneButtonOnKeyboard()
        tf.font = UIFont(name: "SFUIDisplay-SemiBold", size: 18)
        tf.placeholder = "Product Name"
        tf.layer.cornerRadius = 10
        tf.layer.masksToBounds = true
        tf.backgroundColor = .white
        tf.textColor = UIColor(red: 15/255, green: 36/255, blue: 67/255, alpha: 1)
        tf.setLeftPaddingPoints(16)
        tf.setRightPaddingPoints(16)
        tf.translatesAutoresizingMaskIntoConstraints = false
        return tf
    }()
    
    let priceLabel: UILabel = {
        let lbl = UILabel()
        lbl.text = "Price"
        lbl.font = UIFont(name: "SFUIDisplay-SemiBold", size: 12)
        lbl.textColor = UIColor(red: 180/255, green: 180/255, blue: 180/255, alpha: 1)
        lbl.translatesAutoresizingMaskIntoConstraints = false
        return lbl
    }()
    
    let priceCurrencySymbolLabel: UILabel = {
        let lbl = UILabel()
        lbl.text = "₱"
        lbl.font = UIFont(name: "SFUIDisplay-SemiBold", size: 25)
        lbl.textColor = UIColor(red: 15/255, green: 36/255, blue: 67/255, alpha: 1)
        lbl.translatesAutoresizingMaskIntoConstraints = false
        return lbl
    }()
    
    let priceTextField: UITextField = {
        let tf = UITextField()
        tf.addDoneButtonOnKeyboard()
        tf.font = UIFont(name: "SFUIDisplay-Medium", size: 18)
        tf.placeholder = "Price"
        tf.layer.cornerRadius = 10
        tf.layer.masksToBounds = true
        tf.backgroundColor = .white
        tf.textColor =  UIColor(red: 15/255, green: 36/255, blue: 67/255, alpha: 1)
        tf.setLeftPaddingPoints(48)
        tf.setRightPaddingPoints(16)
        tf.translatesAutoresizingMaskIntoConstraints = false
        return tf
    }()
    
    let salePriceLabel: UILabel = {
        let lbl = UILabel()
        lbl.text = "Sale Price"
        lbl.font = UIFont(name: "SFUIDisplay-SemiBold", size: 12)
        lbl.textColor = UIColor(red: 180/255, green: 180/255, blue: 180/255, alpha: 1)
        lbl.translatesAutoresizingMaskIntoConstraints = false
        return lbl
    }()
    
    let priceCurrencyLabel: UILabel = {
        let lbl = UILabel()
        lbl.text = "PHP"
        lbl.font = UIFont(name: "SFUIDisplay-SemiBold", size: 18)
        lbl.textColor = UIColor(red: 15/255, green: 36/255, blue: 67/255, alpha: 1)
        lbl.translatesAutoresizingMaskIntoConstraints = false
        return lbl
    }()
    
    let salePriceTextField: UITextField = {
        let tf = UITextField()
        tf.addDoneButtonOnKeyboard()
        tf.font = UIFont(name: "SFUIDisplay-Medium", size: 18)
        tf.placeholder = "Sale Price"
        tf.layer.cornerRadius = 10
        tf.layer.masksToBounds = true
        tf.backgroundColor = .white
        tf.textColor =  UIColor(red: 15/255, green: 36/255, blue: 67/255, alpha: 1)
        tf.setLeftPaddingPoints(57)
        tf.setRightPaddingPoints(16)
        tf.translatesAutoresizingMaskIntoConstraints = false
        return tf
    }()
    
    let categoriesLabel: UILabel = {
        let lbl = UILabel()
        lbl.text = "Categories"
        lbl.font = UIFont(name: "SFUIDisplay-SemiBold", size: 12)
        lbl.textColor = UIColor(red: 180/255, green: 180/255, blue: 180/255, alpha: 1)
        lbl.translatesAutoresizingMaskIntoConstraints = false
        return lbl
    }()
    
    lazy var categoriesTextField: UITextField = {
        let tf = UITextField()
        tf.loadDropdownData(data: categories)
        tf.addDoneButtonOnKeyboard()
        tf.font = UIFont(name: "SFUIDisplay-SemiBold", size: 18)
        tf.placeholder = "Category Name"
        tf.layer.cornerRadius = 10
        tf.layer.masksToBounds = true
        tf.translatesAutoresizingMaskIntoConstraints = false
        tf.backgroundColor = .white
        tf.textColor =  UIColor(red: 15/255, green: 36/255, blue: 67/255, alpha: 1)
        tf.setLeftPaddingPoints(16)
        tf.setRightPaddingPoints(16)
        return tf
    }()
    
    let dropdownArrowImageView: UIImageView = {
        let iv = UIImageView()
        iv.image = UIImage(named: "small-down")?.withRenderingMode(.alwaysTemplate)
        iv.tintColor = UIColor(red: 96/255, green: 96/255, blue: 96/255, alpha: 1)
        iv.translatesAutoresizingMaskIntoConstraints = false
        return iv
    }()
    
    let stocksLabel: UILabel = {
        let lbl = UILabel()
        lbl.text = "Stocks"
        lbl.font = UIFont(name: "SFUIDisplay-SemiBold", size: 12)
        lbl.textColor = UIColor(red: 180/255, green: 180/255, blue: 180/255, alpha: 1)
        lbl.translatesAutoresizingMaskIntoConstraints = false
        return lbl
    }()
    
    let stocksTextField: UITextField = {
        let tf = UITextField()
        tf.addDoneButtonOnKeyboard()
        tf.font = UIFont(name: "SFUIDisplay-SemiBold", size: 18)
        tf.placeholder = "Stocks"
        tf.layer.cornerRadius = 10
        tf.layer.masksToBounds = true
        tf.backgroundColor = .white
        tf.textColor = UIColor(red: 15/255, green: 36/255, blue: 67/255, alpha: 1)
        tf.setLeftPaddingPoints(16)
        tf.setRightPaddingPoints(16)
        tf.translatesAutoresizingMaskIntoConstraints = false
        return tf
    }()
    
    lazy var setAsNewArrivalCheckbox: UIButton = {
        let btn = UIButton(type: .system)
        btn.setImage(UIImage(named: "checkbox"), for: .normal)
        btn.tag = 0
        btn.addTarget(self, action: #selector(handleSetAsNewArrivalCheck), for: .touchUpInside)
        btn.layer.borderWidth = 2
        btn.layer.cornerRadius = 2
        btn.layer.masksToBounds = true
        btn.imageView?.contentMode = .scaleAspectFit
        btn.layer.borderColor = UIColor(red: 188/255, green: 224/255, blue: 253/255, alpha: 1).cgColor
        btn.contentEdgeInsets = UIEdgeInsets(top: 2, left: 0, bottom: 0, right: 0)
        btn.backgroundColor = .white
        btn.translatesAutoresizingMaskIntoConstraints = false
        return btn
    }()
    
    let setAsNewArrivalLabel: UILabel = {
        let lbl = UILabel()
        lbl.text = "Set as New Arrival"
        lbl.font = UIFont(name: "SFUIDisplay-SemiBold", size: 14)
        lbl.textColor = UIColor(red: 180/255, green: 180/255, blue: 180/255, alpha: 1)
        lbl.translatesAutoresizingMaskIntoConstraints = false
        return lbl
    }()
    
    let salePercentageLabel: UILabel = {
        let lbl = UILabel()
        lbl.text = "Sale Percentage"
        lbl.font = UIFont(name: "SFUIDisplay-SemiBold", size: 12)
        lbl.textColor = UIColor(red: 180/255, green: 180/255, blue: 180/255, alpha: 1)
        lbl.translatesAutoresizingMaskIntoConstraints = false
        return lbl
    }()
    
    let salePercentageTextField: UITextField = {
        let tf = UITextField()
        tf.addDoneButtonOnKeyboard()
        tf.font = UIFont(name: "SFUIDisplay-Medium", size: 18)
        tf.placeholder = "Sale Percentage"
        tf.layer.cornerRadius = 10
        tf.layer.masksToBounds = true
        tf.backgroundColor = .white
        tf.textColor =  UIColor(red: 15/255, green: 36/255, blue: 67/255, alpha: 1)
        tf.setLeftPaddingPoints(16)
        tf.setRightPaddingPoints(16)
        tf.translatesAutoresizingMaskIntoConstraints = false
        return tf
    }()
    
    lazy var setAsSaleCheckbox: UIButton = {
        let btn = UIButton(type: .system)
        btn.setImage(UIImage(named: "checkbox"), for: .normal)
        btn.tag = 0
        btn.addTarget(self, action: #selector(handleSetAsSaleCheck), for: .touchUpInside)
        btn.layer.borderWidth = 2
        btn.layer.cornerRadius = 2
        btn.layer.masksToBounds = true
        btn.imageView?.contentMode = .scaleAspectFit
        btn.layer.borderColor = UIColor(red: 188/255, green: 224/255, blue: 253/255, alpha: 1).cgColor
        btn.contentEdgeInsets = UIEdgeInsets(top: 2, left: 0, bottom: 0, right: 0)
        btn.backgroundColor = .white
        btn.translatesAutoresizingMaskIntoConstraints = false
        return btn
    }()
    
    let setAsSaleLabel: UILabel = {
        let lbl = UILabel()
        lbl.text = "Set as Sale"
        lbl.font = UIFont(name: "SFUIDisplay-SemiBold", size: 14)
        lbl.textColor = .red
        lbl.translatesAutoresizingMaskIntoConstraints = false
        return lbl
    }()
    
    let descriptionLabel: UILabel = {
        let lbl = UILabel()
        lbl.text = "Description"
        lbl.font = UIFont(name: "SFUIDisplay-SemiBold", size: 12)
        lbl.textColor = UIColor(red: 180/255, green: 180/255, blue: 180/255, alpha: 1)
        lbl.translatesAutoresizingMaskIntoConstraints = false
        return lbl
    }()
    
    let descriptionTextView: UITextView = {
        let tv = UITextView()
        tv.addDoneButtonOnKeyboard()
        tv.font = UIFont(name: "SFUIDisplay-Light", size: 18)
        tv.layer.cornerRadius = 10
        tv.layer.masksToBounds = true
        tv.backgroundColor = UIColor(red: 243/255, green: 243/255, blue: 243/255, alpha: 1)
        tv.textColor =  UIColor(red: 77/255, green: 77/255, blue: 77/255, alpha: 1)
        tv.translatesAutoresizingMaskIntoConstraints = false
        tv.isScrollEnabled = true
        tv.textContainerInset = UIEdgeInsets(top: 16, left: 16, bottom: 16, right: 16)
        return tv
    }()
    
    lazy var publishButton: UIButton = {
        let btn = UIButton(type: .system)
        btn.setTitle("PUBLISH", for: .normal)
        btn.titleLabel?.font = UIFont(name: "SFUIDisplay-Bold", size: 18)
        btn.tintColor = .white
        btn.backgroundColor = UIColor(red: 31/255, green: 83/255, blue: 223/255, alpha: 1)
        btn.addTarget(self, action: #selector(handlePublish), for: .touchUpInside)
        btn.translatesAutoresizingMaskIntoConstraints = false
        return btn
    }()
    
    lazy var closeButton: UIButton = {
        let btn = UIButton(type: .system)
        btn.setImage(UIImage(named: "close"), for: .normal)
        btn.tintColor = UIColor(red: 126/255, green: 138/255, blue: 152/255, alpha: 1)
        btn.addTarget(self, action: #selector(handleClose), for: .touchUpInside)
        btn.translatesAutoresizingMaskIntoConstraints = false
        return btn
    }()
    
    @objc func handleSetAsNewArrivalCheck() {
        if setAsNewArrivalCheckbox.tag == 0 {
            setAsNewArrivalCheckbox.tintColor = .white
            setAsNewArrivalCheckbox.tag = 1
        } else {
            setAsNewArrivalCheckbox.tintColor = UIColor(red: 38/255, green: 153/255, blue: 251/255, alpha: 1)
            setAsNewArrivalCheckbox.tag = 0
        }
    }
    
    @objc func handleSetAsSaleCheck() {
        if setAsSaleCheckbox.tag == 0 {
            setAsSaleCheckbox.tintColor = .white
            setAsSaleCheckbox.tag = 1
        } else {
            setAsSaleCheckbox.tintColor = UIColor(red: 38/255, green: 153/255, blue: 251/255, alpha: 1)
            setAsSaleCheckbox.tag = 0
        }
    }
    
    @objc func handleClose() {
        self.dismiss(animated: true, completion: nil)
    }
    
    @objc func handlePublish() {
        print("Publish")
    }
    
    @objc func handleUploadImages() {
        print("Upload Images")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupViews()
    }
    
    private func setupViews() {
        view.backgroundColor = UIColor(red: 0/255, green: 34/255, blue: 87/255, alpha: 1)
        
        view.addSubview(closeButton)
        if #available(iOS 11.0, *) {
            closeButton.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor, constant: 14).isActive = true
        } else {
            // Fallback on earlier versions
            closeButton.topAnchor.constraint(equalTo: view.topAnchor, constant: 14).isActive = true
        }
        
        closeButton.leftAnchor.constraint(equalTo: view.leftAnchor, constant: 24).isActive = true
        closeButton.heightAnchor.constraint(equalToConstant: 24).isActive = true
        closeButton.widthAnchor.constraint(equalTo: closeButton.heightAnchor, constant: 1.0).isActive = true
        
        view.addSubview(safeView)
        if #available(iOS 11.0, *) {
            safeView.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor).isActive = true
        } else {
            // Fallback on earlier versions
            safeView.topAnchor.constraint(equalTo: view.bottomAnchor).isActive = true
        }
        safeView.leftAnchor.constraint(equalTo: view.leftAnchor).isActive = true
        safeView.rightAnchor.constraint(equalTo: view.rightAnchor).isActive = true
        safeView.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true
        
        view.addSubview(publishButton)
        publishButton.heightAnchor.constraint(equalToConstant: 60).isActive = true
        publishButton.leftAnchor.constraint(equalTo: view.leftAnchor).isActive = true
        publishButton.rightAnchor.constraint(equalTo: view.rightAnchor).isActive = true
        publishButton.bottomAnchor.constraint(equalTo: safeView.topAnchor).isActive = true
        
        view.addSubview(scrollView)
        scrollView.topAnchor.constraint(equalTo: closeButton.bottomAnchor, constant: 20).isActive = true
        scrollView.leftAnchor.constraint(equalTo: view.leftAnchor).isActive = true
        scrollView.rightAnchor.constraint(equalTo: view.rightAnchor).isActive = true
        scrollView.bottomAnchor.constraint(equalTo: publishButton.topAnchor).isActive = true
        scrollView.contentSize = CGSize(width: view.frame.width, height: 1060 + (view.frame.width - 48))
        
        scrollView.addSubview(productImagesLabel)
        productImagesLabel.topAnchor.constraint(equalTo: scrollView.topAnchor).isActive = true
        productImagesLabel.leftAnchor.constraint(equalTo: scrollView.leftAnchor, constant: 24).isActive = true
        
        scrollView.addSubview(placeholderView)
        placeholderView.topAnchor.constraint(equalTo: productImagesLabel.bottomAnchor, constant: 8).isActive = true
        placeholderView.leftAnchor.constraint(equalTo: view.leftAnchor, constant: 24).isActive = true
        placeholderView.rightAnchor.constraint(equalTo: view.rightAnchor, constant: -24).isActive = true
        placeholderView.heightAnchor.constraint(equalToConstant: view.frame.width - 48).isActive = true
        
        scrollView.addSubview(imagesPlaceholderImageView)
        imagesPlaceholderImageView.centerXAnchor.constraint(equalTo: placeholderView.centerXAnchor).isActive = true
        imagesPlaceholderImageView.centerYAnchor.constraint(equalTo: placeholderView.centerYAnchor).isActive = true
        imagesPlaceholderImageView.heightAnchor.constraint(equalToConstant: 72).isActive = true
        imagesPlaceholderImageView.widthAnchor.constraint(equalToConstant: 81).isActive = true
        
        scrollView.addSubview(productImagesImageView)
        productImagesImageView.topAnchor.constraint(equalTo: placeholderView.topAnchor).isActive = true
        productImagesImageView.leftAnchor.constraint(equalTo: placeholderView.leftAnchor).isActive = true
        productImagesImageView.rightAnchor.constraint(equalTo: placeholderView.rightAnchor).isActive = true
        productImagesImageView.bottomAnchor.constraint(equalTo: placeholderView.bottomAnchor).isActive = true
        
        scrollView.addSubview(uploadImagesButton)
        uploadImagesButton.centerXAnchor.constraint(equalTo: placeholderView.centerXAnchor).isActive = true
        uploadImagesButton.bottomAnchor.constraint(equalTo: placeholderView.bottomAnchor, constant: -44).isActive = true
        uploadImagesButton.heightAnchor.constraint(equalToConstant: 33).isActive = true
        uploadImagesButton.widthAnchor.constraint(equalToConstant: 177).isActive = true
        
        scrollView.addSubview(nameLabel)
        nameLabel.topAnchor.constraint(equalTo: placeholderView.bottomAnchor, constant: 20).isActive = true
        nameLabel.leftAnchor.constraint(equalTo: placeholderView.leftAnchor).isActive = true
        
        scrollView.addSubview(nameTextField)
        nameTextField.topAnchor.constraint(equalTo: nameLabel.bottomAnchor, constant: 4).isActive = true
        nameTextField.leftAnchor.constraint(equalTo: placeholderView.leftAnchor).isActive = true
        nameTextField.rightAnchor.constraint(equalTo: placeholderView.rightAnchor).isActive = true
        nameTextField.heightAnchor.constraint(equalToConstant: 50).isActive = true
        
        scrollView.addSubview(priceLabel)
        priceLabel.topAnchor.constraint(equalTo: nameTextField.bottomAnchor, constant: 8).isActive = true
        priceLabel.leftAnchor.constraint(equalTo: nameTextField.leftAnchor).isActive = true
        
        scrollView.addSubview(priceTextField)
        priceTextField.topAnchor.constraint(equalTo: priceLabel.bottomAnchor, constant: 4).isActive = true
        priceTextField.leftAnchor.constraint(equalTo: nameTextField.leftAnchor).isActive = true
        priceTextField.rightAnchor.constraint(equalTo: nameTextField.rightAnchor).isActive = true
        priceTextField.heightAnchor.constraint(equalToConstant: 50).isActive = true
        
        scrollView.addSubview(priceCurrencySymbolLabel)
        priceCurrencySymbolLabel.centerYAnchor.constraint(equalTo: priceTextField.centerYAnchor).isActive = true
        priceCurrencySymbolLabel.leftAnchor.constraint(equalTo: priceTextField.leftAnchor, constant: 16).isActive = true
        
        scrollView.addSubview(salePriceLabel)
        salePriceLabel.topAnchor.constraint(equalTo: priceTextField.bottomAnchor, constant: 8).isActive = true
        salePriceLabel.leftAnchor.constraint(equalTo: priceTextField.leftAnchor).isActive = true
        
        scrollView.addSubview(salePriceTextField)
        salePriceTextField.topAnchor.constraint(equalTo: salePriceLabel.bottomAnchor, constant: 4).isActive = true
        salePriceTextField.leftAnchor.constraint(equalTo: priceTextField.leftAnchor).isActive = true
        salePriceTextField.rightAnchor.constraint(equalTo: priceTextField.rightAnchor).isActive = true
        salePriceTextField.heightAnchor.constraint(equalToConstant: 50).isActive = true
        
        scrollView.addSubview(priceCurrencyLabel)
        priceCurrencyLabel.centerYAnchor.constraint(equalTo: salePriceTextField.centerYAnchor).isActive = true
        priceCurrencyLabel.leftAnchor.constraint(equalTo: salePriceTextField.leftAnchor, constant: 16).isActive = true
        
        scrollView.addSubview(categoriesLabel)
        categoriesLabel.topAnchor.constraint(equalTo: salePriceTextField.bottomAnchor, constant: 8).isActive = true
        categoriesLabel.leftAnchor.constraint(equalTo: salePriceTextField.leftAnchor).isActive = true
        
        scrollView.addSubview(categoriesTextField)
        categoriesTextField.topAnchor.constraint(equalTo: categoriesLabel.bottomAnchor, constant: 4).isActive = true
        categoriesTextField.leftAnchor.constraint(equalTo: salePriceTextField.leftAnchor).isActive = true
        categoriesTextField.rightAnchor.constraint(equalTo: salePriceTextField.rightAnchor).isActive = true
        categoriesTextField.heightAnchor.constraint(equalToConstant: 50).isActive = true
        
        scrollView.addSubview(dropdownArrowImageView)
        dropdownArrowImageView.centerYAnchor.constraint(equalTo: categoriesTextField.centerYAnchor).isActive = true
        dropdownArrowImageView.rightAnchor.constraint(equalTo: categoriesTextField.rightAnchor, constant: -14).isActive = true
        dropdownArrowImageView.heightAnchor.constraint(equalToConstant: 9).isActive = true
        dropdownArrowImageView.widthAnchor.constraint(equalToConstant: 14).isActive = true
        
        scrollView.addSubview(stocksLabel)
        stocksLabel.topAnchor.constraint(equalTo: categoriesTextField.bottomAnchor, constant: 8).isActive = true
        stocksLabel.leftAnchor.constraint(equalTo: categoriesTextField.leftAnchor).isActive = true
        
        scrollView.addSubview(stocksTextField)
        stocksTextField.topAnchor.constraint(equalTo: stocksLabel.bottomAnchor, constant: 4).isActive = true
        stocksTextField.leftAnchor.constraint(equalTo: categoriesTextField.leftAnchor).isActive = true
        stocksTextField.rightAnchor.constraint(equalTo: categoriesTextField.rightAnchor).isActive = true
        stocksTextField.heightAnchor.constraint(equalToConstant: 50).isActive = true
        
        scrollView.addSubview(setAsNewArrivalCheckbox)
        setAsNewArrivalCheckbox.topAnchor.constraint(equalTo: stocksTextField.bottomAnchor, constant: 16).isActive = true
        setAsNewArrivalCheckbox.leftAnchor.constraint(equalTo: stocksTextField.leftAnchor, constant: 2).isActive = true
        setAsNewArrivalCheckbox.heightAnchor.constraint(equalToConstant: 30).isActive = true
        setAsNewArrivalCheckbox.widthAnchor.constraint(equalToConstant: 30).isActive = true
        
        scrollView.addSubview(setAsNewArrivalLabel)
        setAsNewArrivalLabel.centerYAnchor.constraint(equalTo: setAsNewArrivalCheckbox.centerYAnchor).isActive = true
        setAsNewArrivalLabel.leftAnchor.constraint(equalTo: setAsNewArrivalCheckbox.rightAnchor, constant: 10).isActive = true
        
        scrollView.addSubview(salePercentageTextField)
        salePercentageTextField.topAnchor.constraint(equalTo: setAsNewArrivalCheckbox.bottomAnchor, constant: 22).isActive = true
        salePercentageTextField.rightAnchor.constraint(equalTo: stocksTextField.rightAnchor).isActive = true
        salePercentageTextField.heightAnchor.constraint(equalToConstant: 50).isActive = true
        salePercentageTextField.widthAnchor.constraint(equalToConstant: 120).isActive = true
        
        scrollView.addSubview(salePercentageLabel)
        salePercentageLabel.leftAnchor.constraint(equalTo: salePercentageTextField.leftAnchor).isActive = true
        salePercentageLabel.bottomAnchor.constraint(equalTo: salePercentageTextField.topAnchor, constant: -4).isActive = true
        
        scrollView.addSubview(setAsSaleCheckbox)
        setAsSaleCheckbox.centerYAnchor.constraint(equalTo: salePercentageTextField.centerYAnchor).isActive = true
        setAsSaleCheckbox.leftAnchor.constraint(equalTo: stocksTextField.leftAnchor, constant: 2).isActive = true
        setAsSaleCheckbox.heightAnchor.constraint(equalToConstant: 30).isActive = true
        setAsSaleCheckbox.widthAnchor.constraint(equalToConstant: 30).isActive = true
        
        scrollView.addSubview(setAsSaleLabel)
        setAsSaleLabel.centerYAnchor.constraint(equalTo: setAsSaleCheckbox.centerYAnchor).isActive = true
        setAsSaleLabel.leftAnchor.constraint(equalTo: setAsSaleCheckbox.rightAnchor, constant: 10).isActive = true
        
        scrollView.addSubview(descriptionLabel)
        descriptionLabel.topAnchor.constraint(equalTo: salePercentageTextField.bottomAnchor, constant: 14).isActive = true
        descriptionLabel.leftAnchor.constraint(equalTo: stocksTextField.leftAnchor).isActive = true
        
        scrollView.addSubview(descriptionTextView)
        descriptionTextView.topAnchor.constraint(equalTo: descriptionLabel.bottomAnchor, constant: 4).isActive = true
        descriptionTextView.leftAnchor.constraint(equalTo: stocksTextField.leftAnchor).isActive = true
        descriptionTextView.rightAnchor.constraint(equalTo: stocksTextField.rightAnchor).isActive = true
        descriptionTextView.heightAnchor.constraint(equalToConstant: 400).isActive = true
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
}
