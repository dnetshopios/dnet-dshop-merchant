//
//  MessagesController.swift
//  dShop Merchant
//
//  Created by Marknel Pogi on 11/8/18.
//  Copyright © 2018 Marknel Pogi. All rights reserved.
//

import UIKit
import Foundation

protocol MessagesMenuDelegate {
    func doCloseMenu()
}

class MessagesController: UIViewController, MessagesMenuDelegate {
    var messages = [Message]() {
        didSet {
            collectionView.reloadData()
        }
    }
    
    lazy var collectionView: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        let cv = UICollectionView(frame: .zero, collectionViewLayout: layout)
        cv.translatesAutoresizingMaskIntoConstraints = false
        cv.backgroundColor = .clear
        cv.delegate = self
        cv.dataSource = self
        return cv
    }()
    
    let cellId = "cellId"
    let headerCellId = "headerCellId"
    
    lazy var searchBar: UISearchBar = {
        let sb = UISearchBar()
        sb.placeholder = "Inbox"
        sb.barTintColor = .white
        sb.addDoneButtonOnKeyboard()
        // sb.becomeFirstResponder()
        sb.translatesAutoresizingMaskIntoConstraints = false
        return sb
    }()
    
    lazy var searchButton: UIButton = {
        let btn = UIButton(type: .system)
        btn.setImage(UIImage(named: "search")?.withRenderingMode(.alwaysOriginal), for: .normal)
        btn.addTarget(self, action: #selector(handleShowMenu), for: .touchUpInside)
        return btn
    }()
    
    lazy var messagesMenuView: MessagesMenuView = {
        let view = MessagesMenuView()
        view.messagesMenuDelegate = self
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    lazy var blackView: UIVisualEffectView = {
        let blurEffect = UIBlurEffect(style: UIBlurEffect.Style.dark)
        let view = UIVisualEffectView(effect: blurEffect)
        view.alpha = 0
        view.backgroundColor = .black
        return view
    }()
    
    func loadMessages() {
        var messages = [Message]()
        for i in 0...10 {
            let message = Message()
            message.id = i + 1
            message.messageTitle = "Lorem \(i)"
            if i % 2 == 0 {
                message.messageContent = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras hendrerit augue congue orci commodo lacinia eget nec lorem. Vestibulum dolor augue, tempus eu lacinia vitae, faucibus sodales justo. Fusce egestas vehicula pretium. In hac habitasse platea dictumst. Donec ultrices eu velit sit amet volutpat. Aliquam vel leo ac tellus accumsan blandit eu in urna. Donec vel sodales orci, auctor rutrum tellus. Sed semper lorem eget dolor sodales, sed volutpat nulla bibendum. Nunc pharetra consequat porta. Nullam sed convallis felis. Donec ut fermentum odio. Phasellus ullamcorper felis ut velit consequat, ullamcorper fringilla nulla suscipit. Curabitur tincidunt suscipit viverra. Sed ullamcorper, purus non mollis hendrerit, sapien dui ullamcorper lacus, vitae tristique arcu felis ac augue.Curabitur tincidunt suscipit viverra. Sed ullamcorper, purus non mollis hendrerit, sapien dui ullamcorper lacus, vitae tristique arcu felis ac augue.Curabitur tincidunt suscipit viverra. <br/><br/>Sed ullamcorper, purus non mollis hendrerit, sapien dui ullamcorper lacus, vitae tristique arcu felis ac augue.Curabitur tincidunt suscipit viverra. Sed ullamcorper, purus non mollis hendrerit, sapien dui ullamcorper lacus, vitae tristique arcu felis ac augue.Curabitur tincidunt suscipit viverra. Sed ullamcorper, purus non mollis hendrerit, sapien dui ullamcorper lacus, vitae tristique arcu felis ac augue."
            } else {
                message.messageContent = "Hi David,<br/><br/>Iste minus et. Non necessitatibus ut est est id amet. Officiis sequi dolorum assumenda ipsam magnam cum possimus. Laudantium nulla amet tempore excepturi id expedita dolorum quisquam deserunt. Odit vel sint dolor eos. Ea blanditiis animi. Quibusdam unde unde. Perspiciatis vel pariatur qui. Deleniti omnis est quae. Laboriosam numquam amet aliquid.<br/><br/>Iste minus et. Non necessitatibus ut est est id amet. Officiis sequi dolorum assumenda ipsam magnam cum possimus. Laudantium nulla amet tempore excepturi id expedita dolorum quisquam deserunt. Odit vel sint dolor eos. Ea blanditiis animi. Quibusdam unde unde. Perspiciatis vel pariatur qui. Deleniti omnis est quae. Laboriosam numquam amet aliquid.Iste minus et. Non necessitatibus ut est est id amet. Officiis sequi dolorum.<br/><br/>Iste minus et. Non necessitatibus ut est est id amet. Officiis sequi dolorum assumenda ipsam magnam cum possimus. Laudantium nulla amet tempore excepturi id expedita dolorum."
            }
            
            messages.append(message)
        }
        
        self.messages = messages
    }
    
    func loadMessagesTags() {
        var tags = [MessagesTag]()
        for i in 0...3 {
            let tag = MessagesTag()
            tag.id = i
            if i == 0 {
                tag.tagName = "Sales"
                tag.tagColor = UIColor(red: 163/255, green: 160/255, blue: 251/255, alpha: 1)
            } else if i == 1 {
                tag.tagName = "Purchased"
                tag.tagColor = UIColor(red: 59/255, green: 134/255, blue: 255/255, alpha: 1)
            } else if i == 2 {
                tag.tagName = "Admin"
                tag.tagColor = UIColor(red: 255/255, green: 101/255, blue: 101/255, alpha: 1)
            } else if i == 3 {
                tag.tagName = "Rating/Reviews"
                tag.tagColor = UIColor(red: 255/255, green: 200/255, blue: 71/255, alpha: 1)
            }
            
            tags.append(tag)
        }
        messagesMenuView.tags = tags
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupViews()
        setupMenu()
        loadMessages()
        loadMessagesTags()
    }
    
    private func setupViews() {
        view.backgroundColor = .mainBG
        
        navigationItem.titleView = searchBar
        navigationItem.rightBarButtonItem = UIBarButtonItem(customView: searchButton)
        
        collectionView.register(MessagesHeaderCell.self, forSupplementaryViewOfKind: UICollectionView.elementKindSectionHeader, withReuseIdentifier: headerCellId)
        
        collectionView.register(MessageCell.self, forCellWithReuseIdentifier: cellId)
        view.addSubview(collectionView)
        collectionView.topAnchor.constraint(equalTo: view.topAnchor).isActive = true
        collectionView.leftAnchor.constraint(equalTo: view.leftAnchor).isActive = true
        collectionView.rightAnchor.constraint(equalTo: view.rightAnchor).isActive = true
        collectionView.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true
    }
    
    var window: UIWindow!
    var windowHeight: CGFloat = 0
    var menuBottomConstraint: NSLayoutConstraint!
    var blackViewBottomConstraint: NSLayoutConstraint!
    
    private func setupMenu() {
        // hidesBottomBarWhenPushed = true
        
        window = UIApplication.shared.keyWindow
        windowHeight = window.frame.height
        
        window.addSubview(blackView)
        blackView.frame = window.frame
        
        window.addSubview(messagesMenuView)
        messagesMenuView.leftAnchor.constraint(equalTo: window.leftAnchor).isActive = true
        messagesMenuView.rightAnchor.constraint(equalTo: window.rightAnchor).isActive = true
        messagesMenuView.heightAnchor.constraint(equalToConstant: windowHeight).isActive = true
        
        menuBottomConstraint = messagesMenuView.bottomAnchor.constraint(equalTo: window.bottomAnchor, constant: windowHeight)
        menuBottomConstraint.isActive = true
    }
    
    public func doCloseMenu() {
        handleHideMenu()
    }
    
    @objc func handleShowMenu() {
        // searchBar.resignFirstResponder()
        searchBar.endEditing(true)
        menuBottomConstraint.constant = 0
        UIView.animate(withDuration: 0.2) {
            self.blackView.alpha = 0.8
            self.window.layoutIfNeeded()
        }
    }
    
    @objc func handleHideMenu() {
        // searchBar.becomeFirstResponder()
        menuBottomConstraint.constant = windowHeight
        UIView.animate(withDuration: 0.2) {
            self.blackView.alpha = 0
            self.window.layoutIfNeeded()
        }
    }
    
    public func navigateToMessageView(message: Message) {
        let controller = MessageController()
        controller.message = message
        let customNavController = CustomNavigationController(rootViewController: controller)
        self.presentDetail(customNavController)
    }
}

// MARK: CollectionView

extension MessagesController: UICollectionViewDelegateFlowLayout, UICollectionViewDelegate, UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let message = messages[indexPath.row]
        navigateToMessageView(message: message)
    }
    
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        let view = collectionView.dequeueReusableSupplementaryView(ofKind: UICollectionView.elementKindSectionHeader, withReuseIdentifier: headerCellId, for: indexPath) as! MessagesHeaderCell
        return view
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
        return CGSize(width: view.frame.width, height: 74)
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return messages.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellId, for: indexPath) as! MessageCell
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 8
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: view.frame.width - 32, height: 100)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 0, left: 16, bottom: 12, right: 16)
    }
}
