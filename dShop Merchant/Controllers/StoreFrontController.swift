//
//  StoreFrontController.swift
//  dShop Merchant
//
//  Created by Marknel Pogi on 12/5/18.
//  Copyright © 2018 Marknel Pogi. All rights reserved.
//

import UIKit

protocol StoreFrontMenuTabDelegate {
    func doScrollToTab(index: Int)
}

protocol StoreProfileDelegate {
    func doAddImageStoreProfile()
}

protocol UserProfileDelegate {
    func doNavigateUserProfile()
}

class StoreFrontController: UIViewController, StoreFrontMenuTabDelegate, StoreProfileDelegate, UserProfileDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    var mainMenuDelegate: MainMenuDelegate?
    
    lazy var userProfileButton: UIButton = {
        let btn = UIButton(type: .system)
        btn.layer.masksToBounds = true
        btn.addTarget(self, action: #selector(handleShowProfile), for: .touchUpInside)
        return btn
    }()
    
    let userProfileImageView: UIImageView = {
        let iv = UIImageView()
        iv.image = UIImage(named: "dummy-avatar")
        iv.contentMode = .scaleAspectFit
        iv.backgroundColor = UIColor(red: 14/255, green: 50/255, blue: 108/255, alpha: 1)
        iv.layer.masksToBounds = true
        iv.layer.cornerRadius = 15
        iv.translatesAutoresizingMaskIntoConstraints = false
        return iv
    }()
    
    func doNavigateUserProfile() {
        navigateToUserProfile()
    }
    
    @objc func handleShowProfile() {
        navigateToUserProfile()
    }
    
    var selectedPhoto: UIImage? {
        didSet {
            collectionView.reloadData()
        }
    }
    
    lazy var headerTabView: StoreFrontHeaderTabView = {
        let view = StoreFrontHeaderTabView()
        view.storeFrontMenuTabDelegate = self
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    lazy var collectionView: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .horizontal
        let cv = UICollectionView(frame: .zero, collectionViewLayout: layout)
        cv.isPagingEnabled = true
        cv.isScrollEnabled = false
        cv.backgroundColor = .clear
        cv.delegate = self
        cv.dataSource = self
        cv.translatesAutoresizingMaskIntoConstraints = false
        return cv
    }()
    
    let cellId = "cellId"
    let headerAndAdsCellId = "headerAndAdsCellId"
    let themeCellId = "themeCellId"
    let storeProfileCellId = "storeProfileCellId"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupViews()
    }
    
    private func setupViews() {
        view.backgroundColor = .mainBG
        
        // MARK: Set navigationbar logo
        let dshopImageView = UIImageView()
        navigationItem.titleView = dshopImageView.dshopLogo()
        navigationItem.leftBarButtonItem = UIBarButtonItem(image: UIImage(named: "menu")?.withRenderingMode(.alwaysOriginal), style: .plain, target: self, action: #selector(handleShowSliderMenu))
        
        let customBarItem = UIBarButtonItem(customView: userProfileButton)
        userProfileButton.addSubview(userProfileImageView)
        userProfileImageView.topAnchor.constraint(equalTo: userProfileButton.topAnchor).isActive = true
        userProfileImageView.leftAnchor.constraint(equalTo: userProfileButton.leftAnchor).isActive = true
        userProfileImageView.widthAnchor.constraint(equalToConstant: 30).isActive = true
        userProfileImageView.heightAnchor.constraint(equalToConstant: 30).isActive = true
        navigationItem.setRightBarButton(customBarItem, animated: true)
        
        view.addSubview(headerTabView)
        headerTabView.topAnchor.constraint(equalTo: view.topAnchor).isActive = true
        headerTabView.leftAnchor.constraint(equalTo: view.leftAnchor).isActive = true
        headerTabView.rightAnchor.constraint(equalTo: view.rightAnchor).isActive = true
        headerTabView.heightAnchor.constraint(equalToConstant: 50).isActive = true
        
        collectionView.register(UICollectionViewCell.self, forCellWithReuseIdentifier: cellId)
        collectionView.register(HeaderAndAdsCell.self, forCellWithReuseIdentifier: headerAndAdsCellId)
        collectionView.register(ThemeCell.self, forCellWithReuseIdentifier: themeCellId)
        collectionView.register(StoreProfileCell.self, forCellWithReuseIdentifier: storeProfileCellId)
        view.addSubview(collectionView)
        collectionView.topAnchor.constraint(equalTo: headerTabView.bottomAnchor).isActive = true
        collectionView.leftAnchor.constraint(equalTo: view.leftAnchor).isActive = true
        collectionView.rightAnchor.constraint(equalTo: view.rightAnchor).isActive = true
        collectionView.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        // headerTabView.scrollToFirstTab()
    }
    
    public func doScrollToTab(index: Int = 0) {
        scrollToStoreFrontTab(index: index)
    }
    
    public func scrollToStoreFrontTab(index: Int = 0) {
        let indexPath = IndexPath(row: index, section: 0)
        collectionView.scrollToItem(at: indexPath, at: .left, animated: true)
    }
    
    @objc func handleShowSliderMenu() {
        mainMenuDelegate?.doShowMainMenu()
    }
    
    public func navigateToUserProfile() {
        let controller = UserProfileController()
        navigationController?.pushViewController(controller, animated: true)
    }
    
    func doAddImageStoreProfile() {
        let myPickerController = UIImagePickerController()
        myPickerController.delegate = self
        myPickerController.sourceType =  UIImagePickerController.SourceType.photoLibrary
        self.present(myPickerController, animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        if let pickedImage = info[UIImagePickerController.InfoKey.originalImage] as? UIImage {
            selectedPhoto = pickedImage
        }
        
        dismiss(animated: true, completion: nil)
    }
}

// MARK: CollectionView
extension StoreFrontController: UICollectionViewDelegateFlowLayout, UICollectionViewDelegate, UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 4
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if indexPath.row == 0 {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: headerAndAdsCellId, for: indexPath) as! HeaderAndAdsCell
            return cell
        } else if indexPath.row == 1 {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: themeCellId, for: indexPath) as! ThemeCell
            return cell
        } else if indexPath.row == 2 {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: storeProfileCellId, for: indexPath) as! StoreProfileCell
            cell.storeProfileDelegate = self
            cell.selectedPhoto = selectedPhoto
            return cell
        }
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellId, for: indexPath)
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: view.frame.width, height: view.frame.height - 50)
    }
}
