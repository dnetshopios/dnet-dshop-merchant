//
//  HomepageController.swift
//  dShop Merchant
//
//  Created by Marknel Pogi on 11/6/18.
//  Copyright © 2018 Marknel Pogi. All rights reserved.
//

import UIKit

protocol LoginDelegate {
    func doShowLogin()
    func doHideLogin()
}

protocol MainMenuDelegate {
    func doShowMainMenu()
    func doHideMainMenu()
}

class HomepageController: UIViewController, LoginDelegate, MainMenuDelegate {
    var userProfileDelegate: UserProfileDelegate?
    
    var summaryStatistics = [SummaryStatistic]() {
        didSet {
            collectionView.reloadData()
        }
    }
    
    lazy var collectionView: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        let cv = UICollectionView(frame: .zero, collectionViewLayout: layout)
        cv.backgroundColor = .clear
        cv.delegate = self
        cv.dataSource = self
        cv.translatesAutoresizingMaskIntoConstraints = false
        return cv
    }()

    let headerCellId = "headerCellId"
    let cellId = "cellId"
    let summaryStatisticsCellId = "summaryStatisticsCellId"
    let monthlyStatisticsCellId = "monthlyStatisticsCellId"
    let customersMapCellId = "customersMapCellId"
    let activitiesDetailsCellId = "activitiesDetailsCellId"
    
    var mainMenuLeftConstraint: NSLayoutConstraint!
    var window: UIWindow!
    var windowWidth: CGFloat = 0
    var windowHeight: CGFloat = 0
    lazy var blackView: UIVisualEffectView = {
        let blurEffect = UIBlurEffect(style: UIBlurEffect.Style.dark)
        let view = UIVisualEffectView(effect: blurEffect)
        view.alpha = 0
        view.backgroundColor = .black
        view.isUserInteractionEnabled = true
        view.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(handleHideSliderMenu)))
        return view
    }()
    
    lazy var mainMenuView: MainMenuView = {
        let menu = MainMenuView()
        menu.loginDelegate = self
        menu.mainMenuDelegate = self
        menu.translatesAutoresizingMaskIntoConstraints = false
        return menu
    }()
    
    lazy var loginView: LoginView = {
        let view = LoginView()
        view.loginDelegate = self
        view.alpha = 1
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    private func loadStatistics() {
        var statistics = [SummaryStatistic]()
        
        for i in 0...3 {
            var title = ""
            var currencySymbol = ""
            var value = ""
            var percentage: Double = 0
            
            if i == 0 {
                title = "Month Sales"
                currencySymbol = "₱"
                value = "60K"
                percentage = -13.8
            } else if i == 1 {
                title = "Daily Sale"
                currencySymbol = "₱"
                value = "2,530"
                percentage = 13.8
            } else if i == 2 {
                title = "Products Sold"
                currencySymbol = ""
                value = "11K"
                percentage = -13.8
            } else if i == 3 {
                title = "Total Revenue"
                currencySymbol = "₱"
                value = "609K"
                percentage = 13.8
            }
            
            let statistic = SummaryStatistic()
            statistic.index = i
            statistic.title = title
            statistic.currencySymbol = currencySymbol
            statistic.value = value
            statistic.percentage = percentage
            
            statistics.append(statistic)
        }
        
        summaryStatistics = statistics
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupViews()
        setupMainMenu()
        setupLogin()
        loadStatistics()
    }
    
    lazy var userProfileButton: UIButton = {
        let btn = UIButton(type: .system)
        btn.layer.masksToBounds = true
        btn.addTarget(self, action: #selector(handleShowProfile), for: .touchUpInside)
        return btn
    }()
    
    let userProfileImageView: UIImageView = {
        let iv = UIImageView()
        iv.image = UIImage(named: "dummy-avatar")
        iv.contentMode = .scaleAspectFit
        iv.backgroundColor = UIColor(red: 14/255, green: 50/255, blue: 108/255, alpha: 1)
        iv.layer.masksToBounds = true
        iv.layer.cornerRadius = 15
        iv.translatesAutoresizingMaskIntoConstraints = false
        return iv
    }()
    
    @objc func handleShowProfile() {
        tabBarController?.selectedIndex = 3
        userProfileDelegate?.doNavigateUserProfile()
    }
    
    private func setupViews() {
        view.backgroundColor = .mainBG
        
        // MARK: Set navigationbar logo
        let dshopImageView = UIImageView()
        navigationItem.titleView = dshopImageView.dshopLogo()
        navigationItem.leftBarButtonItem = UIBarButtonItem(image: UIImage(named: "menu")?.withRenderingMode(.alwaysOriginal), style: .plain, target: self, action: #selector(handleShowSliderMenu))
        
        let customBarItem = UIBarButtonItem(customView: userProfileButton)
        userProfileButton.addSubview(userProfileImageView)
        userProfileImageView.topAnchor.constraint(equalTo: userProfileButton.topAnchor).isActive = true
        userProfileImageView.leftAnchor.constraint(equalTo: userProfileButton.leftAnchor).isActive = true
        userProfileImageView.widthAnchor.constraint(equalToConstant: 30).isActive = true
        userProfileImageView.heightAnchor.constraint(equalToConstant: 30).isActive = true
        navigationItem.setRightBarButton(customBarItem, animated: true)
        
        collectionView.register(HomepageHeaderCell.self, forSupplementaryViewOfKind: UICollectionView.elementKindSectionHeader, withReuseIdentifier: headerCellId)
        collectionView.register(UICollectionViewCell.self, forCellWithReuseIdentifier: cellId)
        collectionView.register(SummaryStatisticsCell.self, forCellWithReuseIdentifier: summaryStatisticsCellId)
        collectionView.register(MonthlyStatisticsCell.self, forCellWithReuseIdentifier: monthlyStatisticsCellId)
        collectionView.register(CustomersMapCell.self, forCellWithReuseIdentifier: customersMapCellId)
        collectionView.register(ActivitiesDetailsCell.self, forCellWithReuseIdentifier: activitiesDetailsCellId)
        
        view.addSubview(collectionView)
        collectionView.topAnchor.constraint(equalTo: view.topAnchor).isActive = true
        collectionView.leftAnchor.constraint(equalTo: view.leftAnchor).isActive = true
        collectionView.rightAnchor.constraint(equalTo: view.rightAnchor).isActive = true
        collectionView.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true
    }
    
    private func setupMainMenu() {
        window = UIApplication.shared.keyWindow
        windowWidth = window.frame.width
        windowHeight = window.frame.height
        
        window.addSubview(blackView)
        blackView.frame = window.frame
        
        window.addSubview(mainMenuView)
        mainMenuView.topAnchor.constraint(equalTo: window.topAnchor).isActive = true
        mainMenuView.widthAnchor.constraint(equalToConstant: 260).isActive = true
        
        mainMenuLeftConstraint = mainMenuView.leftAnchor.constraint(equalTo: window.leftAnchor, constant: -windowWidth)
        mainMenuLeftConstraint.isActive = true
        
        mainMenuView.bottomAnchor.constraint(equalTo: window.bottomAnchor).isActive = true
    }
    
    private func setupLogin() {
        window.addSubview(loginView)
        loginView.topAnchor.constraint(equalTo: window.topAnchor).isActive = true
        loginView.leftAnchor.constraint(equalTo: window.leftAnchor).isActive = true
        loginView.rightAnchor.constraint(equalTo: window.rightAnchor).isActive = true
        loginView.bottomAnchor.constraint(equalTo: window.bottomAnchor).isActive = true
    }
    
    func doShowLogin() {
        UIView.animate(withDuration: 0.2) {
            self.loginView.alpha = 1
            self.window.layoutIfNeeded()
        }
    }
    
    func doHideLogin() {
        UIView.animate(withDuration: 0.5) {
            self.loginView.alpha = 0
            self.window.layoutIfNeeded()
        }
    }
    
    func doShowMainMenu() {
        handleShowSliderMenu()
    }
    
    func doHideMainMenu() {
        handleHideSliderMenu()
    }
    
    @objc public func handleHideSliderMenu() {
        mainMenuLeftConstraint.constant = -windowWidth
        UIView.animate(withDuration: 0.5) {
            self.blackView.alpha = 0
            self.window.layoutIfNeeded()
        }
    }
    
    @objc public func handleShowSliderMenu() {
        mainMenuLeftConstraint.constant = 0
        UIView.animate(withDuration: 0.5) {
            self.blackView.alpha = 0.6
            self.window.layoutIfNeeded()
        }
    }
    
    private func showAllFontsName() {
        for family in UIFont.familyNames {
            
            let sName: String = family as String
            print("family: \(sName)")
            
            for name in UIFont.fontNames(forFamilyName: sName) {
                print("name: \(name as String)")
            }
        }
    }
}

// MARK: COLLECTIONVIEW

extension HomepageController: UICollectionViewDelegateFlowLayout, UICollectionViewDelegate, UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        let view = collectionView.dequeueReusableSupplementaryView(ofKind: UICollectionView.elementKindSectionHeader, withReuseIdentifier: headerCellId, for: indexPath) as! HomepageHeaderCell
        return view
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
        return CGSize(width: view.frame.width, height: 74)
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 4
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if indexPath.row == 0 {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: summaryStatisticsCellId, for: indexPath) as! SummaryStatisticsCell
            cell.statistics = summaryStatistics
            return cell
        } else if indexPath.row == 1 {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: monthlyStatisticsCellId, for: indexPath) as! MonthlyStatisticsCell
            return cell
        } else if indexPath.row == 2 {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: customersMapCellId, for: indexPath) as! CustomersMapCell
            return cell
        } else if indexPath.row == 3 {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: activitiesDetailsCellId, for: indexPath) as! ActivitiesDetailsCell
            return cell
        }
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellId, for: indexPath)
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 12
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if indexPath.row == 0 {
            return CGSize(width: view.frame.width - 32, height: 192)
        } else if indexPath.row == 1 {
            return CGSize(width: view.frame.width - 32, height: 700)
        } else if indexPath.row == 2 {
            return CGSize(width: view.frame.width - 32, height: 500)
        } else if indexPath.row == 3 {
            return CGSize(width: view.frame.width - 32, height: 448)
        }
        
        return .zero
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 0, left: 16, bottom: 16, right: 16)
    }
}

