//
//  NavigationHelper.swift
//  dShop
//
//  Created by Marknel Pineda on 08/10/2018.
//  Copyright © 2018 Marknel Pineda. All rights reserved.
//

import UIKit

//MARK:- Helper Functions

public func generateNavigationController(for rootViewController: UIViewController, title: String, image: UIImage) -> UIViewController {
    let navController = CustomNavigationController(rootViewController: rootViewController)
    
    navController.navigationBar.isTranslucent = false
    navController.navigationBar.barTintColor = .navBG
    navController.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
    
    navController.tabBarItem = UITabBarItem(title: title, image: image, tag: 0)
    navController.tabBarItem.imageInsets = UIEdgeInsets(top: 4, left: 0, bottom: -5, right: 0)
    return navController
}
