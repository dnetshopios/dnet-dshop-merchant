//
//  AspectRatio.swift
//  dShop
//
//  Created by Marknel Pineda on 11/10/2018.
//  Copyright © 2018 Marknel Pineda. All rights reserved.
//

import UIKit

// MARK: HD Ratio 16/9 || 9:16

func getAspectHeight(width: CGFloat) -> CGFloat {
    let height = (width / 16) * 9
    return CGFloat(height)
}

func getAspectWidth(height: CGFloat) -> CGFloat {
    let width = (height / 9) * 16
    return CGFloat(width)
}

func getAspectNewHeight(width: CGFloat, originalWidth: CGFloat, originalHeight: CGFloat) -> CGFloat {
    // adjusted height = <user-chosen width> * original height / original width
    return (width * originalHeight) / originalWidth
}

// adjusted width = <user-chosen height> * original width / original height

