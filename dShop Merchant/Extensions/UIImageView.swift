//
//  UIImageView.swift
//  dShop
//
//  Created by Marknel Pogi on 11/4/18.
//  Copyright © 2018 Marknel Pineda. All rights reserved.
//

import UIKit

extension UIImageView {
    public func dshopLogo() -> UIImageView {
        
        let logoImageView = UIImageView(frame: CGRect(x: 0, y: 0, width: 100, height: 30))
        logoImageView.contentMode = .scaleAspectFit
        
        let image = UIImage(named: "dshop_logo")
        logoImageView.image = image
        
        return logoImageView
    }
}
