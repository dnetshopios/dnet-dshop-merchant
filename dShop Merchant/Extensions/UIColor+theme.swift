//
//  UIColor+theme.swift
//  dShop
//
//  Created by Marknel Pineda on 11/10/2018.
//  Copyright © 2018 Marknel Pineda. All rights reserved.
//

import UIKit

// MARK: Theme colors

extension UIColor {
    // WHITE
    static let plainWhite = UIColor(red: 255/255, green: 255/255, blue: 255/255, alpha: 1)
    
    // NAVIGATION BACKGROUND
    static let navBG = UIColor(red: 2/255, green: 15/255, blue: 58/255, alpha: 1)
    
    // NAVIGATION BACKGROUND
    static let mainBG = UIColor(red: 0/255, green: 34/255, blue: 87/255, alpha: 1)
    
    // ACTIVE TAB
    static let activeTab = UIColor(red: 0/255, green: 165/255, blue: 227/255, alpha: 1)
    
}

