//
//  UITextView+AddImage.swift
//  dShop Merchant
//
//  Created by Marknel Pogi on 12/7/18.
//  Copyright © 2018 Marknel Pogi. All rights reserved.
//

import UIKit

// ref: https://stackoverflow.com/questions/24010035/how-to-add-image-and-text-in-uitextview-in-ios/30417310#30417310

extension UITextView {
    func addImage(image: UIImage) {
        let existingAttributedText = self.attributedText ?? NSAttributedString(string: "")
        let newAttributedString = NSMutableAttributedString(attributedString: existingAttributedText)
        
        let newLineAttributedString = NSAttributedString(string: "\n")
        
        let attachment = NSTextAttachment()
        attachment.image = image
        
        //calculate new size.  (-20 because I want to have a litle space on the right of picture)
        let newImageWidth = (self.bounds.size.width - 32)
        let scale = newImageWidth / image.size.width
        let newImageHeight = image.size.height * scale
        //resize this
        attachment.bounds = CGRect.init(x: 0, y: 0, width: newImageWidth, height: newImageHeight)
        
        let attrStringWithImage = NSAttributedString(attachment: attachment)
        
        newAttributedString.append(newLineAttributedString)
        newAttributedString.append(attrStringWithImage)
        newAttributedString.append(newLineAttributedString)
        
        self.attributedText = newAttributedString
    }
}
