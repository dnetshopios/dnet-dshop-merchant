//
//  UITextField+Inset.swift
//  dShop
//
//  Created by Marknel Pineda on 15/10/2018.
//  Copyright © 2018 Marknel Pineda. All rights reserved.
//

import UIKit

extension UITextField {
    func setLeftPaddingPoints(_ amount:CGFloat){
        let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: amount, height: self.frame.size.height))
        self.leftView = paddingView
        self.leftViewMode = .always
    }
    func setRightPaddingPoints(_ amount:CGFloat) {
        let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: amount, height: self.frame.size.height))
        self.rightView = paddingView
        self.rightViewMode = .always
    }
}

extension UITextField {
    func loadDropdownData(data: [String]) {
        self.inputView = CustomPickerView(pickerData: data, dropdownField: self)
    }
}
